//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
// 3D Gizmos - https://github.com/CedricGuillemet/ImGuizmo

#include "eng_base.h"
#include "xPlugins/imgui/imgui_impl_engbase.h"
#include "xPlugins/imgui/imguiThemeCoolblue.h"
#include "xPlugins/imgui/imguiXProperty.h"
#include "unittests/x_unit_test_property.h"


//-------------------------------------------------------------------------------------------

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR pCmdLine, int nCmdShow)
{
    //
    // Initialize the app
    //
    g_context::Init();
    g_context::get().m_Scheduler.Init();
    g_context::get().m_Scheduler_LogChannel.TurnOff();

    //
    // Deal with the actual app
    //
    xndptr_s<eng_instance>      Instance;
    eng_device*                 pDevice = nullptr;
    eng_window*                 pWindow = nullptr;

    // Create engine instance
    {
        eng_instance::setup         Setup( (void*)hInstance );

      //  Setup.m_bValidation = true;

        auto Err = eng_instance::CreateInstance( Instance, Setup );
        x_assert( !Err );
    }

    // create a device
    {
        eng_device::setup    Setup;
        auto Err = Instance->CreateDevice( pDevice, Setup );
        x_assert( !Err );
    }

    // create a window 
    {
        eng_window::setup   Setup( *pDevice );
        auto Err = Instance->CreateWindow( pWindow, Setup );  
        x_assert( !Err );
    }

    //
    // Initialize the scene
    //
    static const xarray<xvector3,5>  Centers
    {
        xvector3( 0.0f), 
        xvector3( 1.0f),  
        xvector3( 0.5f),  
        xvector3(-0.5f), 
        xvector3(-1.0f) 
    };

    eng_view                        View;
    eng_texture::handle             hTexture;
    eng_sampler::handle             hSampler;
    eng_shader_vert::handle         hVertShader;
    eng_shader_frag::handle         hFragShader;
    eng_material_type::handle       hMaterial;
    eng_material_informed::handle   hMaterialInformed;
    eng_pipeline::handle            hPipeline;

    //
    // Setup the view
    //
    View.setFov         ( xradian{ 91.5_deg } );
    View.setFarZ        ( 256.0f );
    View.setNearZ       ( 0.1f );

    //
    // Create a default texture
    //
    {
        xbitmap                 Bitmap;
        Bitmap.setDefaultTexture();
        pDevice->CreateTexture( hTexture, Bitmap );
    }

    //
    // Create a Sampler for the texture
    //
    if( (1) )
    {
        eng_sampler::setup Setup( hTexture );
        pDevice->CreateSampler( hSampler, Setup );
    }

    //
    // Starting the imgui
    //
    ImGui_engBase_Init( *pDevice, *pWindow, false );

    imguiThemeCoolblue();

    imgui_xproperty::window xPropWindow;

    x_unit_test::property03::testtypes Test;

    xPropWindow.setupProperty( &Test );


    //
    // Do the Integration
    //
    bool show_another_window = false;
    bool show_test_window = false;
    ImVec4 clear_col = ImColor(114, 144, 154);
    while( pWindow->HandleEvents() )
    {
        auto CameraAngles   = View.getAngles(); 
        CameraAngles.m_Yaw += xradian{ 0.1_deg };
        
        View.LookAt( 2.5f, CameraAngles, Centers[0] );
        
        pWindow->BeginRender( View );
        
        // Updates the view port and all the cached matrices
        View = pWindow->getActiveView();

        ImGui_engBase_NewFrame();

        // Bregin
        static bool bOPen = false;
        ImGui::SetNextWindowPos( ImVec2(0,0) );
        ImGui::SetWindowSize( "maindoc", ImVec2( (f32)View.getViewport().getWidth(), (f32)View.getViewport().getHeight()), 0 );
        ImGui::Begin( "maindoc", &bOPen, 
            ImGuiSetCond_Always                     |
            ImGuiWindowFlags_NoMove                 |
            ImGuiWindowFlags_NoResize               |
            ImGuiWindowFlags_NoInputs               |
            ImGuiWindowFlags_NoTitleBar             |
            ImGuiWindowFlags_NoSavedSettings        |
            ImGuiWindowFlags_NoScrollbar            |
            ImGuiWindowFlags_NoCollapse             |
            ImGuiWindowFlags_NoFocusOnAppearing     |
            ImGuiWindowFlags_AlwaysUseWindowPadding |
            ImGuiWindowFlags_NoBringToFrontOnFocus );
        ImGui::BeginDockspace();

        // 1. Show a simple window
        // Tip: if we don't call ImGui::Begin()/ImGui::End() the widgets appears in a window automatically called "Debug"
        
        static bool Open = true;
        if( ImGui::BeginDock( "test1", &Open ) )
        {
            static float f = 0.0f;
            ImGui::Text("Hello, world!");
            ImGui::SliderFloat("float", &f, 0.0f, 1.0f);
            ImGui::ColorEdit3("clear color", (float*)&clear_col);
            if (ImGui::Button("Test Window")) show_test_window ^= 1;
            if (ImGui::Button("Another Window")) show_another_window ^= 1;
            ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
        }
        ImGui::EndDock();

        // 2. Show another simple window, this time using an explicit Begin/End pair
        if (show_another_window)
        {
            ImGui::SetNextWindowSize(ImVec2(200,100), ImGuiSetCond_FirstUseEver);
            ImGui::Begin("Another Window", &show_another_window);
            ImGui::Text("Hello");
            ImGui::End();
        }

        // 3. Show the ImGui test window. Most of the sample code is in ImGui::ShowTestWindow()
        if (show_test_window)
        {
            ImGui::SetNextWindowPos(ImVec2(650, 20), ImGuiSetCond_FirstUseEver);     // Normally user code doesn't need/want to call it because positions are saved in .ini file anyway. Here we just want to make the demo initial state a bit more friendly!
            ImGui::ShowTestWindow(&show_test_window);
        }

        //
        // Do the properties
        //
        xPropWindow.Update();
        xPropWindow.Render();

        ImGui::EndDockspace();
        ImGui::End();
        ImGui::Render();

        pWindow->EndRender();
        pWindow->PageFlip();
    }

    //
    // Shut down
    //
    ImGui_engBase_Shutdown();

    //
    // End the app
    //
    g_context::Kill();
    return 0;
}

