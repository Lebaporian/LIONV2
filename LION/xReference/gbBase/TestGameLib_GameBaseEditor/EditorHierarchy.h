//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

class entity_hierarchy
{
public:

    gb_game_graph::base*                m_pBase             { nullptr };
    xvector<gb_component::entity*>      m_Select            {};
    xvector<gb_component::entity*>      m_InWorldEntities   {};
    xvector<gb_component::entity*>      m_OutWorldEntities  {};

    void UhselectAll( void )
    {
        for( auto& pE : m_Select )
        {
            pE->setEditorSelect( false );
        }

        m_Select.DeleteAllEntries();
    }

    void Refresh( void )
    {
        m_InWorldEntities.DeleteAllEntries();
        m_OutWorldEntities.DeleteAllEntries();

        x_lk_guard_as_const_get_as_const( m_pBase->m_CompTypeDB.m_Vector, ComponentTypeLinearVector );

        for( const auto pTypeEntry : ComponentTypeLinearVector )
        {
            if( pTypeEntry->m_pEntry->isEntityType() == false ) continue;

            for( const auto pEntry : pTypeEntry->m_pEntry->getInWorld() )
            {
                auto& Entity = pEntry->SafeCast<gb_component::entity>();
                m_InWorldEntities.append( &Entity );
            }
        }
    }

    void Render( void )
    {
        static bool Open1 = true;
        xstring     Temp;

        if( ImGui::BeginDock( ICON_FA_SITEMAP " Hierarchy", &Open1 ) )
        {
            // Temporary storage of what node we have clicked to process selection at the end of the loop. May be a pointer to your own node type, etc.
            gb_component::entity* pSelected = nullptr;

            ImGui::Button( "Test" );
            ImGui::Separator();
            ImGui::BeginChild("scene view");

            //
            // List of entities
            //
            if( ImGui::TreeNodeEx( "World", ImGuiTreeNodeFlags_DefaultOpen ) )
            {
                if( m_pBase )
                {
                    x_lk_guard_as_const_get_as_const( m_pBase->m_CompTypeDB.m_Vector, ComponentTypeLinearVector );

                    for( const auto pTypeEntry : ComponentTypeLinearVector )
                    {
                        if( pTypeEntry->m_pEntry->isEntityType() == false ) continue;

                        for( const auto pEntry : pTypeEntry->m_pEntry->getInWorld() )
                        {
                            auto& Entity = pEntry->SafeCast<gb_component::entity>();

                            const ImGuiTreeNodeFlags node_flags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick | (Entity.isEditorSelected() ? ImGuiTreeNodeFlags_Selected : 0);
                    
                            // Leaf: The only reason we have a TreeNode at all is to allow selection of the leaf. Otherwise we can use BulletText() or TreeAdvanceToLabelPos()+Text().
                            Entity.getGuid().getStringHex( Temp );  
                            if( Entity.isKindOf<gb_component::entity_static>() )        ImGui::TreeNodeEx( &Entity, node_flags | ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen, ICON_FA_UNIVERSITY " %s(%s)", Entity.getType().getCategoryName().m_Char, Temp );
                            else if( Entity.isKindOf<gb_component::entity_dynamic>() )  ImGui::TreeNodeEx( &Entity, node_flags | ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen, ICON_FA_LEAF " %s(%s)", Entity.getType().getCategoryName().m_Char, Temp );
                            else                                                        ImGui::TreeNodeEx( &Entity, node_flags | ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen, ICON_FA_COGS " %s(%s)", Entity.getType().getCategoryName().m_Char, Temp );
                        
                            if (ImGui::IsItemClicked()) 
                                pSelected = &Entity;
                        }
                    }
                }
                ImGui::TreePop();
            }

            /*
            //
            // Entities that are not in the world
            //
            if (ImGui::TreeNode("Outside World"))
            {
                if( m_pBase )
                {
                    x_lk_guard_as_const_get( m_pBase->m_EntityDB.m_Vector, EntityLinearVector ); 
                    for( auto* pEntry : EntityLinearVector )
                    {
                        auto& Entity = *pEntry->m_pEntry;
                        if( Entity.isInWorld() == true ) continue;

                        const ImGuiTreeNodeFlags node_flags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick | (Entity.isEditorSelected() ? ImGuiTreeNodeFlags_Selected : 0);
                    
                        // Leaf: The only reason we have a TreeNode at all is to allow selection of the leaf. Otherwise we can use BulletText() or TreeAdvanceToLabelPos()+Text().
                        Entity.getGuid().getStringHex( Temp );                    
                        if( Entity.isKindOf<gb_component::entity_static>() )        ImGui::TreeNodeEx( &Entity, node_flags | ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen, ICON_FA_UNIVERSITY " %s(%s)", Entity.getType().getCategoryName().m_Char, Temp );
                        else if( Entity.isKindOf<gb_component::entity_dynamic>() )  ImGui::TreeNodeEx( &Entity, node_flags | ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen, ICON_FA_LEAF " %s(%s)", Entity.getType().getCategoryName().m_Char, Temp );
                        else                                                        ImGui::TreeNodeEx( &Entity, node_flags | ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen, ICON_FA_COGS " %s(%s)", Entity.getType().getCategoryName().m_Char, Temp );
                        if (ImGui::IsItemClicked()) 
                            pSelected = &Entity;
                    }
                }

                ImGui::TreePop();
            }
            */

            ImGui::EndChild();

            //
            // Deal with selection
            //
            if( pSelected )
            {
                // Update selection state. Process outside of tree loop to avoid visual inconsistencies during the clicking-frame.
                if( ImGui::GetIO().KeyCtrl )
                {
                    // CTRL+click to toggle
                    pSelected->setEditorSelect( true );
                    m_Select.append( pSelected ); 
                }
                else
                {
                    // Remove all the selected entities so far
                    for( auto& pS : m_Select )
                    {
                        pS->setEditorSelect( false );
                    }

                    // Select entity
                    m_Select.DeleteAllEntries();
                    pSelected->setEditorSelect( true );
                    m_Select.append( pSelected ); 
                }
            }

        }
        ImGui::EndDock();
    }

};