//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

namespace editor_windows
{
    class blueprint_selector
    {
    public:

        enum class mode : u8
        {
            NORMAL,
            CREATE_ONLY
        };

        blueprint_selector( void ) = default;

        bool isCreated( void ) const
        {
            return m_bCreated;
        }

        void Refresh( gb_game_graph::base& Base )
        {
            m_pBase = &Base;

            RefreshList();

            // The type must be register!
            // x_assert( Base.m_ConstDataTypeDB.Find( gType ) );
        }

        void Open( mode Mode )
        {
            ImGui::OpenPopup( "Blueprint Selector" );
            m_Mode = Mode;
        }

        bool Render( void )
        {
            ImGui::SetNextWindowSizeConstraints(ImVec2(400, 200), ImVec2(400, 200)); 
            
            if( ImGui::BeginPopup( "Blueprint Selector" ) )
            {
                bool bSelected = false;

                ImGui::CollapsingHeader( "Blueprint Selector", ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_Framed );

                ImGui::Separator();
                if( ImGui::Button( ICON_FA_FILE_O " Create New Entry", ImVec2( 200, 0 ) ) )
                {
                    m_pSelected = &m_pBase->CreateBlueprint();

                    // create a blueprint with a default entity
                    auto& Entity = m_pBase->CreateEntity<gb_component::entity>( gb_component::entity::guid::RESET );
                    m_pSelected->setup( X_STR(""), X_STR("New Blueprint"), Entity );
                    Entity.linearDestroy();

                    // Notify the system that we just created a new blueprint
                    m_pBase->m_eventNewBlueprint.Notify( nullptr );

                    ImGui::CloseCurrentPopup();
                    bSelected = true;
                    m_bCreated = true;
                }

                ImGui::Separator();
                if( ImGui::Button( ICON_FA_TIMES ) )
                {
                    m_Filter.Clear();
                }
                ImGui::SameLine(); 
                m_Filter.Draw("Filter", -100.0f);

               // ImGui::Separator();
                ImGui::BeginChild("item view");

                // See if the user wants us to filer entries for him
                const bool bFilter = m_Filter.IsActive();

                //
                // List of entities
                //
                if ( m_Mode == mode::NORMAL && (bFilter || ImGui::TreeNodeEx("Blueprints", ImGuiTreeNodeFlags_DefaultOpen )) )
                {
                    xvector<std::pair<folder*,int>> m_Stack;
                    auto*   pFolder = &m_Folder;
                    int     i=0;
                    do
                    {
                        AGAIN:
                        for( ; i < pFolder->m_Folders.getCount(); i++ )
                        {
                            if( bFilter || ImGui::TreeNode(pFolder->m_Folders[i].m_Name) )
                            {
                                // Push current situation in the stack
                                m_Stack.append(std::pair<folder*,int>{pFolder, i+1} );

                                pFolder = &pFolder->m_Folders[i];
                                i = 0;
                                goto AGAIN;
                            }
                        }

                        for( auto pBlueprint : pFolder->m_Bluesprints )
                        {
                            // const ImGuiTreeNodeFlags node_flags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick | 0;//(m_pSelected == pBlueprint ? ImGuiTreeNodeFlags_Selected : 0);

                            if( bFilter )
                            {
                                const char* pName = pBlueprint->getName();
                                const char* pEnd  = &pName[ x_strlen(pName).m_Value ];

                                if( m_Filter.PassFilter( pName, pEnd ) == false )
                                    continue;
                            }

                            ImGui::TreeNodeEx( pBlueprint, ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen, ICON_FA_CODEPEN " %s", (const char*)pBlueprint->getName() );

                            if (ImGui::IsItemClicked())
                            {
                                m_pSelected = pBlueprint;
                                bSelected = true;
                                m_bCreated = false;
                                ImGui::CloseCurrentPopup();
                            }
                        }
                
                        // Done with this tree
                        if(!bFilter) ImGui::TreePop();
                        int iCur = m_Stack.getCount<int>()-1;

                        // Nothing else to do then just be done
                        if( iCur == -1 ) break;

                        // continue with the previous folder
                        pFolder = m_Stack[iCur].first;
                        i       = m_Stack[iCur].second;

                        // Remove entry from the stack
                        m_Stack.DeleteWithSwap( iCur );

                    } while( true );
                }

                /*
                for( auto& pBlueprint : m_List )
                {
                    if( bFilter )
                    {
                        const char* pName = pBlueprint->getName();
                        const char* pEnd  = &pName[ x_strlen(pName).m_Value ];

                        if( m_Filter.PassFilter( pName, pEnd ) == false )
                            continue;
                    }

                    const auto RefCount = 0;//pBlueprint->getReferenceCount();
                    if( RefCount == 1 )
                    {
                        const auto Index = m_List.getIndexByEntry( pBlueprint );
                        if( ImGui::Button( xstring::Make( ICON_FA_TIMES "##%d", Index) ) ) 
                        {
                            m_iSelectedEntry = static_cast<int>(Index);
                            ImGui::OpenPopup( "Delete Constant Data" );
                        }
                    }
                    else
                    {
                        ImGui::Text( "(%d) ", RefCount - 1 );
                    }
                    ImGui::SameLine();

                    if( ImGui::Selectable( pBlueprint->getName() ) )   // Editor
                    {
                        m_iSelectedEntry = static_cast<int>(  m_List.getIndexByEntry( pBlueprint ) );
                        ImGui::CloseCurrentPopup();
                        bSelected = true;
                    }
                }
                */

                /*
                //
                // Deleting constant data
                //
                if( ImGui::BeginPopupModal( "Delete Blueprint" ) )
                {
                    if( ImGui::Button( "Yes Delete", ImVec2(120,0) ) )
                    {
                        m_pBase->m_BlueprintDB.Delete( m_List[m_iSelectedEntry]->getGuid() );
                        ImGui::CloseCurrentPopup();
                        RefreshList();
                        m_iSelectedEntry = -1;
                    }

                    ImGui::SameLine();
                    if( ImGui::Button( "Cancel", ImVec2( 120, 0 ) ) )
                    {
                        ImGui::CloseCurrentPopup();
                        m_iSelectedEntry = -1;
                    }

                    ImGui::EndPopup();
                }
                */

                ImGui::EndChild();
                ImGui::EndPopup();

                if( bSelected ) 
                {
                    return true;
                }
            }

            return false;
        }

        gb_blueprint::master& getSelected( void )
        {
            return *m_pSelected; //*m_List[ m_iSelectedEntry ];
        }

    protected:

        struct folder
        {
            xstring                             m_Name;
            xvector<folder>                     m_Folders       {};
            xvector<gb_blueprint::master*>      m_Bluesprints   {};
        };

    protected:

        void RefreshList( void )
        {
//            m_List.DeleteAllEntries();

            m_pSelected = nullptr;
            m_Folder.m_Name = X_STR("");
            m_Folder.m_Folders.DeleteAllEntries();
            m_Folder.m_Bluesprints.DeleteAllEntries();

            x_lk_guard_as_const_get_as_const( m_pBase->m_BlueprintDB.m_Vector, BlueprintLinearVector );

            for( const auto& pEntry : BlueprintLinearVector )
            {
                auto& Blueprint     = *pEntry->m_pEntry;
                auto& Path          = Blueprint.getPath();
                auto  iCur          = 0;
                int   iLast         = x_strstr<xchar>( &Path[iCur], "/" );
                auto  pFolder       = &m_Folder;

                // Check if it does not have a folder
                if( iLast < 2 )
                {
                    iLast = Path.getLength().m_Value;
                    if( iLast == 1 ) 
                    {
                        x_assert( Path[0] == '/' );
                        iLast = iCur;
                    }
                    else if( iLast == 0 ) iLast = iCur;
                }

                // Find Folder
                while( iCur != iLast )
                {
                    bool bFound = false;

                    // Search all the folders inside this folder
                    for( auto& Folder : pFolder->m_Folders )
                    {
                        if( 0 == x_strncmp<xchar>( Folder.m_Name, &Path[iCur], iLast - iCur) )
                        {
                            bFound  = true;
                            pFolder = &Folder;
                            break;
                        }
                    }

                    // Add new folder if we did not find the path
                    if( !bFound )
                    {
                        auto& NewFolder = pFolder->m_Folders.append();
                        NewFolder.m_Name.Copy( &Path[iCur] );
                        NewFolder.m_Name[ iLast - iCur ] = 0;
                        pFolder = &NewFolder;
                    }

                    // Get ready for next folder
                    if(Path[iLast]) for( iCur = ++iLast; Path[iLast] && Path[iLast]!='/'; iLast++ );
                    else break;
                }

                // Add the blue print to the right folder
                pFolder->m_Bluesprints.append( &Blueprint ); 
            }
        }


    protected:

        gb_blueprint::master*                           m_pSelected { nullptr };
        folder                                          m_Folder            {};
        ImGuiTextFilter                                 m_Filter;
        gb_game_graph::base*                            m_pBase{ nullptr };
        mode                                            m_Mode;
        bool                                            m_bCreated = false;
    };
}