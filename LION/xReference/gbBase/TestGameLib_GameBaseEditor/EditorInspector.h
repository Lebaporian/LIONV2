//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

class entity_property_window : protected imgui_xproperty::window
{
    x_object_type( entity_property_window, rtti( imgui_xproperty::window) );

public:
    
    entity_property_window( void )
    {
        m_WindowName = X_STR("Inspector");
    }

    void SetProperty( gb_component::entity* pEntity )
    {
        m_pEntity = pEntity; 
        if( m_pEntity ) t_parent::SetProperty( reinterpret_cast<xproperty_v2::base*>(0xffffffffffffffff) );
        else            t_parent::SetProperty( nullptr );
        m_ItemChanges.DeleteAllEntries();
        m_ProCollection.DeleteAllEntries();
        RefreshTree();
    }

    void                    Update              ( void ) { imgui_xproperty::window::Update(); }
    void                    Render              ( void ) { imgui_xproperty::window::Render(); }
    gb_component::entity*   getEntity           ( void ) { return m_pEntity; }

protected:
    
    class entity_item : public imgui_property::item_topfolder<>
    {
    public:
        x_object_type( entity_item, rtti(imgui_property::item_topfolder<>), is_not_copyable, is_not_movable )
        xstring                 m_BackupHelp    {};

    public:

        entity_item     ( imgui_property::window& W ) : t_parent( W ) {}

        virtual void onGadgetRender( void )
        {
            ImGui::AlignFirstTextHeightToWidgets();

            const ImGuiStyle *  style               = &ImGui::GetStyle();
            ImGui::PushStyleColor(ImGuiCol_Button, style->Colors[ImGuiCol_Header]);

            auto&                   Win         =   m_Window.SafeCast<entity_property_window>();
            auto&                   Entity      =   *Win.getEntity();
            gb_component::base&     Comp        =   [&]() -> gb_component::base& 
                                                    {
                                                        gb_component::base* pComp       = nullptr;
                                                        int                 i           = 0;
                                                        const int           iTopFolder  = Win.getTopFolderIndex( this );
                                                        
                                                        x_assert( iTopFolder != -1 );
                                                        for( pComp = &Entity; i != iTopFolder; pComp = pComp->getNextComponent(), i++ )
                                                        {
                                                            x_assert(pComp);
                                                        }

                                                        return *pComp;
                                                    }();
            //
            // Deal with the active button
            //
            bool bActive = Comp.isActive();
            bool Press;

            if( m_BackupHelp.isEmpty() )
            {
                m_BackupHelp = m_Help;
            }
            m_Help = X_STR("");
            if( bActive ) 
            {
                Press = ImGui::Button( ICON_FA_CHECK_SQUARE_O, ImVec2( 13,13 ) );  ImGui::SameLine();
                if (ImGui::IsItemHovered())
                {
                    /*
                    ImGui::BeginTooltip();
                    ImGui::PushTextWrapPos(450.0f);
                    ImGui::TextUnformatted("Activated Component\nComponent will be executed when the game runs");
                    ImGui::PopTextWrapPos();
                    ImGui::EndTooltip();
                    */
                    m_Window.UpdateSelected ( this );
                    m_Help.Copy("Activated Component\nComponent will be executed when the game runs");
                }
            }
            else 
            {
                Press = ImGui::Button( ICON_FA_SQUARE_O, ImVec2( 13,13 ) );  ImGui::SameLine();
                if (ImGui::IsItemHovered())
                {
                    /*
                    ImGui::BeginTooltip();
                    ImGui::PushTextWrapPos(450.0f);
                    ImGui::TextUnformatted("Deactivated Component\nComponent does not execute when the game runs");
                    ImGui::PopTextWrapPos();
                    ImGui::EndTooltip();
                    */
                    m_Window.UpdateSelected ( this );
                    m_Help.Copy("Deactivated Component\nComponent does not execute when the game runs");
                }
            }

            //
            // Deal with the readiness of a component
            //
            xvector<xstring> WarningList;
            auto Error = Comp.linearCheckResolve( WarningList );
            if( Error.isError() ) ImGui::TextColored( ImColor(170,0,0,255), "Error" ); 
            else                  ImGui::TextColored( ImColor(0,255,0,255), "R" );
            ImGui::SameLine(); 
            if (ImGui::IsItemHovered())
            {
                m_Window.UpdateSelected ( this );
                if( Error.isError() )
                {
                    m_Help.Copy( Error.getString() );
                }
                else
                {
                    m_Help.Copy("Ready Component\nThis component has all the requirements needed to work property");
                }
            }

            //
            // Component menu
            //
            if( Press )  Comp.setActive( !bActive );
            ImGui::SameLine();

            //
            // Deal with the menu button
            //
            ImGui::PushItemWidth(-1);
            const char* pDisplay = "Component";
            if( Comp.isEntity() ) pDisplay = "Entity"; 
            ImGui::Button( pDisplay, ImVec2(ImGui::GetContentRegionAvailWidth(), 0 ) ); 

            ImGui::PopStyleColor();
            ImGui::PopItemWidth();

            if( ImGui::IsItemActive() )
                ImGui::OpenPopup("##piepopup");

            const char* Entityitems[]    = { "Paste", "Copy", "Paste New", "Add New", "Delete"  };
            const char* Componentitems[] = { "Paste", "Copy", "Move Down", "Move Up", "Delete"  };
            static int selected = -1;

            const ImVec2 Pos = ImGui::GetIO().MouseClickedPos[0];
            int n = -1;
            if( Comp.isEntity() )   n = ImGui::PiePopupSelectMenu( Pos, "##piepopup", Entityitems, static_cast<int>(x_countof(Entityitems)), &selected );
            else                    n = ImGui::PiePopupSelectMenu( Pos, "##piepopup", Componentitems, static_cast<int>(x_countof(Componentitems)), &selected );


            if( m_Help[0] == 0 ) m_Help = m_BackupHelp;
        }
    };


    struct const_data_window
    {
        const_data_window( entity_property_window& MainWindow, imgui_property::base_item& BaseItem ) : m_MainWindow{ MainWindow }, m_BaseItem{BaseItem} {}
        void Render( void )
        {
            if(m_JustOpen) 
            {
                ImGui::OpenPopup( "Options" );
                m_JustOpen = false;
            }

            ImGui::SetNextWindowSizeConstraints(ImVec2(400, 200), ImVec2(400, 200)); 
            
            if( ImGui::BeginPopup("Options") )
            {
                if( ImGui::Button("Constant Data Editor", ImVec2( -1, 0 ) ) )
                    m_Open = false;
                    
                ImGui::Separator();
                m_Filter.Draw("Filter", -100.0f);
                ImGui::Separator();
                ImGui::BeginChild("item view");

                xuptr Index = 0;
                if (m_Filter.IsActive())
                {
                    for( auto& pConstData : x_iter_ref( Index, m_List ) )
                    {
                        const char* pName = pConstData->getEditorName();
                        const char* pEnd  = &pName[ x_strlen(pName).m_Value ];
                        if( m_Filter.PassFilter( pName, pEnd ) )
                        {
                            ImGui::Bullet();
                            if( ImGui::Selectable( pConstData->getEditorName() ) )
                            {
                                auto& Item = m_BaseItem.SafeCast<imgui_xproperty::item_guid>();
                                Item.m_Value = pConstData->getGuid().m_Value;
                                m_MainWindow.Update();
                                m_Open = false;
                            }
                        }
                    }
                }
                else
                {
                    for( auto& pConstData : x_iter_ref( Index, m_List ) )
                    {
                        ImGui::Bullet();
                        if( ImGui::Selectable( pConstData->getEditorName() ) )
                        {
                            auto& Item = m_BaseItem.SafeCast<imgui_xproperty::item_guid>();
                            Item.m_Value = pConstData->getGuid().m_Value;
                            m_MainWindow.Update();
                            m_Open = false;
                        }
                    }
                }
                ImGui::EndChild();
                ImGui::EndPopup();
            }
        }

        imgui_property::base_item&          m_BaseItem;
        entity_property_window&             m_MainWindow;
        xvector<gb_component::const_data*>  m_List;
        ImGuiTextFilter                     m_Filter;
        bool                                m_Open          { true };
        bool                                m_JustOpen      { true };
    };

protected:

    //------------------------------------------------------------------------------------------------------
    virtual xowner<imgui_property::item_topfolder<>*> onAddTopNode( void ) override
    {
        return x_new( entity_item, { *this } );
    }

    //------------------------------------------------------------------------------------------------------
    
    virtual void PropQuerySet( xfunction_view<const xproperty_v2::entry*(int Index)> Function ) override
    {
        return m_pEntity->linearPropSetV2( Function );
    }

    //------------------------------------------------------------------------------------------------------
    
    virtual void PropQueryGet( xfunction_view<xproperty_v2::entry*(int Index)> Function ) const override
    {
        return m_pEntity->linearPropGetV2( Function );
    }

    //------------------------------------------------------------------------------------------------------
    
    virtual void PropEnum( xfunction_view<xproperty_v2::entry&(void)> Function, bool bFullPath = true, xproperty_v2::base::enum_mode Mode  = xproperty_v2::base::enum_mode::REALTIME ) const override
    {
        m_pEntity->linearPropEnumV2( Function, bFullPath, Mode );
    }

    //------------------------------------------------------------------------------------------------------
    
    virtual void onRender( void ) override
    {
        t_parent::onRender();
        if( m_ConstDataWindow.isValid() )
        {
            if( m_ConstDataWindow->m_Open == false ) m_ConstDataWindow.Delete();
            else                                     m_ConstDataWindow->Render();
        }
    }

    //------------------------------------------------------------------------------------------------------
    virtual void onNotifyGuidClick( imgui_property::base_item& BaseItem ) override
    {
        auto& ItemGuid = BaseItem.SafeCast<imgui_xproperty::item_guid>();
        auto& PropData = ItemGuid.getUserData<imgui_xproperty::prop_data>();

        if( x_strcmp( ItemGuid.m_Category.m_pValue, "ConstantData/*" ) == 0 )
        {
            auto& GameMgr = m_pEntity->getGlobalInterface().m_GameMgr;

            m_ConstDataWindow.New( *this, BaseItem );
            x_lk_guard_as_const_get_as_const( GameMgr.m_ConstDataDB.m_Vector, ConstantDataDB );
            for( auto& Node : ConstantDataDB )
            {
                gb_component::const_data& Entry = *Node->m_pEntry;

                if( Entry.getType().getGuid().m_Value != ItemGuid.m_Type )
                    continue;

                // append the entry into the list
                m_ConstDataWindow->m_List.append()          = &Entry;
            }

            // Remember which item we are dealing with
            if( m_ConstDataWindow->m_List.getCount() == 0 ) m_ConstDataWindow.Delete();
        }
    }

protected:

    gb_component::entity*           m_pEntity { nullptr };
    xndptr_s<const_data_window>     m_ConstDataWindow;
};
