//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

class blueprint_panel
{
public:

    struct folder
    {
        xstring                             m_Name;
        xvector<folder>                     m_Folders       {};
        xvector<gb_blueprint::master*>      m_Bluesprints   {};
    };

    xvector<gb_blueprint::master*>      m_Select            {};
    folder                              m_Folder            {};
    gb_blueprint::master*               m_pSelected;

    void UhselectAll( void )
    {
        m_Select.DeleteAllEntries();
        m_pSelected = nullptr;
    }

    void setup( gb_game_graph::base& Base )
    {
        m_pBase = &Base;
        m_msgBlueprint.Connect( Base.m_eventNewBlueprint );
    }

    void Refresh( void )
    {
        m_pSelected = nullptr;
        m_Folder.m_Name = X_STR("");
        m_Folder.m_Folders.DeleteAllEntries();
        m_Folder.m_Bluesprints.DeleteAllEntries();

        x_lk_guard_as_const_get_as_const( m_pBase->m_BlueprintDB.m_Vector, BlueprintLinearVector );

        for( const auto& pEntry : BlueprintLinearVector )
        {
            auto& Blueprint     = *pEntry->m_pEntry;
            auto& Path          = Blueprint.getPath();
            auto  iCur          = 0;
            int   iLast         = x_strstr<xchar>( &Path[iCur], "/" );
            auto  pFolder       = &m_Folder;

            // Check if it does not have a folder
            if( iLast < 2 )
            {
                iLast = Path.getLength().m_Value;
                if( iLast == 1 ) 
                {
                    x_assert( Path[0] == '/' );
                    iLast = iCur;
                }
                else if( iLast == 0 ) iLast = iCur;
            }

            // Find Folder
            while( iCur != iLast )
            {
                bool bFound = false;

                // Search all the folders inside this folder
                for( auto& Folder : pFolder->m_Folders )
                {
                    if( 0 == x_strncmp<xchar>( Folder.m_Name, &Path[iCur], iLast - iCur) )
                    {
                        bFound  = true;
                        pFolder = &Folder;
                        break;
                    }
                }

                // Add new folder if we did not find the path
                if( !bFound )
                {
                    auto& NewFolder = pFolder->m_Folders.append();
                    NewFolder.m_Name.Copy( &Path[iCur] );
                    NewFolder.m_Name[ iLast - iCur ] = 0;
                    pFolder = &NewFolder;
                }

                // Get ready for next folder
                if(Path[iLast]) for( iCur = ++iLast; Path[iLast] && Path[iLast]!='/'; iLast++ );
                else break;
            }

            // Add the blue print to the right folder
            pFolder->m_Bluesprints.append( &Blueprint ); 
        }
    }

    void Render( void )
    {
        static bool Open1 = true;
        xstring     Temp;

        if( ImGui::BeginDock( ICON_FA_CODEPEN " Blueprint", &Open1 ) )
        {
            // Temporary storage of what node we have clicked to process selection at the end of the loop. May be a pointer to your own node type, etc.
            bool bSelected = false;

            //ImGui::Button( "Test" );
            //ImGui::Separator();
            ImGui::BeginChild("scene view");

            //
            // List of entities
            //
            if ( ImGui::TreeNodeEx( "Blueprints", ImGuiTreeNodeFlags_DefaultOpen ) )
            {
                xvector<std::pair<folder*,int>> m_Stack;
                auto*   pFolder = &m_Folder;
                int     i=0;
                do
                {
                    AGAIN:
                    for( ; i < pFolder->m_Folders.getCount(); i++ )
                    {
                        if( ImGui::TreeNode(pFolder->m_Folders[i].m_Name) )
                        {
                            // Push current situation in the stack
                            m_Stack.append(std::pair<folder*,int>{pFolder, i+1} );

                            pFolder = &pFolder->m_Folders[i];
                            i = 0;
                            goto AGAIN;
                        }
                    }

                    for( auto pBlueprint : pFolder->m_Bluesprints )
                    {
                        const ImGuiTreeNodeFlags node_flags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick | (m_pSelected == pBlueprint ? ImGuiTreeNodeFlags_Selected : 0);
                        ImGui::TreeNodeEx( pBlueprint, node_flags | ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen, ICON_FA_CODEPEN " %s", (const char*)pBlueprint->getName() );

                        if (ImGui::IsItemClicked())
                        {
                            m_pSelected = pBlueprint;
                            bSelected = true;
                            /*
                            if( m_Select.getCount() )
                            {
                                if( m_Select[0] != m_pSelected )
                                {
                                    bSelected = true;
                                    m_pSelected = pBlueprint;
                                }
                            }
                            else
                            {
                                bSelected = true;
                                m_pSelected = pBlueprint;
                            }
                            */
                        }
                    }
                
                    // Done with this tree
                    ImGui::TreePop();
                    int iCur = m_Stack.getCount<int>()-1;

                    // Nothing else to do then just be done
                    if( iCur == -1 ) break;

                    // continue with the previous folder
                    pFolder = m_Stack[iCur].first;
                    i       = m_Stack[iCur].second;

                    // Remove entry from the stack
                    m_Stack.DeleteWithSwap( iCur );

                } while( true );
            }
            ImGui::EndChild();

            //
            // Deal with selection
            //
            if( bSelected )
            {
                // Update selection state. Process outside of tree loop to avoid visual inconsistencies during the clicking-frame.
                if( ImGui::GetIO().KeyCtrl )
                {
                    // CTRL+click to toggle
                    m_Select.append( m_pSelected ); 
                }
                else
                {
                    // Select entity
                    m_Select.DeleteAllEntries();
                    m_Select.append( m_pSelected ); 
                }
            }
        }
        ImGui::EndDock();
    }

protected:

    void msgNewBluePrint( gb_blueprint::master* pBlueprint )
    {
        if( m_pBase ) Refresh();
    }

protected:

    gb_game_graph::base*                m_pBase             { nullptr };
    x_message::delegate<blueprint_panel,gb_blueprint::master*>         m_msgBlueprint{ *this, &blueprint_panel::msgNewBluePrint };

};