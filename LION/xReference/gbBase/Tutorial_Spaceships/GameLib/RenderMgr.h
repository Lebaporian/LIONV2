
namespace my_game
{
    class render_mgr final : public gb_component_mgr::base
    {
        x_object_type( render_mgr, is_quantum_lock_free, rtti(gb_component_mgr::base), is_not_copyable, is_not_movable )

    public: 
    
        struct events
        {
            x_message::event<eng_draw&, const eng_view&> m_DebugRender;
        
        } m_Events;

    public: 
                                render_mgr                  ( gb_game_graph::base& GameGraph ) : 
                                    gb_component_mgr::base
                                    {
                                        guid{"RenderMgr"},
                                        GameGraph,
                                        X_WSTR("RenderMgr")
                                    } { setupAffinity( affinity::AFFINITY_MAIN_THREAD ); }
        void                    InitEngine                  ( HINSTANCE hInstance = GetModuleHandle(NULL) );
        auto&                   getWindow                   ( void ) { return *m_pWindow; }

    protected:

        virtual         void                    onExecute               ( void )                noexcept override;
        x_incppfile     void                    FrameBegin              ( void )                noexcept;
        x_incppfile     void                    FrameEnd                ( void )                noexcept;

    protected:

        xndptr_s<eng_instance>      m_Instance      {};
        eng_device*                 m_pDevice       { nullptr };
        eng_window*                 m_pWindow       { nullptr };
        eng_view                    m_View          {};

    protected:
        
    };
}