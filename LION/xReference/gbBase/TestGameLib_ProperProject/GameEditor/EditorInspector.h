//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

class entity_property_window : protected imgui_xproperty::window
{
    x_object_type( entity_property_window, rtti( imgui_xproperty::window) );

public:
    
    entity_property_window( void )
    {
        m_WindowName = X_STR("Inspector");
    }

    void setup( gb_component::entity* pEntity )
    {
        m_pEntity = pEntity; 
        m_ItemChanges.DeleteAllEntries();
        m_ProCollection.m_List.DeleteAllEntries();
        RefreshTree();
    }

    void    Update              ( void ) { imgui_xproperty::window::Update(); }
    void    Render              ( void ) { imgui_xproperty::window::Render(); }
    gb_component::entity*    getEntity( void ) { return m_pEntity; }

protected:
    
    class entity_item : public imgui_property::item_topfolder<>
    {
    public:
        x_object_type( entity_item, rtti(imgui_property::item_topfolder<>), is_not_copyable, is_not_movable )
        xstring m_BackupHelp;

    public:

        entity_item     ( imgui_property::window& W ) : t_parent( W ) {}

        virtual void onGadgetRender( void )
        {
            ImGui::AlignFirstTextHeightToWidgets();

            const ImGuiStyle *  style               = &ImGui::GetStyle();
            ImGui::PushStyleColor(ImGuiCol_Button, style->Colors[ImGuiCol_Header]);

            auto&                   Win         = m_Window.SafeCast<entity_property_window>();
            auto&                   Entity      = *Win.getEntity();
            gb_component::base*     pComp       = &Entity;
            
            for( pComp = &Entity; pComp; pComp = pComp->getNextComponent() )
            {
                if( x_strstr<xchar>( pComp->getType().getCategoryName().m_pValue, m_Name ) ) break;
            }

            x_assert( pComp );

            //
            // Deal with the active button
            //
            bool bActive = pComp->isActive();
            bool Press;

            if( m_BackupHelp.isEmpty() )
            {
                m_BackupHelp = m_Help;
            }
            m_Help = X_STR("");
            if( bActive ) 
            {
                Press = ImGui::Button( ICON_FA_CHECK_SQUARE_O, ImVec2( 13,13 ) );  ImGui::SameLine();
                if (ImGui::IsItemHovered())
                {
                    /*
                    ImGui::BeginTooltip();
                    ImGui::PushTextWrapPos(450.0f);
                    ImGui::TextUnformatted("Activated Component\nComponent will be executed when the game runs");
                    ImGui::PopTextWrapPos();
                    ImGui::EndTooltip();
                    */
                    m_Window.UpdateSelected ( this );
                    m_Help.Copy("Activated Component\nComponent will be executed when the game runs");
                }
            }
            else 
            {
                Press = ImGui::Button( ICON_FA_SQUARE_O, ImVec2( 13,13 ) );  ImGui::SameLine();
                if (ImGui::IsItemHovered())
                {
                    /*
                    ImGui::BeginTooltip();
                    ImGui::PushTextWrapPos(450.0f);
                    ImGui::TextUnformatted("Deactivated Component\nComponent does not execute when the game runs");
                    ImGui::PopTextWrapPos();
                    ImGui::EndTooltip();
                    */
                    m_Window.UpdateSelected ( this );
                    m_Help.Copy("Deactivated Component\nComponent does not execute when the game runs");
                }
            }

            //
            // Deal with the readiness of a component
            //
            xvector<xstring> WarningList;
            auto Error = pComp->LinearCheckResolve( WarningList );
            if( Error.isError() ) ImGui::TextColored( ImColor(170,0,0,255), "Error" ); 
            else                  ImGui::TextColored( ImColor(0,255,0,255), "R" );
            ImGui::SameLine(); 
            if (ImGui::IsItemHovered())
            {
                m_Window.UpdateSelected ( this );
                if( Error.isError() )
                {
                    m_Help.Copy( Error.getString() );
                }
                else
                {
                    m_Help.Copy("Ready Component\nThis component has all the requirements needed to work property");
                }
            }

            //
            // Component menu
            //
            if( Press )  pComp->setActive( !bActive );
            ImGui::SameLine();

            //
            // Deal with the menu button
            //
            ImGui::PushItemWidth(-1);
            ImGui::Button( "Menu", ImVec2(ImGui::GetContentRegionAvailWidth(), 0 ) ); 

            ImGui::PopStyleColor();
            ImGui::PopItemWidth();

            if( ImGui::IsItemActive() )
                ImGui::OpenPopup("##piepopup");

            const char* Entityitems[]    = { "Paste", "Copy", "Paste New", "Add New", "Delete"  };
            const char* Componentitems[] = { "Paste", "Copy", "Move Down", "Move Up", "Delete"  };
            static int selected = -1;

            const ImVec2 Pos = ImGui::GetIO().MouseClickedPos[0];
            int n = -1;
            if( pComp == &Entity ) n = ImGui::PiePopupSelectMenu( Pos, "##piepopup", Entityitems, static_cast<int>(x_countof(Entityitems)), &selected );
            else                   n = ImGui::PiePopupSelectMenu( Pos, "##piepopup", Componentitems, static_cast<int>(x_countof(Componentitems)), &selected );


            if( m_Help[0] == 0 ) m_Help = m_BackupHelp;
        }
    };

protected:

    //------------------------------------------------------------------------------------------------------
    virtual xowner<imgui_property::item_topfolder<>*> onAddTopNode( void ) override
    {
        return x_new( entity_item, { *this } );
    }

    //------------------------------------------------------------------------------------------------------
    
    virtual bool PropQuerySet( xfunction<bool( xproperty_query& Query, xproperty_data& PropertyToQuery )> Function ) override
    {
        return m_pEntity->LinearPropSet( Function );
    }

    //------------------------------------------------------------------------------------------------------
    
    virtual bool PropQueryGet( xfunction<bool( xproperty_query& Query, xproperty_data& PropertyToQuery )> Function ) const override
    {
        return m_pEntity->LinearPropGet( Function );
    }

    //------------------------------------------------------------------------------------------------------
    
    virtual void PropEnum( xfunction<void( xproperty_enum&  Enum,  xproperty_data& Data)> Function, bool bFullPath = true, xproperty_enum::mode Mode  = xproperty_enum::mode::REALTIME ) const override
    {
        m_pEntity->LinearPropEnum( Function, bFullPath, Mode );
    }

protected:

    gb_component::entity* m_pEntity { nullptr };
};
