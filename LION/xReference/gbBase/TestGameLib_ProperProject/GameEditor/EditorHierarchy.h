//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

class entity_hierarchy
{
public:

    gb_game_graph::base*                m_pBase             { nullptr };
    xvector<gb_component::entity*>      m_Select            {};
    xvector<gb_component::entity*>      m_InWorldEntities   {};
    xvector<gb_component::entity*>      m_OutWorldEntities  {};


    void Refresh( void )
    {
        m_InWorldEntities.DeleteAllEntries();
        m_OutWorldEntities.DeleteAllEntries();

        for( auto& pEntity : m_pBase->m_EntityDB )
        {
            if( pEntity->isInWorld() ) m_InWorldEntities.append()  =  pEntity;
            else                       m_OutWorldEntities.append() =  pEntity;
        }
    }

    void Render( void )
    {
        static bool Open1 = true;
        xstring     Temp;

        if( ImGui::BeginDock( "Hierarchy", &Open1 ) )
        {
            // Temporary storage of what node we have clicked to process selection at the end of the loop. May be a pointer to your own node type, etc.
            gb_component::entity* pSelected = nullptr;

            //
            // List of entities
            //
            if (ImGui::TreeNode("World"))
            {
                if( m_pBase )
                {
                    for( auto& pEntity : m_InWorldEntities )
                    {
                        const ImGuiTreeNodeFlags node_flags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick | (pEntity->isEditorSelected() ? ImGuiTreeNodeFlags_Selected : 0);
                    
                        // Leaf: The only reason we have a TreeNode at all is to allow selection of the leaf. Otherwise we can use BulletText() or TreeAdvanceToLabelPos()+Text().
                        pEntity->getGuid().getStringHex( Temp );  
                        if( pEntity->isKindOf<gb_component::entity_static>() )        ImGui::TreeNodeEx( pEntity, node_flags | ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen, ICON_FA_UNIVERSITY " %s(%s)", pEntity->getType().getCategoryName(), Temp );
                        else if( pEntity->isKindOf<gb_component::entity_dynamic>() )  ImGui::TreeNodeEx( pEntity, node_flags | ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen, ICON_FA_LEAF " %s(%s)", pEntity->getType().getCategoryName(), Temp );
                        else                                                          ImGui::TreeNodeEx( pEntity, node_flags | ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen, ICON_FA_COGS " %s(%s)", pEntity->getType().getCategoryName(), Temp );
                        
                        if (ImGui::IsItemClicked()) 
                            pSelected = pEntity;
                    }
                }

                ImGui::TreePop();
            }

            //
            // Entities that are not in the world
            //
            if (ImGui::TreeNode("Outside World"))
            {
                if( m_pBase )
                {
                    for( auto& pEntity : m_OutWorldEntities )
                    {
                        const ImGuiTreeNodeFlags node_flags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick | (pEntity->isEditorSelected() ? ImGuiTreeNodeFlags_Selected : 0);
                    
                        // Leaf: The only reason we have a TreeNode at all is to allow selection of the leaf. Otherwise we can use BulletText() or TreeAdvanceToLabelPos()+Text().
                        pEntity->getGuid().getStringHex( Temp );                    
                        if( pEntity->isKindOf<gb_component::entity_static>() )        ImGui::TreeNodeEx( pEntity, node_flags | ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen, ICON_FA_UNIVERSITY " %s(%s)", pEntity->getType().getCategoryName(), Temp );
                        else if( pEntity->isKindOf<gb_component::entity_dynamic>() )  ImGui::TreeNodeEx( pEntity, node_flags | ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen, ICON_FA_LEAF " %s(%s)", pEntity->getType().getCategoryName(), Temp );
                        else                                                          ImGui::TreeNodeEx( pEntity, node_flags | ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen, ICON_FA_COGS " %s(%s)", pEntity->getType().getCategoryName(), Temp );
                        if (ImGui::IsItemClicked()) 
                            pSelected = pEntity;
                    }
                }

                ImGui::TreePop();
            }
            
            //
            // Deal with selection
            //
            if( pSelected )
            {
                // Update selection state. Process outside of tree loop to avoid visual inconsistencies during the clicking-frame.
                if( ImGui::GetIO().KeyCtrl )
                {
                    // CTRL+click to toggle
                    pSelected->setEditorSelect( true );
                    m_Select.append( pSelected ); 
                }
                else
                {
                    // Remove all the selected entities so far
                    for( auto& pS : m_Select )
                    {
                        pS->setEditorSelect( false );
                    }

                    // Select entity
                    m_Select.DeleteAllEntries();
                    pSelected->setEditorSelect( true );
                    m_Select.append( pSelected ); 
                }
            }

        }
        ImGui::EndDock();
    }

};