//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
class log_window
{
public:

    void    Clear()     
    {
        x_lk_guard( m_Lock ); 
        Buf.clear(); 
        LineOffsets.clear(); 
    }

    void    AddLog(const char* fmt, ...)
    {
        x_lk_guard( m_Lock );
        int old_size = Buf.size();
        va_list args;
        va_start(args, fmt);
        Buf.appendv(fmt, args);
        va_end(args);
        for (int new_size = Buf.size(); old_size < new_size; old_size++)
            if (Buf[old_size] == '\n')
                LineOffsets.push_back(old_size);
        ScrollToBottom = true;
    }

    void    Draw(const char* title, bool* p_open = NULL)
    {
        x_lk_guard( m_Lock );
        if( ImGui::BeginDock( title, p_open ) )
        {
            if (ImGui::Button("Clear")) Clear();
            ImGui::SameLine();
            bool copy = ImGui::Button("Copy");
            ImGui::SameLine();
            Filter.Draw("Filter", -100.0f);
            ImGui::Separator();
            ImGui::BeginChild("scrolling", ImVec2(0,0), false, ImGuiWindowFlags_HorizontalScrollbar);
            if (copy) ImGui::LogToClipboard();

            if (Filter.IsActive())
            {
                const char* buf_begin = Buf.begin();
                const char* line = buf_begin;
                for (int line_no = 0; line != NULL; line_no++)
                {
                    const char* line_end = (line_no < LineOffsets.Size) ? buf_begin + LineOffsets[line_no] : NULL;
                    if (Filter.PassFilter(line, line_end))
                        ImGui::TextUnformatted(line, line_end);
                    line = line_end && line_end[1] ? line_end + 1 : NULL;
                }
            }
            else
            {
                ImGui::TextUnformatted(Buf.begin());
            }

            if (ScrollToBottom)
                ImGui::SetScrollHere(1.0f);
            ScrollToBottom = false;
            ImGui::EndChild();
        }
        ImGui::EndDock();
    }

protected:

    using lock = x_lk_spinlock::base<x_lk_spinlock::non_reentrance>;

protected:

    lock                m_Lock;
    ImGuiTextBuffer     Buf;
    ImGuiTextFilter     Filter;
    ImVector<int>       LineOffsets;        // Index to lines offset
    bool                ScrollToBottom;
};
