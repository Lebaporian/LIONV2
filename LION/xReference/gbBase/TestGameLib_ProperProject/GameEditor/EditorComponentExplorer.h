//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

class editor_component_explorer
{
public:

    void Render( gb_game_graph::base& GameGraph )
    {
        onRender( GameGraph );
    }

    void Refresh( gb_game_graph::base& GameGraph )
    {
        m_GroupList.DeleteAllEntries();
        m_SelectedGroup = m_SelectedType = ~0u;
        auto& List = GameGraph.m_CompTypeDB.getLinearListOfEntries();

        m_GroupList.append().m_Name = X_STR("Ungrouped");
        int Index = 0;
        for( auto& Entry : List )
        {
            const auto& TypeName = Entry->getCategoryName();

            //
            // Find the right group
            //
            const bool bAppended = [&]
            {
                for( auto& Group : m_GroupList )
                {
                    for( int i = 0; Group.m_Name[i] == TypeName.m_pValue[i]; i++ )
                    {
                        if( Group.m_Name[i] == TypeName.m_pValue[i] && TypeName.m_pValue[i+1] == '/' )
                        {
                            auto& Type = Group.m_TypeList.append();
                            Type.m_Name.Copy( (const char*)&TypeName.m_pValue[i+2] );
                            Type.m_Guid = Entry->getGuid();
                            Type.m_Help.Copy( Entry->getHelp() );
                            return true;
                        }
                    }
                }
                return false;
            }();

            //
            // Did not find the group then we must create the group or insert it into the ungroup group
            //
            if( !bAppended )
            {
                // Get the index from where the name starts -1 if it did not find a group
                const int Index = [&]
                {
                    for( int i = 0; TypeName.m_pValue[i]; i++ )
                        if( TypeName.m_pValue[i] == '/' ) return i;
                    return -1;
                }();

                if( Index == -1 )
                {
                    // insert into the ungroup group
                    auto& Type = m_GroupList[0].m_TypeList.append();
                    Type.m_Name.Copy( &TypeName.m_pValue[Index+1] );
                    Type.m_Guid = Entry->getGuid();
                    Type.m_Help.Copy( Entry->getHelp() );
                }
                else
                {
                    auto& Group = m_GroupList.append();
                    Group.m_Name.Copy( TypeName.m_pValue );
                    Group.m_Name[Index] = 0;

                    auto& Type = Group.m_TypeList.append();
                    Type.m_Name.Copy( &TypeName.m_pValue[Index+1] );
                    Type.m_Guid = Entry->getGuid();
                    Type.m_Help.Copy( Entry->getHelp() );
                }
            }
        }
    }

protected:

    struct type_component
    {
        xstring                         m_Name {};
        gb_component::type_base::guid   m_Guid {};
        xstring                         m_Help {};
    };

    struct type_group
    {
        xstring                     m_Name { };
        xvector<type_component>     m_TypeList  { };
        bool                        m_bOpen     { true };
    };

protected:

    void onRender( gb_game_graph::base& GameGraph )
    {
        ImGui::SetNextWindowSize(ImVec2(500, 440), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("Component Explorer", &m_Open ))
        {
            // left panel (The componets)
            static int selected = 0;
            ImGui::BeginChild("left pane", ImVec2(150, 0), true);
            {
                ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(0,0));
                xuptr GroupID = 0;
                for( auto& Group : x_iter_ref( GroupID, m_GroupList ) )
                {
                    ImGui::AlignFirstTextHeightToWidgets();
                    if( ImGui::TreeNodeEx( Group.m_Name, ImGuiTreeNodeFlags_Framed ) )
                    {
                        xuptr ID = 0;
                        for( auto& Type : x_iter_ref( ID, Group.m_TypeList ) )
                        {
                            ImGui::PushID(static_cast<int>(ID));
                            ImGui::Bullet();
                            if( ImGui::Selectable( Type.m_Name, m_SelectedGroup == GroupID && m_SelectedType == ID ) )
                            {
                                m_SelectedGroup = GroupID;
                                m_SelectedType  = ID;
                            }
                            ImGui::PopID();
                        }
                        ImGui::TreePop();
                    }
                }
                ImGui::PopStyleVar();
            }
            ImGui::EndChild();
            ImGui::SameLine();

            // right
            ImGui::BeginGroup();
                ImGui::BeginChild("item view", ImVec2(0, ImGui::GetWindowHeight()/2-ImGui::GetItemsLineHeightWithSpacing())); // Leave room for 1 line below us
                    ImGui::TextWrapped("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ");
                ImGui::EndChild();
                ImGui::Separator();
                if (ImGui::Button("Add", ImVec2( 80, 0 ))) {}
                ImGui::SameLine();
                if (ImGui::Button("Remove", ImVec2( 80, 0 ))) {}
                ImGui::Separator();
                ImGui::BeginChild("item view2", ImVec2(0, -ImGui::GetItemsLineHeightWithSpacing())); // Leave room for 1 line below us
                    if( m_SelectedGroup == m_SelectedType && m_SelectedType == ~0u )
                    {
                        ImGui::TextWrapped("Select a component for help.");
                    }
                    else
                    {
                        ImGui::TextWrapped( m_GroupList[m_SelectedGroup].m_TypeList[m_SelectedType].m_Help );
                    }
                ImGui::EndChild();
                ImGui::BeginChild("buttons");
                    if (ImGui::Button("OK", ImVec2( 80, 0 ))) {}
                    ImGui::SameLine();
                    if (ImGui::Button("Cancel", ImVec2( 80, 0 ))) {}
                ImGui::EndChild();
            ImGui::EndGroup();
        }
        ImGui::End();
    }

protected:

    xvector<type_group>         m_GroupList     {};
    bool                        m_Open          { true };
    xuptr                       m_SelectedGroup = 0;
    xuptr                       m_SelectedType  = 0;
};