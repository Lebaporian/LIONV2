//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//------------------------------------------------------------------------------
// Global function. BE careful when you used them. Make sure you don't need to 
// use the class stead.
//------------------------------------------------------------------------------

void    x_srand     ( s32 Seed );
s32     x_rand      ( void );               
s32     x_irand     ( s32 Min, s32 Max );   
f32     x_frand     ( f32 Min=0, f32 Max=1 );   

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Description:
//     xrandom_small is a simple random number generator based on
//     George Marsaglia's MWC (multiply with carry) generator.
//     Although it is very simple, it passes Marsaglia's DIEHARD
//     series of random number generator tests.
//------------------------------------------------------------------------------
class xrandom_small
{
public:

    constexpr                   xrandom_small       ( void )                                noexcept = default;
    
    x_inline        void        setSeed64           ( u64 u )                               noexcept;
    x_inline        void        setSeed32           ( u32 u )                               noexcept;
    x_inline        u64         getSeed             ( void )                        const   noexcept;
            
                    
    x_inline        u32         Rand32              ( void )                                noexcept;
    x_inline        s32         Rand32              ( s32 Start, s32 End )                  noexcept;

    x_inline        f64         RandF64             ( void )                                noexcept;
    x_inline        f32         RandF32             ( void )                                noexcept;
    x_inline        f32         RandF32             ( f32 Start, f32 End )                  noexcept;
    x_inline        f64         RandF64             ( f64 Start, f64 End )                  noexcept;
            
    x_inline        f64         Normal              ( void )                                noexcept;
    x_inline        f64         Normal              ( f64 mean, f64 standardDeviation )     noexcept;
    x_inline        f64         Exponential         ( void )                                noexcept;
    x_inline        f64         Exponential         ( f64 mean )                            noexcept;
    x_inline        f64         Gamma               ( f64 shape, f64 scale )                noexcept;
    x_inline        f64         ChiSquare           ( f64 degreesOfFreedom )                noexcept;
    x_inline        f64         InverseGamma        ( f64 shape, f64 scale )                noexcept;
    x_inline        f64         Weibull             ( f64 shape, f64 scale )                noexcept;
    x_inline        f64         Cauchy              ( f64 median, f64 scale )               noexcept;
    x_inline        f64         StudentT            ( f64 degreesOfFreedom )                noexcept;
    x_inline        f64         Laplace             ( f64 mean, f64 scale )                 noexcept;
    x_inline        f64         LogNormal           ( f64 mu, f64 sigma )                   noexcept;
    x_inline        f64         Beta                ( f64 a, f64 b )                        noexcept;
    
protected:
    
    // These values are not magical, just the default values Marsaglia used.
    // Any pair of unsigned integers should be fine.
    u32         m_W { 521288629 };
    u32         m_Z { 362436069 };
};

//------------------------------------------------------------------------------
// Description:
//     This class is design to create random numbers. It is important to have your
//     own instance of this class when you want to create your own random numbers
//     base on a known seeded sequence. If you used the global functions such x_rand
//     you may get sequence random numbers which may lead to have issues while 
//     trying to recreate situation in the game such when debugging for instance.
//
//<P>  Most often you will just need the Rand32/FastRand32 or RandF32. Some of
//     other random function such the Gamma and Chi, etc. Are for different types
//     of distributions that may be use in things like tools.
//
//<P>  Also note that the class has an over head of about 2K. So don't over use it.
//
// Example:
// <CODE>
//      void main( void )
//      {
//          xrandom_large MyRndGenerator;
//
//          x_printf( " A number between 0 and 1 %f\n", MyRndGenerator.RandF32() );
//          x_printf( " A number between 0 and 100 %d\n", , MyRndGenerator.Rand32(0,100) );
//      }
// </CODE>
//------------------------------------------------------------------------------
class xrandom_large
{
public:
                xrandom_large   ( void )                    noexcept;
                xrandom_large   ( s32 Seed )                noexcept;
    void        Seed            ( s32 Seed )                noexcept;

    s32         Rand32          ( void )                    noexcept;
    s32         FastRand32      ( void )                    noexcept;
    s32         Rand32          ( s32 Start, s32 End )      noexcept;

    f64         RandF64         ( void )                    noexcept;
    f32         RandF32         ( void )                    noexcept;
    f32         RandF32         ( f32 Start, f32 End )      noexcept;
    f64         RandF64         ( f64 Start, f64 End )      noexcept;

    void        SetupVector2    ( xvector2& V )             noexcept;
    void        SetupVector3    ( xvector3d& V )            noexcept;

    f64         DiskF64         ( f64& xp, f64& yp )        noexcept;
    f32         DiskF32         ( f32& xp, f32& yp )        noexcept;
    f64         NormalF64       ( f64 Sigma, f64 Mean )     noexcept;
    f64         Exponential     ( f64 mu )                  noexcept;
    f64         Gamma           ( f64 a )                   noexcept;
    f64         Beta            ( f64 a, f64 b )            noexcept;
    f64         Chi             ( f64 nu )                  noexcept;
    f64         FDist           ( f64 nu1, f64 nu2 )        noexcept;
    f64         TDist           ( f64 nu )                  noexcept;
    s32         Geometric       ( f64 p )                   noexcept;
    s32         Binomial        ( f64 p, s32 trials )       noexcept;
    s32         Poisson         ( f64 mu )                  noexcept;
    
protected:

    enum
    {
        R	    = 48,
        LOGR	=  6,
        MAX_V   = 571
    };

protected:
    
    s32                         m_Feed;   
    s32                         m_Tap;    
    s32                         m_Borrow;
    s32                         m_Y;
    xarray<s32,R>               m_Vec;
    xarray<s32,MAX_V>           m_V;
};

