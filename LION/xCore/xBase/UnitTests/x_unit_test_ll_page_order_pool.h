//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

namespace x_unit_test { namespace ll_page_order_pool
{
    struct node
    {
        int x { 0 };
    };

    //-------------------------------------------------------------------------------------------

    inline void Test( void )
    {
        enum { MAX_ENTRIES = 100000 };
        x_ll_page_ordered_pool< node, MAX_ENTRIES > List;
   
        x_job_block     m_Block;
        xuptr           i = 0;

        //
        // Completness test
        //
        if( (0) )
        {
            for( int i=0; i<MAX_ENTRIES; i++ ) 
            {
                List.pop().x = i;
            }

            for( int i=0; i<MAX_ENTRIES; i++ ) 
            {
                for( auto& Node : List )
                {
                    x_assert(Node.x == i);
                    List.push( Node );
                    break;
                }
            }
        }

        //
        // Random test
        //
        if( 1 )
        {
            for( int i=0; i<10; i++ ) m_Block.SubmitJob( [&]()
            {
                node* pEntry=nullptr;

                for( int j = 0; j < (MAX_ENTRIES / 10); j++ )
                {
                    if( (rand()%4) == 0) 
                    {
                        pEntry = &List.pop();
                        pEntry->x = 1; 
                    } 
                    else List.pop().x = 1; 
                
                    if( (rand()%4) == 0 ) if(pEntry) { List.push( *pEntry ); pEntry = nullptr; }
                }
            } );


            m_Block.Join();
        }
        else
        {
            node* pEntry=nullptr;
            for( int i=0; i<MAX_ENTRIES; i++ ) 
            {
                if( (rand()%4) == 0) 
                {
                    pEntry = &List.pop();
                    pEntry->x = 1; 
                } 
                else List.pop().x = 1; 
                if( (rand()%4) == 0 ) if(pEntry) { List.push( *pEntry ); pEntry = nullptr; }
            }
        }

        // enumerates
        int t=0;
        for( auto& Node : List )
        {
            Node.x = t++;
        }

        // free
        for( auto& Node : List )
        {
            List.push( Node );
        }

        // check to make sure that there is nothing in it
        for( auto& Node : List )
        {
            x_assert(false);
        }

        int x = 0;   
    }

}}