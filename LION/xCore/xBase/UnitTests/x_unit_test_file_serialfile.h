//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once


namespace x_unit_test { namespace serialfile 
{

    //------------------------------------------------------------------------------
    // TYPES
    //------------------------------------------------------------------------------

    struct data1
    {
        void SerializeIO( xserialfile& SerialFile ) const
        {
            SerialFile.Serialize( m_A );
        }
        s16     m_A;
    };

    struct data3
    {
        void SerializeIO( xserialfile& SerialFile ) const
        {
            SerialFile.Serialize( m_Count );
            SerialFile.Serialize( m_Data, m_Count );
        }
    
        s32              m_Count;
        xdataptr<data1>  m_Data;
    };

    struct data2 : public data1
    {
        // data
        data3                   m_GoInStatic;
        data3                   m_DontDynamic;
        xarray<data3,8>         m_GoTemp;

        enum : u32
        {
            DYNAMIC_COUNT = (1024*1024)/sizeof(data1) + 4,
            STATIC_COUNT  = (1024*1024)/sizeof(data1) + 4,
            STATIC_MAX    = 0xffffffff
        };
    
        // Initialize all the data
        data2( void )
        {
            m_A = 100;
        
            m_DontDynamic.m_Count = DYNAMIC_COUNT;
            m_DontDynamic.m_Data.m_pPtr = x_malloc( data1, m_DontDynamic.m_Count );
            for( s32 i=0; i<m_DontDynamic.m_Count; i++ ) { m_DontDynamic.m_Data.m_pPtr[i].m_A = 22+i; }
        
            m_GoInStatic.m_Count = STATIC_COUNT;
            m_GoInStatic.m_Data.m_pPtr = x_malloc( data1, m_GoInStatic.m_Count );
            for( s32 i=0; i<m_GoInStatic.m_Count; i++ ) { m_GoInStatic.m_Data.m_pPtr[i].m_A = (s16)(100/(i+1)); }

            for ( data3& Temp : m_GoTemp )
            {
                Temp.m_Count = STATIC_COUNT;
                Temp.m_Data.m_pPtr = x_malloc( data1, Temp.m_Count );
                for ( s32 i = 0; i<Temp.m_Count; i++ ) { Temp.m_Data.m_pPtr[ i ].m_A = (s16)( 100 / ( i + 1 ) ); }
            }
        }
    
        // Sanity check
        void SanityCheck( void )
        {
            x_assert( m_A == 100 );
        
            x_assert( m_DontDynamic.m_Count == DYNAMIC_COUNT );
            for (s32 i = 0; i < m_DontDynamic.m_Count; i++)
            {
                if (m_DontDynamic.m_Data.m_pPtr[i].m_A != (s16)(22 + i))
                {
                    x_assert(m_DontDynamic.m_Data.m_pPtr[i].m_A == (s16)(22 + i));
                }            
            }
        
            x_assert( m_GoInStatic.m_Count == STATIC_COUNT );
            for( s32 i=0; i<m_GoInStatic.m_Count; i++ )
            {
                if (m_GoInStatic.m_Data.m_pPtr[i].m_A != (s16)(100 / (i + 1)))
                {
                    x_assert(m_GoInStatic.m_Data.m_pPtr[i].m_A == (s16)(100 / (i + 1)));
                }
            }

            for ( data3& Temp : m_GoTemp )
            {
                x_assert( Temp.m_Count == STATIC_COUNT );
                for ( s32 i = 0; i<Temp.m_Count; i++ )
                {
                    if ( Temp.m_Data.m_pPtr[ i ].m_A != (s16)( 100 / ( i + 1 ) ) )
                    {
                        x_assert( Temp.m_Data.m_pPtr[ i ].m_A == (s16)( 100 / ( i + 1 ) ) );
                    }
                }
            }
        }
    
        // Save the data
        void SerializeIO( xserialfile& SerialFile ) const
        {
            // Make sure that it is the first version
            SerialFile.setResourceVersion( 1 );
        
            // Tell the structure to save it self
            SerialFile.Serialize( m_GoInStatic );
        
            // Don't always need to go into structures
            SerialFile.Serialize ( m_DontDynamic.m_Count );
            SerialFile.Serialize( m_DontDynamic.m_Data, m_DontDynamic.m_Count, xserialfile::mem_type::MASK_UNIQUE );
        
            // Serialize the temp
            for ( const data3& Temp : m_GoTemp )
            {
                SerialFile.Serialize( Temp.m_Count );
                SerialFile.Serialize( Temp.m_Data, Temp.m_Count, xserialfile::mem_type::MASK_TEMP_MEMORY );
            }

            // Tell our parent to save it self
            data1::SerializeIO( SerialFile );
        }
    
        // This is the loading constructor by the time is call the file already loaded
        data2( xserialfile& SerialFile )
        {
            x_assert( SerialFile.getResourceVersion() == 1 );
        
            // *** Only reason to have a something inside this constructor is to deal with dynamic data
            // We move the memory to some other random place
            data1*  pData = x_malloc( data1, m_DontDynamic.m_Count );
            x_memcpy( pData, m_DontDynamic.m_Data.m_pPtr, m_DontDynamic.m_Count * sizeof(data1) );
        
            // Now we can overwrite the dynamic pointer without a worry
            x_free(m_DontDynamic.m_Data.m_pPtr);
            m_DontDynamic.m_Data.m_pPtr = pData;

            // make sure everything is ok
            SanityCheck();
        }

        // Only when saving we need to destroy separate static allocations
        void DestroyStaticStuff( void )
        {
            for ( data3& Temp : m_GoTemp )
            {
                if( Temp.m_Data.m_pPtr ) x_free( Temp.m_Data.m_pPtr );
            }

            if( m_GoInStatic.m_Data.m_pPtr ) x_free( m_GoInStatic.m_Data.m_pPtr );
        }
    
        ~data2( void )
        {
            // Here to deal with dynamic stuff
            if(m_DontDynamic.m_Data.m_pPtr) x_free( m_DontDynamic.m_Data.m_pPtr );
        }
    };


    //------------------------------------------------------------------------------
    // FUNCTIONS
    //------------------------------------------------------------------------------

    //------------------------------------------------------------------------------

    void Test( void )
    {
        xwstring     FileName( X_WSTR("temp:/SerialFile.bin") );
    
        // Save
        {
            xserialfile SerialFile;
            data2     TheData;
            TheData.SanityCheck();
            SerialFile.Save( FileName, TheData );
            TheData.DestroyStaticStuff();
        }
    
        // Load
        {
            xserialfile  SerialFile;
            data2*     pTheData;
        
            // This hold thing could happen in one thread
            SerialFile.Load( FileName, pTheData );
        
            // Okay one pointer to nuck
            x_delete( pTheData );
        }
    }

}}
