//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
// Basic functionality 
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------

namespace x_unit_test { namespace network { namespace udp_basic_peer_to_peer 
{
    x_constexprvar int                  PACKETS_TO_SENT = 500;

    struct node
    {
        bool        m_bRecived      { false };
        xstring     m_RecivedString {};
        xstring     m_SentString    {};
    };

    //-------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------
    // UDP TEST
    //-------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------

    struct computer_udp
    {
    public:

        using hsocket = xnet_udp_mesh::t_hsocket;
        using hclient = xnet_udp_mesh::t_hclient;
        
        struct type
        {
            const char* m_pString;
            const int   m_Length;
        };

        struct info
        {
            int                             m_SentCount         {0};
            int                             m_RecieveCount      {0};
            xarray<node,PACKETS_TO_SENT>    m_PacketInfo        {};
        };

    public:

        x_constexprvar int                                      PROTOCOL_COUNT = static_cast<int>( xnet_base_protocol::protocol::ENUM_COUNT );
        x_constexprvar xarray<type,PROTOCOL_COUNT>              s_Type
        {{
            { "", 0 },
            { "NonRealiable ID: ",  x_constStrLength( "NonRealiable ID: " ) },
            { "Reliable ID: ",      x_constStrLength( "Reliable ID: " )     },
            { "InOrder ID: ",       x_constStrLength( "InOrder ID: " )      },
        }};


        x_constexprvar xarray<xnet_address::port,4>             m_Ports             {{ xnet_address::port{4001}, {4002} }}; 
        hsocket                                                 m_hLocalSocket      {};
        xnet_udp_mesh                                           m_NetMesh           {};
        int                                                     m_TotalPacketsSent  { 0 };
        xarray<info,PROTOCOL_COUNT>                             m_Info              {};
        xnet_address                                            m_MyAddress         {};
        int                                                     m_PortIndex         {};
        bool                                                    m_isInitialize      { false };

    public:

        //-------------------------------------------------------------------------------
        virtual ~computer_udp( void ) = default;

        //-------------------------------------------------------------------------------
        computer_udp( int MyIndex )
        {
            m_PortIndex = MyIndex;
            m_MyAddress.setup( xnet_socket::getLocalIP4(), m_Ports[m_PortIndex] );

            // Create a socket to talk to the outside world
            m_hLocalSocket = m_NetMesh.openSocket( m_MyAddress.getPort() );
        
            // make sure it is valid
            x_assert( m_hLocalSocket.isValid() );
        }

        //-------------------------------------------------------------------------------

        virtual void Initialize( xbuffer_view<xbyte> )
        {
            // Create a client add the other computer as a client
            const hclient hClientID = m_NetMesh.CreateClient( { xnet_socket::getLocalIP4(), m_Ports[1 - m_PortIndex] } );
        
            // make sure it is valid
            x_assert( hClientID.isValid() );
        
            // Mark client as certified
            m_NetMesh.getClient( hClientID ).CertifyClient();

            m_isInitialize = true;
        }
    
        //-------------------------------------------------------------------------------
        
        void onRun( void )
        {
            xafptr<xbyte>   xPacket;
            const s32       AllocSize{ xnet_base_protocol::MAX_PACKET_SIZE };
        
            // Alloc the packet
            xPacket.Alloc( AllocSize );
        
            if( m_isInitialize == false )
            {
                Initialize( xPacket );
                return;
            }

            //
            // Send some data to our only client
            //
            if( m_NetMesh.getClientCount() )
            {
                int iChoose = (m_TotalPacketsSent++) % PROTOCOL_COUNT;
                if( iChoose == static_cast<int>(xnet_base_protocol::protocol::BASE) ) iChoose++; 

                auto& Info  = m_Info[iChoose];
                auto& Entry = Info.m_PacketInfo[Info.m_SentCount++];
                Entry.m_SentString.setup( xstring::t_characters{ 64 }, "%s%d", s_Type[iChoose].m_pString, Info.m_SentCount - 1 );

                switch( static_cast<xnet_base_protocol::protocol>(iChoose) )
                {
                    case xnet_base_protocol::protocol::NONRELIABLE:
                        m_NetMesh.getClient( 0 ).SendNonreliablePacket  ( m_hLocalSocket, xbuffer_view<xbyte>{ (xbyte*)&Entry.m_SentString[0], static_cast<xuptr>(Entry.m_SentString.getLength().m_Value + 1) } );    
                        break;
                    case xnet_base_protocol::protocol::RELIABLE: 
                    {
                        m_NetMesh.getClient( 0 ).SendReliablePacket     ( m_hLocalSocket, xbuffer_view<xbyte>{ (xbyte*)&Entry.m_SentString[0], static_cast<xuptr>(Entry.m_SentString.getLength().m_Value + 1) } ); 
                        break;
                    }
                    case xnet_base_protocol::protocol::INORDER: 
                    {
                        m_NetMesh.getClient( 0 ).SendInOrderPacket      ( m_hLocalSocket, xbuffer_view<xbyte>{ (xbyte*)&Entry.m_SentString[0], static_cast<xuptr>(Entry.m_SentString.getLength().m_Value + 1) } ); 
                        break;
                    }
                    default: x_assume( false );
                }
            } 
        
            //
            // Receive information
            //
            do 
            {
                int         Size;
                hclient     hClient;
                auto        Err     = m_NetMesh.getSocketPackets( m_hLocalSocket, xPacket, Size, hClient );

                //
                // Deal with errors first
                // 
                if( Err )
                {
                    if( Err.getCode() == xnet_base_protocol::ERR_TIMEOUT )
                    {
                        xnet_udp_client& Client = m_NetMesh.getClient( hClient );
                        printf( "Client[%s] Has timeout and disconnected\n", (const char*)Client.getAddress().getStrAddress() );
                        m_NetMesh.DeleteClient( hClient );
                    }
                    else if( Err.getCode() == xnet_base_protocol::ERR_ALERT_MID )
                    {
                        xnet_udp_client& Client = m_NetMesh.getClient( hClient );
                        printf( "Client[%s] May be a hacker\n", (const char*)Client.getAddress().getStrAddress() );
                    }
                    else if( Err.getCode() == xnet_base_protocol::ERR_FATAL )
                    {
                        printf( "Fatal error in the network system %s\n", Err.getString() );
                        Err.IgnoreError();
                        break;
                    }

                    // OK lets ignore errors and continue
                    Err.IgnoreError();
                    continue;
                }

                //
                // If size < 0 then we are not interested
                //
                if( Size <= 0 ) break;

                x_assert( hClient.isValid() );
                xnet_udp_client& Client = m_NetMesh.getClient( hClient );
            
                // If we get a negative address then the client has timed out
                if( Client.getClientType() == xnet_udp_client::TYPE_UNKOWN )
                {
                    printf( "Client[%s] Has tried to connect to us (we are not accepting new clients)\n", (const char*)Client.getAddress().getStrAddress() );
                    m_NetMesh.DeleteClient( hClient );
                }
                else
                {
                    x_assert( x_strlen( (const char*)&xPacket[0] ).m_Value >= 5 );

                    for( int i=0; i<s_Type.getCount(); i++ )
                    {
                        if( -1 != x_strstr( (char*)&xPacket[0], s_Type[i].m_pString ) )
                        {
                            const int   Index = x_atoi32( &xPacket[s_Type[i].m_Length] );
                            auto&       Info  = m_Info[i];
                            auto&       Entry = Info.m_PacketInfo[Index];

                            if( Entry.m_bRecived )
                            {
                                printf( "For Protocol: %d -- Already got this packet Packet!\n", i );
                            }
                            else
                            {
                                Info.m_RecieveCount++; 
                                Entry.m_bRecived = true;
                                Entry.m_RecivedString.Copy( (char*)&xPacket[0] );
                            }
                        }
                    }

                    printf(".");
                    //printf( "Client[%s] Gave us: %s\n", (const char*)Client.getAddress().getStrAddress(), (char*)&xPacket[0] );
                    fflush(stdout);
                }

            } while( true );
        }
    };

//-----------------------------------------------------------------------
// End of udp_basic_peer_to_peer
//-----------------------------------------------------------------------
} namespace udp_basic_server {

    struct computer_udp : public x_unit_test::network::udp_basic_peer_to_peer::computer_udp 
    {
        using t_parent =  x_unit_test::network::udp_basic_peer_to_peer::computer_udp ;

        x_constexprvar char* m_pAsking     = "Can I join please";
        x_constexprvar char* m_pGreenLight = "OK YOU ARE COOL";

        computer_udp( int MyIndex ) : t_parent{ MyIndex } {}

        //
        // This initialization function is to show how to do a hanshake between a server and a client
        //
        virtual void Initialize( xbuffer_view<xbyte> xPacket ) override
        {
            // Index == 0 will play the role of the server
            if( m_PortIndex == 0 )
            {
                //
                // Receive information
                //
                do 
                {
                    int         Size;
                    hclient     hClient;
                    auto        Err     = m_NetMesh.getSocketPackets( m_hLocalSocket, xPacket, Size, hClient );

                    //
                    // Deal with errors first
                    // 
                    if( Err )
                    {
                        if( Err.getCode() == xnet_base_protocol::ERR_FATAL )
                        {
                            printf( "Fatal error in the network system %s\n", Err.getString() );
                            Err.IgnoreError();
                            break;
                        }

                        // OK lets ignore errors and continue
                        Err.IgnoreError();
                        continue;
                    }

                    //
                    // If size < 0 then we are not interested
                    //
                    if( Size <= 0 ) break;

                    x_assert( hClient.isValid() );
                    xnet_udp_client& Client = m_NetMesh.getClient( hClient );
            
                    // If we get a negative address then the client has timed out
                    if( Client.getClientType() == xnet_udp_client::TYPE_UNKOWN )
                    {
                        //
                        // A client is trying to connect to us (the server) so we can ask a few questions if we wanted... but we will pretend he is cool
                        //
                        if( 0 == x_strcmp<char>( (char*)&xPacket[0], m_pAsking ) )
                        {
                            Client.SendReliablePacket( m_hLocalSocket, xbuffer_view<xbyte>{ (xbyte*)m_pGreenLight, static_cast<xuptr>(x_strlen( m_pGreenLight ).m_Value + 1) } ); 
                            Client.CertifyClient();
                            m_isInitialize = true;
                        }
                        else
                        {
                            printf( "Client[%s] Has tried to connect to us but is not asking nicely\n", (const char*)Client.getAddress().getStrAddress() );
                            m_NetMesh.DeleteClient( hClient );
                        }
                    }

                } while( true );

            }
            else // Other index will play the role of a client
            {
                if( m_NetMesh.getClientCount() == 0 )
                {
                    // Create a client add the other computer as a client
                    const hclient hClientID = m_NetMesh.CreateClient( { xnet_socket::getLocalIP4(), m_Ports[1 - m_PortIndex] } );
        
                    // make sure it is valid
                    x_assert( hClientID.isValid() );
                    
                    m_NetMesh.getClient(hClientID).SendReliablePacket( m_hLocalSocket, xbuffer_view<xbyte>{ (xbyte*)m_pAsking, static_cast<xuptr>(x_strlen( m_pAsking ).m_Value + 1) } );
                }

                //
                // Receive information
                //
                do 
                {
                    int         Size;
                    hclient     hClient;
                    auto        Err     = m_NetMesh.getSocketPackets( m_hLocalSocket, xPacket, Size, hClient );

                    //
                    // Deal with errors first
                    // 
                    if( Err )
                    {
                        if( Err.getCode() == xnet_base_protocol::ERR_FATAL )
                        {
                            printf( "Fatal error in the network system %s\n", Err.getString() );
                            Err.IgnoreError();
                            break;
                        }

                        // OK lets ignore errors and continue
                        Err.IgnoreError();
                        continue;
                    }

                    //
                    // If size < 0 then we are not interested
                    //
                    if( Size <= 0 ) break;

                    x_assert( hClient.isValid() );
                    xnet_udp_client& Client = m_NetMesh.getClient( hClient );
            
                    // If we get a negative address then the client has timed out
                    if( Client.getClientType() == xnet_udp_client::TYPE_UNKOWN )
                    {
                        //
                        // The server allowed us to connect!
                        //
                        if( 0 == x_strcmp( (char*)&xPacket[0], m_pGreenLight ) )
                        {
                            Client.CertifyClient();
                            m_isInitialize = true;
                            return;    
                        }
                        else
                        {
                            printf( "The server [%s] is talking non sense!\n", (const char*)Client.getAddress().getStrAddress() );
                        }
                    }

                } while( true );
            }
        }
    };

//-----------------------------------------------------------------------
// End of udp_basic_server
//-----------------------------------------------------------------------
}

    //-------------------------------------------------------------------------------
    template< typename T >
    void UDPTest( int iChoose )
    {
        xarray<xndptr_s<T>,2>    Computer;

        //
        // We create the peers here 
        // set the lag simulation for each of them
        //
        int Index = 0;
        for( auto& Entry : Computer )
        {
            Entry.New( Index++ );

            switch( iChoose )
            {
                // No need to do anything
                case 0: break;

                // May be typical worse case
                case 1: Entry->m_NetMesh.getSocket( Entry->m_hLocalSocket ).EnableLagSimulator( 30, 5, 5, 30, 60 ); break; 

                // Very bad network most games I think will die right away
                // The system can survive for a while if we increase the buffer size per client
                case 2: Entry->m_NetMesh.getSocket( Entry->m_hLocalSocket ).EnableLagSimulator( 45, 10, 5, 20, 100 ); break;
            }
        };

        //
        // Start the show
        //
       // g_context::get().m_Network_LogChannel.TurnOff();
        for( s32 i=0; i<udp_basic_peer_to_peer::PACKETS_TO_SENT; i++ )
        {
            for( auto& Entry : Computer )
            {
                Entry->onRun();
                x_Sleep(10);
            }
        }
       // g_context::get().m_Network_LogChannel.TurnOn();

        //
        // Check results
        //
        const auto& A = *Computer[0];
        const auto& B = *Computer[1];

        for( int i = 1; i < static_cast<int>(xnet_base_protocol::protocol::ENUM_COUNT); i++ )
        {
            printf( "PeerA For Protocol: %d sent: %d and %d were received\n", i, A.m_Info[i].m_SentCount, B.m_Info[i].m_RecieveCount );
            printf( "PeerB For Protocol: %d sent: %d and %d were received\n", i, B.m_Info[i].m_SentCount, A.m_Info[i].m_RecieveCount );
        }
    }

    //-----------------------------------------------------------------------
    void Test( void )
    {
        //
        // UDP tests
        //
        if( (1) )
        {
            // Normal connection as a server test
            if( (1) ) UDPTest<udp_basic_server::computer_udp>(0);

            // Normal connection as a peer to peer test
            if( (1) ) UDPTest<udp_basic_peer_to_peer::computer_udp>(0);

            // Stress test all the protocols
            if( (1) ) UDPTest<udp_basic_peer_to_peer::computer_udp>(2);
        }
    }
}}