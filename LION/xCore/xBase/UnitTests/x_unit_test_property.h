//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
// Basic functionality 
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------

namespace x_unit_test { namespace property01 
{
    //-----------------------------------------------------------------------
    // Very simple example on using properties
    // note it does not use macros to expose literally what is going on
    // the enumeration lists all the property of the class
    // note that properties are organize in folders like structures
    struct toy01_naket : public xproperty
    {
        virtual void onPropEnum ( xproperty_enum&  Enum ) const noexcept
        {
            Enum.beginScope( X_STR("BodyPhysics"), [&]( xproperty_enum::scope& Scope )
            {
                Scope.AddProperty<xprop_s32>( X_STR("Speed"), X_STR("Speed of the vehicle") );
                Scope.AddProperty<xprop_s32>( X_STR("Mass"), X_STR("Mass in Kg") );
            });
        }

        virtual bool onPropQuery( xproperty_query& Query ) noexcept
        {
            if( Query.isScope( X_STR_CRCINFO("BodyPhysics") ) ) do
            {
                if( Query.isVar( X_STR_CRCINFO("Speed") ) ) 
                {
                    // Send data to the user or receives data from the user
                    xprop_s32::SendOrReceive( Query, m_Speed, 0, 1205 );
                } 
                else if( Query.isVar( X_STR_CRCINFO("Mass") ) ) 
                {
                    xprop_s32::SendOrReceive( Query,m_Mass, 0, 1205 ); 
                } 
            } while( Query.NextPropertyOrChangeScope() );
            else return false;
            return true;
        }

        int m_Speed = 100;
        int m_Mass  =  50;
    };

    //-----------------------------------------------------------------------
    // This class uses a hierarchy properties, which simply involves 
    // calling the parent's functions.
    struct toy02_naket : public toy01_naket
    {
        //-----------------------------------------------------------------------
        virtual void onPropEnum ( xproperty_enum&  Enum ) const noexcept
        {
            Enum.beginScope( X_STR("AI"), [&]( xproperty_enum::scope& Scope )
            {
                toy01_naket::onPropEnum( Scope );
                Scope.beginScopeArray( X_STR("Navigation"), m_Array.getCount<int>(), [&]( xproperty_enum::scope_array& Scope )
                {
                    for( int i = 0; i < m_Array.getCount<int>(); i++ )
                    {
                        Scope.beginScopeArrayEntry( i, [&]( xproperty_enum::scope& Scope )                          
                        {                                                                                           
                            Scope.AddProperty<xprop_s32>( X_STR("Blue"), X_STR("This is a random test for blue colors..") );                                          
                            Scope.AddProperty<xprop_s32>( X_STR("Yellow"), X_STR("This is a random test for blue colors..") );                                        
                            Scope.beginScopeArray( X_STR( "States" ), m_Array[i].m_States.getCount<int>(), [&]( xproperty_enum::scope_array& Scope )
                            {
                                for( int j = 0; j < m_Array[i].m_States.getCount<int>(); j++ )
                                {
                                    Scope.beginScopeArrayEntry( j, [&]( xproperty_enum::scope& Scope )                          
                                    {  
                                         Scope.AddProperty<xprop_s32>( X_STR("Entry"), X_STR("This is an array entry which is an integer") );                                                                                         
                                    });
                                }
                            });
                        });
                    }
                });

                Scope.AddProperty<xprop_s32>( X_STR("RandomSeed"), X_STR("Speed of something") );
                Scope.AddProperty<xprop_s32>( X_STR("Size"), X_STR("Size of something as well") );
            });
        }

        //-----------------------------------------------------------------------
        virtual bool onPropQuery( xproperty_query& Query ) noexcept
        {
            if( Query.isScope( X_STR_CRCINFO("AI") ) ) do
            {
                if( Query.isSearchingForScopes() )
                {
                    // Deal with base
                    if( toy01_naket::onPropQuery( Query ) ) {}
                    else if( Query.isScope( X_STR_CRCINFO("Navigation") ) ) do
                    {
                        const int iNavigation = Query.getArrayIndex();
                        if( Query.isSearchingForScopes() )
                        {
                            if( Query.isScope( X_STR_CRCINFO( "States" ) ) ) do
                            {
                                const int iStates = Query.getArrayIndex();

                                if( Query.isVar( X_STR_CRCINFO("Count") ) ) { if( Query.isSending() ) xprop_s32::Send( Query, m_Array[iNavigation].m_States.getCount<int>() ); }
                                else if( Query.isVar( X_STR_CRCINFO("Entry") ) ) xprop_s32::SendOrReceive( Query, m_Array[iNavigation].m_States[iStates] );
                        
                            } while( Query.NextPropertyOrChangeScope() );
                        }
                        else if( Query.isVar( X_STR_CRCINFO("Count") ) )
                        {
                            if( Query.isReceiving() )
                            {
                                // ignore this because we cant change the size of our array
                                // Sometimes time can be useful for allocating memory and such.
                            }
                            else
                            {
                                // Send back the size of our array
                                xprop_s32::Send( Query, m_Array.getCount<int>() );
                            }
                        }
                        else if( Query.isVar( X_STR_CRCINFO("Blue") ) )  
                        {
                            xprop_s32::SendOrReceive( Query, m_Array[iNavigation].m_Blue );
                        } 
                        else if( Query.isVar( X_STR_CRCINFO("Yellow") ) )
                        {
                            xprop_s32::SendOrReceive( Query, m_Array[iNavigation].m_Yellow );
                        } 
                    } while( Query.NextPropertyOrChangeScope() );
                }
                else // -- Section for properties notice the else --- 
                {
                    if( Query.isVar( X_STR_CRCINFO("RandomSeed") ) ) 
                    {
                        xprop_s32::SendOrReceive( Query, m_RandomSeed );
                    } 
                    else if( Query.isVar( X_STR_CRCINFO("Size") ) ) 
                    {
                         xprop_s32::SendOrReceive( Query, m_Size );
                    } 
                }
             
            } while( Query.NextPropertyOrChangeScope() );
            else return false;
            return true;
        }

        struct node
        {
            int                 m_Blue       { 0xf1 };
            int                 m_Yellow     { 0xff };
            xarray<int,3>       m_States     {{ 1,2,3 }};
        };

        xarray<node,2>      m_Array         {};
        int                 m_RandomSeed    { 111 };
        int                 m_Size          { 44 };
    };

    //-----------------------------------------------------------------------
    // This example shows how macros can help simplify the code
    // this class is 100% equivalent to "toy02_naket"
    struct toy03 : public toy02_naket
    {
        //-----------------------------------------------------------------------
        virtual bool onPropQuery( xproperty_query& Query ) noexcept
        {
            X_PROP_MAINSCOPE( Query, "AI", 
                // --- Scopes ---
                X_PROP_SCOPE_CHILD( toy01_naket::onPropQuery( Query ) )
                X_PROP_IS_ARRAY( "Navigation", iNavigation,
                    // --- Scopes ---
                    X_PROP_IS_ARRAY( "States", iStates,
                        // --- Scopes ---
                        {}
                        , // --- Properties --- (notice the comma)
                        X_PROP_IS_PROP( "Count", if( Query.isSending() ) xprop_s32::Send( Query, m_Array.getCount<int>() ) )
                        X_PROP_IS_PROP( "Entry",  xprop_s32::SendOrReceive( Query, m_Array[iNavigation].m_States[iStates] ) )
                    )
                    , // --- Properties --- (notice the comma)
                    X_PROP_IS_PROP( "Count", if( Query.isSending() ) xprop_s32::Send( Query, m_Array.getCount<int>() ) )
                    X_PROP_IS_PROP( "Blue",   xprop_s32::SendOrReceive( Query, m_Array[iNavigation].m_Blue   ) )
                    X_PROP_IS_PROP( "Yellow", xprop_s32::SendOrReceive( Query, m_Array[iNavigation].m_Yellow ) )
                ) 
                , // --- Properties ---   (notice the comma)
                X_PROP_IS_PROP( "RandomSeed", xprop_s32::SendOrReceive( Query, m_RandomSeed ) )
                X_PROP_IS_PROP( "Size",       xprop_s32::SendOrReceive( Query, m_Size ))
            )
        }
    };

    //-----------------------------------------------------------------------

    void TestTheValues( xproperty& Property, xarray<xproperty_data,20>& Results, int nIndex )
    {
        //
        // Test getting values
        //
        Property.PropQueryGet( [&]( xproperty_query& Query, xproperty_data& Data )
        {
            //
            // First read the results
            //
            if( Query.hasResults() )
            {
                auto& Res = Query.getResults();
      
                // Every value should have been reset to 123
                // Except for array sizes of course
                if( x_strstr<xchar>( Res.m_Name, "[]Count" ) == -1 )
                {
                    x_assert( Res.getS32().m_Value == 123 );
                }
            }

            //
            // are we done?
            //
            if( Query.getPropertyIndex() == nIndex ) 
            {
                return false;
            }

            //
            // Tell which property to get next
            //
            Data = Results[ Query.getPropertyIndex() ];

            return true; 
        });

    }

    //-----------------------------------------------------------------------

    void BasicTest( bool bFullPath, xproperty&& Property )
    {
        xarray<xproperty_data,20>   Results;
        
        //
        // Enumerate all the properties
        //
        int nIndex = 0;
        Property.PropEnum( [&]( xproperty_enum& Enum, xproperty_data& Data )
        {
            // Just collect them all inside an static array
            Results[ nIndex++ ] = Data;
        
        }, bFullPath );

        //
        // Test getting values
        //
        Property.PropQueryGet( [&]( xproperty_query& Query, xproperty_data& Data )
        {
            //
            // First read the results if any
            //
            if( Query.hasResults() )
            {
                const int Index  = Query.getResultsIndex();
                auto      Res    = Query.getResults();
                
                // Result must match the type and the name at least 
                x_assert( Res.getType() == Results[Index].getType() );
                x_assert( 0 == x_strcmp<xchar>( Res.m_Name, Results[Index].m_Name ) );

                // Store the data in our result array
                Results[Index] = Res;
            }

            //
            // are we done?
            //
            if( Query.getPropertyIndex() == nIndex ) 
            {
                return false;
            }

            //
            // Tell which property to get next
            //
            auto& Res = Results[ Query.getPropertyIndex() ];
            Data.CopyInfo( Res );

            return true;
        });

        //
        // Test setting values
        //
        xproperty_data Temp;
        Property.PropQuerySet( [&]( xproperty_query& Query, const xproperty_data*& pData )
        {
            //
            // are we done?
            //
            if( Query.getPropertyIndex() == nIndex ) 
            {
                return false;
            }

            //
            // Set all the values to 123
            //
            xprop_s32::setup( Temp, Results[ Query.getPropertyIndex() ].m_Name, 123 );
            pData = &Temp; 
            return true;
        });

        TestTheValues( Property, Results, nIndex );

        //
        // Save the properties
        //
        {
            xtextfile TextFile;
            TextFile.openForWriting( X_WSTR("temp:/PropertyTest.txt" ) );
            Property.Save( TextFile );
        }

        //
        // Load them
        //
        {
            xtextfile TextFile;
            TextFile.openForReading( X_WSTR("temp:/PropertyTest.txt" ) );
            x_verify( TextFile.ReadRecord() );
            Property.Load( TextFile );
        }

        TestTheValues( Property, Results, nIndex );

        x_assume( true );
    }

    //-----------------------------------------------------------------------
    void TestToy2( void )
    {
        // Test with long paths
        BasicTest( true, toy02_naket{} );

        // test with relative paths
        BasicTest( false, toy02_naket{} );

        x_assume( true );
    }

    //-----------------------------------------------------------------------
    void TestToy3( void )
    {
        // Test with long paths
        BasicTest( true, toy03{} );

        // test with relative paths
        BasicTest( false, toy03{} );
        
        x_assume( true );
    }

    //-----------------------------------------------------------------------

    void Test( void )
    {
        TestToy2();
        TestToy3();
        x_assume( true );
    }
}}


//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
// test the flags
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
namespace x_unit_test { namespace property02 
{
    struct testf: public xproperty
    {

        virtual void onPropEnum( xproperty_enum&  Enum ) const noexcept override
        {
            Enum.beginScope( X_STR("BodyPhysics"), [&]( xproperty_enum::scope& Scope )
            {
                // This property will save even though we have said it is read only
                Scope.AddProperty<xprop_s32>( X_STR("Speed"), X_STR("Speed of the car"), xproperty_data::flags::MASK_READ_ONLY | xproperty_data::flags::MASK_FORCE_SAVE );
                
                // This property wont save but it will be able to be display in an editor
                Scope.AddProperty<xprop_s32>( X_STR("Mass"), X_STR("Mass of the car"), xproperty_data::flags::MASK_READ_ONLY );

                // This is a hidden property usually good when saving but not to display to the user
                Scope.AddProperty<xprop_s32>( X_STR("Other"), X_STR("Something hidden"), xproperty_data::flags::MASK_NOT_VISIBLE );

                // a standard property
                Scope.AddProperty<xprop_s32>( X_STR("Normal"), X_STR("Some Normal information") );
            });

        }

        // we don't care about getting or setting just testing flags
        virtual bool onPropQuery( xproperty_query& Query ) noexcept override
        {
            return true;
        }
    };


    void Test( void )
    {
        testf                       F;
        xarray<xproperty_data,20>   Results;
        
        //
        // Enumerate base on save
        //
        int nIndex = 0;
        F.PropEnum( [&]( xproperty_enum& Enum, xproperty_data& Data )
        {
            // Just collect them all inside an static array
            Results[ nIndex++ ] = Data;
        
        }, true, xproperty_enum::mode::SAVE );

        x_assert( nIndex == 3 );
        x_assert( x_strstr<xchar>( Results[0].m_Name, "Speed" ) >= 0 );
        x_assert( x_strstr<xchar>( Results[1].m_Name, "Other" ) >= 0 );
        x_assert( x_strstr<xchar>( Results[2].m_Name, "Normal" ) >= 0 );

        //
        // Enumerate base on real time
        //
        nIndex = 0;
        F.PropEnum( [&]( xproperty_enum& Enum, xproperty_data& Data )
        {
            // Just collect them all inside an static array
            Results[ nIndex++ ] = Data;
        
        }, true, xproperty_enum::mode::REALTIME );

        x_assert( nIndex == 3 );
        x_assert( x_strstr<xchar>( Results[0].m_Name, "Speed" ) >= 0 );
        x_assert( x_strstr<xchar>( Results[1].m_Name, "Mass"  ) >= 0 );
        x_assert( x_strstr<xchar>( Results[2].m_Name, "Normal" ) >= 0 );
    }
}}

//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
// Test different types of properties
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------

namespace x_unit_test { namespace property03 
{
    struct types: public xproperty
    {
        virtual void onPropEnum( xproperty_enum&  Enum ) const noexcept override
        {
            Enum.beginScope( X_STR("Types"), [&]( xproperty_enum::scope& Scope )
            {
                Scope.AddProperty<xprop_bool>   ( X_STR("BitMaskBool"),     X_STR("Example on how to deal with bitmasks")  );
                Scope.AddProperty<xprop_bool>   ( X_STR("SimpleBool"),      X_STR("This is just a regular bool")  );
                Scope.AddProperty<xprop_button> ( X_STR("SomeButton"),      X_STR("This is just a regular button")  );
                Scope.AddProperty<xprop_button> ( X_STR("ToggleButton"),    X_STR("This button changes state")  );
                Scope.AddProperty<xprop_enum>   ( X_STR("SomeEnum"),        X_STR("Testing Enumerations")  );
                Scope.AddProperty<xprop_v2>     ( X_STR("V2"),              X_STR("Testing A vector2")   );
                Scope.AddProperty<xprop_v3>     ( X_STR("V3"),              X_STR("Testing A vector3")   );
                Scope.AddProperty<xprop_guid>   ( X_STR("SomeGuid"),        X_STR("Testing A vector3")   );
            });
        }

        //-----------------------------------------------------------------------
        virtual bool onPropQuery( xproperty_query& Query ) noexcept override
        {
            X_PROP_MAINSCOPE( Query, "Types", 
                // --- Scopes ---
                
                , // --- Properties ---   (notice the comma)
                X_PROP_IS_PROP( "SomeGuid",         xprop_guid::SendOrReceive( Query, m_TestGuid.m_Value, X_STR("Entity"), 0 ))
                X_PROP_IS_PROP( "BitMaskBool",      xprop_bool::SendOrReceive( Query, m_BitmaskBool, (1u<<2) ))
                X_PROP_IS_PROP( "SimpleBool",       xprop_bool::SendOrReceive( Query, m_SimpleBool ))
                X_PROP_IS_PROP( "ToggleButton", 
                    static const xarray<xstring,2> List
                    {
                        X_STR("Option1"),
                        X_STR("Option2")
                    };
                    if( Query.isReceiving() )
                    {
                        xstring String;
                        xprop_button::Recive( Query, String );

                        if( String == List[0] ) m_ButtonToggle = 1;
                        if( String == List[1] ) m_ButtonToggle = 0;
                    }
                    else
                    {
                        xprop_button::Send( Query, List[m_ButtonToggle] );
                    }) 
                X_PROP_IS_PROP( "SomeButton", 
                    if( Query.isReceiving() )
                    {
                        // I can do something here...
                    }
                    else
                    {
                        static xstring PressMe( X_STR("Press me") );
                        xprop_button::Send( Query, PressMe );
                    }) 
                X_PROP_IS_PROP( "SomeEnum", 
                    static const xprop_enum::entry EnumList[] =
                    {
                        { 0, X_STR("First Choice") },
                        { 1, X_STR("Second Choice") },
                        { 2, X_STR("2 Choice") },
                        { 3, X_STR("3 Choice") },
                        { 4, X_STR("4 Choice") },
                        { xprop_enum::entry::END_DISPLAY, X_STR("") },  // From here forward are to tests backwards compatibility
                        { 10, X_STR("5 Choice") },
                        { 15, X_STR("6 Choice") },
                        { 17, X_STR("7 Choice") }
                    };
                    xprop_enum::SendOrReceive( Query, m_EnumVal, EnumList, x_countof(EnumList) ) )
                X_PROP_IS_PROP( "V2",       xprop_v2::SendOrReceive( Query, m_V2, xvector2(-100), xvector2(100) ))
                X_PROP_IS_PROP( "V3",       xprop_v3::SendOrReceive( Query, m_V3, xvector3(0), xvector3(1) ))
            )
        }


        xguid<int>  m_TestGuid      {};
        u32         m_BitmaskBool   { 0 };
        bool        m_SimpleBool    { false };
        int         m_ButtonToggle  { 0 };
        int         m_EnumVal       { 0 };
        xvector2    m_V2            { 0.5f, 0.5f };
        xvector3    m_V3            { 0.5f, 0.5f, 0.5f };
    };

    struct testtypes: public types
    {
        virtual void onPropEnum( xproperty_enum&  Enum ) const noexcept override
        {
            Enum.beginScope( X_STR("BodyPhysics"), [&]( xproperty_enum::scope& Scope )
            {
                types::onPropEnum( Enum );
                Scope.AddProperty<xprop_s32>    ( X_STR("Integer"), X_STR("Testing Integers") );
                Scope.AddProperty<xprop_f32>    ( X_STR("Float"),   X_STR("Testing Floating point numbers")     );
                Scope.AddProperty<xprop_string> ( X_STR("String"),  X_STR("Testing A string")   );
                Scope.AddProperty<xprop_color>  ( X_STR("Color"),   X_STR("Testing A color")   );
            });
        }

        //-----------------------------------------------------------------------
        virtual bool onPropQuery( xproperty_query& Query ) noexcept override
        {
            X_PROP_MAINSCOPE( Query, "BodyPhysics", 
                // --- Scopes ---
                types::onPropQuery( Query )
                , // --- Properties ---   (notice the comma)
                X_PROP_IS_PROP( "Integer",  xprop_s32::SendOrReceive( Query, m_Int ) )
                X_PROP_IS_PROP( "Float",    xprop_f32::SendOrReceive( Query, m_Float ))
                X_PROP_IS_PROP( "String",   xprop_string::SendOrReceive( Query, m_String ))
                X_PROP_IS_PROP( "Color",    xprop_color::SendOrReceive( Query, m_Color ))
            )
        }


        int     m_Int       { 22 };
        f32     m_Float     { 0.01f };
        xstring m_String    { X_STR("Hello") };
        xcolor  m_Color     { 255, 128, 128, 255 };
    };


    void Test( void )
    {
        bool                        bFullPath = false;
        testtypes                   Property;
        xarray<xproperty_data,20>   Results;
        
        //
        // Enumerate all the properties
        //
        int nIndex = 0;
        Property.PropEnum( [&]( xproperty_enum& Enum, xproperty_data& Data )
        {
            // Just collect them all inside an static array
            Results[ nIndex++ ] = Data;
        
        }, bFullPath );

        //
        // Test getting values
        //
        Property.PropQueryGet( [&]( xproperty_query& Query, xproperty_data& Data )
        {
            //
            // First read the results if any
            //
            if( Query.hasResults() )
            {
                const int Index  = Query.getResultsIndex();
                auto      Res    = Query.getResults();
                
                // Result must match the type and the name at least 
                x_assert( Res.getType() == Results[Index].getType() );
                x_assert( 0 == x_strcmp<xchar>( Res.m_Name, Results[Index].m_Name ) );

                // Store the data in our result array
                Results[Index] = Res;
            }

            //
            // are we done?
            //
            if( Query.getPropertyIndex() == nIndex ) 
            {
                return false;
            }

            //
            // Tell which property to get next
            //
            Data.CopyInfo( Results[ Query.getPropertyIndex() ] );

            return true;
        });

        const auto S32V = Results[8].getS32().m_Value;
        const auto F32V = Results[9].getF32().m_Value;
        const auto SV   = Results[10].getString();
        x_assert( S32V  == 22 );
        x_assert( F32V == 0.01f );
        x_assert( x_strcmp<xchar>( SV, "Hello" ) == 0 );

        Results[8].getS32().m_Value = 100;
        Results[9].getF32().m_Value = 200;
        Results[10].getString().setup( "ByeBye" );

        //
        // Test setting values
        //
        Property.PropQuerySet( [&]( xproperty_query& Query, const xproperty_data*& pData )
        {
            //
            // are we done?
            //
            if( Query.getPropertyIndex() == nIndex ) 
            {
                return false;
            }

            //
            // Set all the values to 123
            //
            pData = &Results[ Query.getPropertyIndex() ];

            return true;
        });

        x_assert( Results[8].getS32().m_Value == Property.m_Int );
        x_assert( Results[9].getF32().m_Value == Property.m_Float );
        x_assert( x_strcmp<xchar>( Results[10].getString(), Property.m_String ) == 0 );

        x_assume( true );
    }
}}


//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
// Test properties collections
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
namespace x_unit_test { namespace property04 
{
    void Test( void )
    {
        bool                                bFullPath = false;
        x_unit_test::property03::testtypes  Property;
        xproperty_data_collection           BlueprintCollection;
        xproperty_data_collection           Collection;

        //
        // Collect the properties for the blueprint
        //        
        Property.PropToCollection( BlueprintCollection, bFullPath );

        //
        // Change some values
        //
        Property.m_Float = 2.0f;
        Property.m_String = X_STR("This is a test");

        //
        // Collect the properties for the entity...
        //
        Property.PropToCollection( Collection, bFullPath );

        //
        // Filter the properties
        //
        xproperty_data_collection           FinalList;

        // Lets filter the properties
        FinalList.appendSubtractFilterByNameAndValue( Collection, BlueprintCollection );

        x_assert( FinalList.m_List.getCount() == 2 );

    }
}}


//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
namespace x_unit_test { namespace property 
{
    void Test( void )
    {
        x_unit_test::property01::Test();
        x_unit_test::property02::Test();
        x_unit_test::property03::Test();
        x_unit_test::property04::Test();
    }
}}