//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

/////////////////////////////////////////////////////////////////////////////////

class xnet_tcp_client 
{
    x_object_type( xnet_tcp_client, is_linear, is_not_movable, is_not_copyable )

public:
        
        enum type
        {
            TYPE_UNKOWN,
            TYPE_CERTIFIED
        };

    using err                   = xnet_socket::err;
    using errors                = xnet_socket::errors;

public:
                                xnet_tcp_client         ( void )                                                            noexcept { m_ClientType = TYPE_UNKOWN; } 
            err                 openSocket              ( s32 Port )                                                        noexcept;
            err                 ConnectToServer         ( xnet_address Address )                                            noexcept;
            err                 Send                    ( const xbuffer_view<xbyte> Packet )                                noexcept;
            err                 Receive                 ( xbuffer_view<xbyte> Packet, s32& UserSize, bool bBlock = false )  noexcept;
            type                getClientType           ( void )                                                    const   noexcept { return m_ClientType; }
            void                CertifyClient           ( void )                                                            noexcept { m_ClientType = TYPE_CERTIFIED; }
            xnet_socket&        getSocket               ( void )                                                            noexcept { return m_Socket; }

protected:

    type            m_ClientType;
    xnet_socket     m_Socket;

protected:

    friend class xnet_tcp_server;
};

//////////////////////////////////////////////////////////////////////////////////

class xnet_tcp_server
{
    x_object_type( xnet_tcp_server, is_linear, is_not_movable, is_not_copyable )

public:

    using t_client_container    = xphvector<xnet_tcp_client,128>;
    using t_hclient             = t_client_container::t_handle; 

    using err                   = xnet_socket::err;
    using errors                = xnet_socket::errors;

public:
                                xnet_tcp_server         ( void )                    noexcept = default;
            err                 openSocket              ( s32 Port )                noexcept;
            err                 AcceptConnections       ( void )                    noexcept;
            xnet_socket&        getSocket               ( void )                    noexcept { return m_Socket; }
//            xhandle             CreateClient            ( xnet_address Address );
//            void                DeleteClient            ( xhandle hClient );
            s32                 getClientCount          ( void )            const   noexcept { return m_lClient.getCount<int>(); }
            xnet_tcp_client&    getClient               ( s32 Index )               noexcept { return m_lClient[Index]; }
            xnet_tcp_client&    getClient               ( t_hclient hClient )       noexcept { return m_lClient(hClient); }
//            xhandle             findClient              ( const xnet_address& FromAddr );

protected:

    xnet_socket                 m_Socket;
    t_client_container          m_lClient;
};

