//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//------------------------------------------------------------------------------
x_inline
void xnet_address::Clear( void ) noexcept
{
    m_IP4  = {0};
    m_Port = 0;
    m_Type = type::IP4; 
}

//------------------------------------------------------------------------------
x_inline
bool xnet_address::isValid( void ) const noexcept
{
    if( m_Type == type::IP4 )
    {
        if( m_IP4.m_Value == 0x0000 ) return false;
        if( m_IP4.m_Value == 0xFFFF ) return false;
    }
    else
    {
        // todo: Check here
    }

    if( m_Port.m_Value == 0 )             return false;

    return true;
}

//------------------------------------------------------------------------------
x_inline
void xnet_address::setup( ip4 IP, port Port ) noexcept
{
    m_IP4  = IP;
    m_Port = Port;
    m_Type = type::IP4;
}

//------------------------------------------------------------------------------
x_inline
void xnet_address::setup( const ip6& IP, port Port ) noexcept
{
    m_IP6  = IP;
    m_Port = Port;
    m_Type = type::IP6;
}

//------------------------------------------------------------------------------
x_inline
void xnet_address::setup( const xarray<u16,8>& Array, port Port ) noexcept
{
    m_IP6  =  { x_EndianSystemToBig(Array[0]),x_EndianSystemToBig(Array[1]),
                x_EndianSystemToBig(Array[2]),x_EndianSystemToBig(Array[3]),
                x_EndianSystemToBig(Array[4]),x_EndianSystemToBig(Array[5]),
                x_EndianSystemToBig(Array[6]),x_EndianSystemToBig(Array[7]) };
    m_Port = Port;
    m_Type = type::IP6;
}

//------------------------------------------------------------------------------
x_inline
void xnet_address::setIP( ip4 IP ) noexcept
{
    m_IP4  = IP;
    m_Type = type::IP4;
}

//------------------------------------------------------------------------------
x_inline
void xnet_address::setIP( ip6 IP ) noexcept
{
    m_IP6  = IP;
    m_Type = type::IP6;
}

//------------------------------------------------------------------------------
x_inline
void xnet_address::setPort( port Port ) noexcept
{
    m_Port = Port;
}

//------------------------------------------------------------------------------
x_inline
const char* xnet_address::setStrIP( const char* pIPStr ) noexcept
{
    // Check ip type
    for( int i = 0; *pIPStr; i++ )
    {
        if( pIPStr[i] == '.' )
        {
            m_Type = type::IP4;
            break;
        }
        else if( pIPStr[i] == ':' || pIPStr[i] == '[' )
        {
            m_Type = type::IP6;
            break;
        }
    }

    if( m_Type == type::IP4 )
    {
        m_IP4.m_Value  = 0;
        char C = *pIPStr++;

        for( s32 i=0; i<4; i++ )
        {
            u32  D = 0;

            while( (C>='0') && (C<='9') )
            {
                D = D*10 + (C-'0');
                C = *pIPStr++;
                x_assert( D < 256 );
            }

            m_IP4.m_Value = (m_IP4.m_Value << 8) | D;

            if( C == 0 || C == ':' ) 
            { 
                x_assert( i==3 );
                break; 
            }
            else         
            { 
                x_assert( i<3 );
                x_assert( C == '.');
                pIPStr++;
            }
        }
    }
    else
    {
        m_IP6.m_Value = {{0,0,0,0,0,0,0,0}};
        char C = x_tolower(*pIPStr++);

        for( s32 i=0; i<m_IP6.m_Value.getCount<s32>(); i++ )
        {
            u32  D = 0;

            do  
            {
                if(      C>='0' && C<='9' ) D = D*16 + (C-'0');
                else if( C>='a' && C<='f' ) D = D*16 + (C-'a'); 
                else break;
                C = *pIPStr++;
                x_assert( D < 256 );
            } while( true );

            x_assert( D <= 0xffff );
            m_IP6.m_Value[i] = x_EndianSystemToBig(D);

            if( C == ']' || C == 0 ) 
            {
                x_assert( i == 7 );
                break;
            }
            else
            {
                x_assert( i<3 );
                x_assert( C == ':' );
                C = x_tolower(*pIPStr++);
            }
        }
    }

    return pIPStr;
}

//------------------------------------------------------------------------------
x_inline
const char* xnet_address::setStrAddress( const char* pAddrStr ) noexcept
{
    // Read IP.
    pAddrStr = setStrIP( pAddrStr );
    x_assert( *pAddrStr );
     
    // Read port.
    m_Port = 0;
    if( m_Type == type::IP4 )
    {
        x_assert( *pAddrStr == ':' );
        char C = *++pAddrStr;
        while( (C>='0') && (C<='9') )
        {
            m_Port.m_Value = m_Port.m_Value*10 + (C-'0');
            C      = *++pAddrStr;
        }
    }
    else
    {
        x_assert( *pAddrStr == ']' );
        ++pAddrStr;
        x_assert( *pAddrStr == ':' );

        char C = x_tolower(*++pAddrStr);
        do  
        {
            if(      C>='0' && C<='9' ) m_Port.m_Value = m_Port.m_Value*16 + (C-'0');
            else if( C>='a' && C<='f' ) m_Port.m_Value = m_Port.m_Value*16 + (C-'a'); 
            else break;
            C = x_tolower(*++pAddrStr);
        } while( true );
    }

    return pAddrStr;
}

//------------------------------------------------------------------------------
x_inline
xstring xnet_address::getStrIP( void ) const noexcept
{
    if( m_Type == type::IP4 )
    {
        const auto IP4 = m_IP4.get();
        return xstring::Make( xstring::t_characters{ 30 }, "%d.%d.%d.%d",
                       (IP4>>24)&0xFF,                         
                       (IP4>>16)&0xFF,                         
                       (IP4>> 8)&0xFF,                         
                       (IP4>> 0)&0xFF );
    }
    else
    {
        xstring Str{ xstring::t_characters{ 128 } };
        const xarray<u16,8> IP6 = m_IP6.get();
        for( int i = 0; i < IP6.getCount(); i++ )
        {
            if( i == 7 )
            {
                if(IP6[i]>0)    Str.append( xstring::Make("%X", IP6[i] ) );
            }
            else
            {
                if(IP6[i]>0)    Str.append( xstring::Make("%X:", IP6[i] ) );
                else            Str.append( ":" ); 
            }
        }
        return Str;
    }
}

//------------------------------------------------------------------------------
x_inline
xstring xnet_address::getStrAddress( void ) const noexcept
{
    if( m_Type == type::IP4 )
    {
        const auto IP4 = m_IP4.get();
        return xstring::Make( xstring::t_characters{ 30 }, "%d.%d.%d.%d:%d",
                       (IP4>>24)&0xFF,                         
                       (IP4>>16)&0xFF,                         
                       (IP4>> 8)&0xFF,                         
                       (IP4>> 0)&0xFF,
                        m_Port.get() );
    }
    else
    {
        xstring                 Str{ xstring::t_characters{ 128 } };
        const xarray<u16,8>     IP6 = m_IP6.get();
        
        Str.append("[");
        for( int i = 0; i < IP6.getCount(); i++ )
        {
            if( i == 7 )
            {
                if(IP6[i]>0)    Str.append( xstring::Make("%X", IP6[i] ) );
            }
            else
            {
                if(IP6[i]>0)    Str.append( xstring::Make("%X:", IP6[i] ) );
                else            Str.append( ":" ); 
            }
        }
        Str.append( xstring::Make("]:%X", m_Port.get() ) );

        return Str;
    }
}

//------------------------------------------------------------------------------
constexpr 
bool xnet_address::operator == ( const xnet_address& A ) const noexcept
{
    return ( m_Port.m_Value == A.m_Port.m_Value )   && 
           ( m_Type == type::IP4 )  ? 
                (   m_IP4.m_Value == A.m_IP4.m_Value ) 
           :    (   m_IP6.m_Value[0] == A.m_IP6.m_Value[0] &&
                    m_IP6.m_Value[1] == A.m_IP6.m_Value[1] &&
                    m_IP6.m_Value[2] == A.m_IP6.m_Value[2] &&
                    m_IP6.m_Value[3] == A.m_IP6.m_Value[3] &&
                    m_IP6.m_Value[4] == A.m_IP6.m_Value[4] &&
                    m_IP6.m_Value[5] == A.m_IP6.m_Value[5] &&
                    m_IP6.m_Value[6] == A.m_IP6.m_Value[6] &&
                    m_IP6.m_Value[7] == A.m_IP6.m_Value[7] );
}

//------------------------------------------------------------------------------
constexpr
bool xnet_address::operator != ( const xnet_address& A ) const noexcept
{
    return !( (*this) == A );
}


