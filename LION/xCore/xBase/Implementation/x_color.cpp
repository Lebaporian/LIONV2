//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#include "x_Base.h"

//==============================================================================
// MACROS
//==============================================================================
#define COMP_BIT(A,B) u32(((1<<(A))-1)<<(B)) 
#define COMP_SFT(A,B) (((A)-8)+(B))

#define DESC_ARGB(A,AL,R,RL,G,GL,B,BL) (A+R+G+B),                                                      \
                                       COMP_SFT(A,AL), COMP_SFT(R,RL), COMP_SFT(G,GL), COMP_SFT(B,BL), \
                                       COMP_BIT(A,AL), COMP_BIT(R,RL), COMP_BIT(G,GL), COMP_BIT(B,BL), 


#define BUILD_RGBA(R,G,B,A) DESC_ARGB( A,R+G+B,   R, 0,      G,B,     B,G+B )
#define BUILD_RGBU(R,G,B,U) DESC_ARGB( 0,R+G+B,   R, 0,      G,B,     B,G+B )

#define BUILD_ABGR(A,B,G,R) DESC_ARGB( A,0,       R,G+B+A,   G,B+A,   B,A )
#define BUILD_UBGR(U,B,G,R) DESC_ARGB( 0,0,       R,G+B+U,   G,B+U,   B,U )

#define BUILD_URGB(U,R,G,B) DESC_ARGB( 0,0,       R,U,       G,R+U,   B,G+R+U )
#define BUILD_ARGB(A,R,G,B) DESC_ARGB( A,0,       R,A,       G,R+A,   B,G+R+A )

#define BUILD_BGRA(B,G,R,A) DESC_ARGB( A,R+G+B,   R,G+B,     G,B,     B,0 )
#define BUILD_BGRU(B,G,R,U) DESC_ARGB( 0,R+G+B,   R,G+B,     G,B,     B,0 )


//==============================================================================
// VARIABLES
//==============================================================================
const xarray<xcolor::fmt_desc,xcolor::FMT_END> xcolor::g_FormatDesc = []()->auto
{
    xarray<xcolor::fmt_desc,xcolor::FMT_END> FormatDesc = { fmt_desc{ FMT_NULL, 0u,  0, BUILD_ARGB(0,0,0,0) } };

    FormatDesc[ FMT_16_ABGR_4444 ] = fmt_desc{ FMT_16_ABGR_4444, u32(1<<FMT_16_ABGR_4444), 16, BUILD_ABGR(4,4,4,4)  };
    FormatDesc[ FMT_16_ARGB_4444 ] = fmt_desc{ FMT_16_ARGB_4444, u32(1<<FMT_16_ARGB_4444), 16, BUILD_ARGB(4,4,4,4)  };
    FormatDesc[ FMT_16_RGBA_4444 ] = fmt_desc{ FMT_16_RGBA_4444, u32(1<<FMT_16_RGBA_4444), 16, BUILD_RGBA(4,4,4,4)  };
    FormatDesc[ FMT_16_RGB_565   ] = fmt_desc{ FMT_16_RGB_565,   u32(1<<FMT_16_RGB_565  ), 16, BUILD_RGBU(5,6,5,0)  };
    FormatDesc[ FMT_16_BGR_565   ] = fmt_desc{ FMT_16_BGR_565,   u32(1<<FMT_16_BGR_565  ), 16, BUILD_BGRU(5,6,5,0)  };
    FormatDesc[ FMT_16_ARGB_1555 ] = fmt_desc{ FMT_16_ARGB_1555, u32(1<<FMT_16_ARGB_1555), 16, BUILD_ARGB(1,5,5,5)  };
    FormatDesc[ FMT_16_RGBA_5551 ] = fmt_desc{ FMT_16_RGBA_5551, u32(1<<FMT_16_RGBA_5551), 16, BUILD_RGBA(5,5,5,1)  };
    FormatDesc[ FMT_16_URGB_1555 ] = fmt_desc{ FMT_16_URGB_1555, u32(1<<FMT_16_URGB_1555), 16, BUILD_URGB(1,5,5,5)  };
    FormatDesc[ FMT_16_RGBU_5551 ] = fmt_desc{ FMT_16_RGBU_5551, u32(1<<FMT_16_RGBU_5551), 16, BUILD_RGBU(5,5,5,1)  };
    FormatDesc[ FMT_16_ABGR_1555 ] = fmt_desc{ FMT_16_ABGR_1555, u32(1<<FMT_16_ABGR_1555), 16, BUILD_ABGR(1,5,5,5)  };
    FormatDesc[ FMT_16_UBGR_1555 ] = fmt_desc{ FMT_16_UBGR_1555, u32(1<<FMT_16_UBGR_1555), 16, BUILD_UBGR(1,5,5,5)  };
    FormatDesc[ FMT_24_RGB_888   ] = fmt_desc{ FMT_24_RGB_888,   u32(1<<FMT_24_RGB_888  ), 24, BUILD_RGBU(8,8,8,0)  };
    FormatDesc[ FMT_24_ARGB_8565 ] = fmt_desc{ FMT_24_ARGB_8565, u32(1<<FMT_24_ARGB_8565), 24, BUILD_ARGB(8,5,6,5)  };
    FormatDesc[ FMT_32_RGBU_8888 ] = fmt_desc{ FMT_32_RGBU_8888, u32(1<<FMT_32_RGBU_8888), 32, BUILD_RGBU(8,8,8,8)  };
    FormatDesc[ FMT_32_URGB_8888 ] = fmt_desc{ FMT_32_URGB_8888, u32(1<<FMT_32_URGB_8888), 32, BUILD_URGB(8,8,8,8)  };
    FormatDesc[ FMT_32_ARGB_8888 ] = fmt_desc{ FMT_32_ARGB_8888, u32(1<<FMT_32_ARGB_8888), 32, BUILD_ARGB(8,8,8,8)  };
    FormatDesc[ FMT_32_RGBA_8888 ] = fmt_desc{ FMT_32_RGBA_8888, u32(1<<FMT_32_RGBA_8888), 32, BUILD_RGBA(8,8,8,8)  };
    FormatDesc[ FMT_32_ABGR_8888 ] = fmt_desc{ FMT_32_ABGR_8888, u32(1<<FMT_32_ABGR_8888), 32, BUILD_ABGR(8,8,8,8)  };
    FormatDesc[ FMT_32_BGRA_8888 ] = fmt_desc{ FMT_32_BGRA_8888, u32(1<<FMT_32_BGRA_8888), 32, BUILD_BGRA(8,8,8,8)  };

    return FormatDesc;
}();

const xarray<xcolor::best_match,xcolor::FMT_END> xcolor::g_Match = []()->auto
{
    xarray<xcolor::best_match,FMT_END> Match = { best_match{ { FMT_NULL, FMT_END } }};
    
    Match[ FMT_16_ARGB_4444 ] = best_match{ { FMT_16_ARGB_4444, FMT_16_RGBA_4444, FMT_24_ARGB_8565, FMT_32_ARGB_8888, FMT_32_RGBA_8888, FMT_32_ABGR_8888, FMT_32_BGRA_8888,                     FMT_END } };
    Match[ FMT_16_RGBA_4444 ] = best_match{ { FMT_16_RGBA_4444, FMT_16_ARGB_4444, FMT_24_ARGB_8565, FMT_32_ARGB_8888, FMT_32_RGBA_8888, FMT_32_ABGR_8888, FMT_32_BGRA_8888,                     FMT_END } };
    Match[ FMT_16_RGB_565   ] = best_match{ { FMT_16_RGB_565  , FMT_16_URGB_1555, FMT_16_RGBU_5551, FMT_16_UBGR_1555, FMT_24_RGB_888,   FMT_32_RGBU_8888, FMT_32_URGB_8888,                     FMT_END } };
    Match[ FMT_16_ARGB_1555 ] = best_match{ { FMT_16_ARGB_1555, FMT_16_RGBA_5551, FMT_16_ABGR_1555, FMT_24_ARGB_8565, FMT_32_ARGB_8888, FMT_32_RGBA_8888, FMT_32_ABGR_8888, FMT_32_BGRA_8888,   FMT_END } };
    Match[ FMT_16_RGBA_5551 ] = best_match{ { FMT_16_RGBA_5551, FMT_16_ARGB_1555, FMT_16_ABGR_1555, FMT_24_ARGB_8565, FMT_32_ARGB_8888, FMT_32_RGBA_8888, FMT_32_ABGR_8888, FMT_32_BGRA_8888,   FMT_END } };
    Match[ FMT_16_URGB_1555 ] = best_match{ { FMT_16_URGB_1555, FMT_16_RGBU_5551, FMT_16_UBGR_1555, FMT_16_RGB_565,   FMT_24_RGB_888,   FMT_32_RGBU_8888, FMT_32_URGB_8888,                     FMT_END } };
    Match[ FMT_16_RGBU_5551 ] = best_match{ { FMT_16_RGBU_5551, FMT_16_URGB_1555, FMT_16_UBGR_1555, FMT_16_RGB_565,   FMT_24_RGB_888,   FMT_32_RGBU_8888, FMT_32_URGB_8888,                     FMT_END } };
    Match[ FMT_16_ABGR_1555 ] = best_match{ { FMT_16_ABGR_1555, FMT_16_RGBA_5551, FMT_16_ARGB_1555, FMT_24_ARGB_8565, FMT_32_ARGB_8888, FMT_32_RGBA_8888, FMT_32_ABGR_8888, FMT_32_BGRA_8888,   FMT_END } };
    Match[ FMT_24_RGB_888   ] = best_match{ { FMT_24_RGB_888  , FMT_32_RGBU_8888, FMT_32_URGB_8888,                                                                                             FMT_END } };
    Match[ FMT_24_ARGB_8565 ] = best_match{ { FMT_24_ARGB_8565, FMT_32_ARGB_8888, FMT_32_RGBA_8888, FMT_32_ABGR_8888, FMT_32_BGRA_8888,                                                         FMT_END } };
    Match[ FMT_32_RGBU_8888 ] = best_match{ { FMT_32_RGBU_8888, FMT_32_URGB_8888, FMT_24_RGB_888  ,                                                                                             FMT_END } };
    Match[ FMT_32_URGB_8888 ] = best_match{ { FMT_32_URGB_8888, FMT_32_RGBU_8888, FMT_24_RGB_888  ,                                                                                             FMT_END } };
    Match[ FMT_32_ARGB_8888 ] = best_match{ { FMT_32_ARGB_8888, FMT_32_RGBA_8888, FMT_32_ABGR_8888, FMT_32_BGRA_8888,                                                                           FMT_END } };
    Match[ FMT_32_RGBA_8888 ] = best_match{ { FMT_32_RGBA_8888, FMT_32_ARGB_8888, FMT_32_ABGR_8888, FMT_32_BGRA_8888,                                                                           FMT_END } };
    Match[ FMT_32_ABGR_8888 ] = best_match{ { FMT_32_ABGR_8888, FMT_32_BGRA_8888, FMT_32_ARGB_8888, FMT_32_RGBA_8888,                                                                           FMT_END } };
    Match[ FMT_32_BGRA_8888 ] = best_match{ { FMT_32_BGRA_8888, FMT_32_ABGR_8888, FMT_32_ARGB_8888, FMT_32_RGBA_8888,                                                                           FMT_END } };

    return Match;
}();

const xarray<xcolor,20> xcolor::g_ColorCategories = 
{
    xcolor{ 0x1f77b4ff },
    xcolor{ 0xaec7e8ff },
    xcolor{ 0xff7f0eff },
    xcolor{ 0xffbb78ff },
    xcolor{ 0x2ca02cff },
    xcolor{ 0x98df8aff },
    xcolor{ 0xd62728ff },
    xcolor{ 0xff9896ff },
    xcolor{ 0x9467bdff },
    xcolor{ 0xc5b0d5ff },
    xcolor{ 0x8c564bff },
    xcolor{ 0xc49c94ff },
    xcolor{ 0xe377c2ff },
    xcolor{ 0xf7b6d2ff },
    xcolor{ 0x7f7f7fff },
    xcolor{ 0xc7c7c7ff },
    xcolor{ 0xbcbd22ff },
    xcolor{ 0xdbdb8dff },
    xcolor{ 0x17becfff },
    xcolor{ 0x9edae5ff }
};

//------------------------------------------------------------------------------
// The Hue/Saturation/Value system (or also called Hexcone model) was created by
// Smith in 1978. It was for an aesthetic purpose, accessing color by family,
// purely and intensity rather than by its component. With that model it becomes
// easy to produce some kind of hellish brown or some kind of
// you-know-that-color-in-between-(x) and (y).
// The H value is a 360 degree value through color families.
// The S (or Saturation) is the degree of strength of a color. Greater is S, the
// purest is the color. if S max is 100, then Hue=red and S=100 would produce an
// intense red (reproduced by RGB (max,0,0)) 
// Finally, the V, for value, is the darkness/lightness of a color. More V is
// great, more the color is close to white.
//------------------------------------------------------------------------------
void xcolor::getHSV( f32& H, f32& S, f32& V ) const
{
    f32 r = m_R/255.f;
    f32 g = m_G/255.f;
    f32 b = m_B/255.f;

    float K = 0.f;
    if (g < b)
    {
        const float tmp = g; g = b; b = tmp;
        K = -1.f;
    }
    if (r < g)
    {
        const float tmp = r; r = g; g = tmp;
        K = -2.f / 6.f - K;
    }

    const float chroma = r - (g < b ? g : b);
    H = fabsf(K + (g - b) / (6.f * chroma + 1e-20f));
    S = chroma / (r + 1e-20f);
    V = r;
}

//------------------------------------------------------------------------------

xcolor& xcolor::setupFromHSV( f32 h, f32 s, f32 v )
{
    f32 out_r, out_g, out_b; 
    if (s == 0.0f)
    {
        // gray
        out_r = out_g = out_b = v;
    }
    else
    {
        h = fmodf(h, 1.0f) / (60.0f/360.0f);
        int   i = (int)h;
        float f = h - (float)i;
        float p = v * (1.0f - s);
        float q = v * (1.0f - s * f);
        float t = v * (1.0f - s * (1.0f - f));

        switch (i)
        {
        case 0: out_r = v; out_g = t; out_b = p; break;
        case 1: out_r = q; out_g = v; out_b = p; break;
        case 2: out_r = p; out_g = v; out_b = t; break;
        case 3: out_r = p; out_g = q; out_b = v; break;
        case 4: out_r = t; out_g = p; out_b = v; break;
        case 5: default: out_r = v; out_g = p; out_b = q; break;
        }
    }

    out_r *= 255;
    out_g *= 255;
    out_b *= 255;

    m_R = (u8)x_Min( 255.0f, out_r );
    m_G = (u8)x_Min( 255.0f, out_g );
    m_B = (u8)x_Min( 255.0f, out_b );
    m_A = 255;

    return *this;
}
