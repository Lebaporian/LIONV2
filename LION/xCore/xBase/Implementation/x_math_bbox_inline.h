//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//==============================================================================
// FUNCTIONS
//==============================================================================

//------------------------------------------------------------------------------
inline
xbbox::xbbox( void )
{
    setIdentity();
}

//------------------------------------------------------------------------------
inline
xbbox::xbbox( const xvector3d& P1 ) :
    m_Min( P1 ),
    m_Max( P1 ) 
{

}

//------------------------------------------------------------------------------
inline
xbbox::xbbox( const xvector3d& P1, const xvector3d& P2 )
{
    setup( P1, P2 );
}

//------------------------------------------------------------------------------
inline
xbbox::xbbox( const xvector3d* pVerts, const s32 nVerts )
{
    x_assert( nVerts >= 0 );

    if( nVerts > 0 )
    {
        Intersect( pVerts[0] );
        Intersect( &pVerts[1], nVerts-1 );
    }
    else
    {
        setIdentity();
    }
}

//------------------------------------------------------------------------------
inline
xbbox::xbbox( const xvector3d& Center, const f32 Radius )
{
    setup( Center, Radius );
}

//------------------------------------------------------------------------------
inline
void xbbox::setZero( void )
{
    m_Min.setZero();
    m_Max.setZero();
}

//------------------------------------------------------------------------------
inline
void xbbox::setIdentity( void )
{
    m_Min.setup(  X_F32_MAX,  X_F32_MAX,  X_F32_MAX );
    m_Max.setup( -X_F32_MAX, -X_F32_MAX, -X_F32_MAX );
}

//------------------------------------------------------------------------------
inline
bool xbbox::isValid( void ) const
{
    if( !(m_Min.isValid() && m_Max.isValid()) )
        return false;

    if( !(m_Min.getMin( m_Max ) == m_Min) ) 
        return false;

    return m_Max.getMax( m_Min ) == m_Max;
}

//------------------------------------------------------------------------------
inline
xbbox& xbbox::setup( const xvector3d& Center, const f32 Radius )
{
    m_Min = Center - xvector3d(Radius);
    m_Max = Center + xvector3d(Radius);
    return *this;
}

//------------------------------------------------------------------------------
inline
xbbox& xbbox::setup( const xvector3d& P1, const xvector3d& P2 )
{
    m_Min = P1.getMin( P2 );
    m_Max = P1.getMax( P2 );
    return *this;
}

//------------------------------------------------------------------------------
inline
xvector3 xbbox::getSize( void ) const
{
    return m_Max - m_Min;
}

//------------------------------------------------------------------------------
inline
xvector3 xbbox::getCenter( void ) const
{
    return (m_Min + m_Max) * 0.5f;
}

//------------------------------------------------------------------------------
inline
f32 xbbox::getRadius( void ) const
{
    return getSize().getLength() * 0.5f;
}

//------------------------------------------------------------------------------
inline
f32 xbbox::getRadiusSquared( void ) const
{
    return getSize().getLengthSquared() * 0.5f;
}

//------------------------------------------------------------------------------
inline
f32 xbbox::getSurfaceArea( void ) const
{
    xvector3d Size( getSize() );
    return ( Size.m_X*Size.m_Y + 
             Size.m_Y*Size.m_Z +
             Size.m_Z*Size.m_X ) * 2.0f;
}

//------------------------------------------------------------------------------
inline
bool xbbox::InRange( const f32 Min, const f32 Max ) const
{
    return m_Min.isInrange( Min, Max ) && m_Max.isInrange( Min, Max );
}

//------------------------------------------------------------------------------
inline
xbbox& xbbox::Inflate( const xvector3d& S )
{
    m_Min -= S;
    m_Max += S;
    return *this;
}

//------------------------------------------------------------------------------
inline
xbbox& xbbox::Deflate( const xvector3d& S )
{
    m_Min += S;
    m_Max -= S;
    return *this;
}

//------------------------------------------------------------------------------
inline
xbbox& xbbox::Translate( const xvector3d& Delta )
{
    m_Min += Delta;
    m_Max += Delta;
    return *this;
}

//------------------------------------------------------------------------------
inline
xbbox& xbbox::Transform( const xmatrix4& M )
{
    xvector3d Min( m_Min ), Max( m_Max );
    f32     a, b;
    s32     i, j;

    // setup the xbbox to be at the translation point
    m_Max = m_Min.setup( M(0,3), M(1,3), M(2,3) );

    // Find extreme points by considering product of 
    // min and max with each component of M.
    for( j=0; j<3; j++ )
    {
        for( i=0; i<3; i++ )
        {
            a = M(j,i) * Min[i ];
            b = M(j,i) * Max[i ];

            if( a < b )
            {
                Min[j ] += a;
                Max[j ] += b;
            }
            else
            {
                Min[j ] += b;
                Max[j ] += a;
            }
        }
    }

    return *this;
}

//------------------------------------------------------------------------------
inline
bool xbbox::Intersect( const xvector3d& Point ) const
{
    return (Point.m_X <= m_Max.m_X) && 
           (Point.m_Y <= m_Max.m_Y) && 
           (Point.m_Z <= m_Max.m_Z) && 
           (Point.m_X >= m_Min.m_X) && 
           (Point.m_Y >= m_Min.m_Y) && 
           (Point.m_Z >= m_Min.m_Z);
}

//------------------------------------------------------------------------------
inline
bool xbbox::Intersect( const xbbox& xbbox ) const
{
    return (xbbox.m_Min.m_X >= m_Max.m_X) && 
           (xbbox.m_Max.m_Y <= m_Max.m_Y) && 
           (xbbox.m_Min.m_Z >= m_Max.m_Z) && 
           (xbbox.m_Max.m_X <= m_Min.m_X) && 
           (xbbox.m_Min.m_Y >= m_Min.m_Y) && 
           (xbbox.m_Max.m_Z <= m_Min.m_Z);
}

//------------------------------------------------------------------------------
inline
bool xbbox::Intersect( const xplane& Plane ) const
{
    xvector3d PMin, PMax;

    if( Plane.m_Normal.m_X > 0 )   
    { 
        PMax.m_X = m_Max.m_X;   
        PMin.m_X = m_Min.m_X; 
    }
    else                           
    { 
        PMax.m_X = m_Min.m_X;   
        PMin.m_X = m_Max.m_X; 
    }

    if( Plane.m_Normal.m_Y > 0 )   
    { 
        PMax.m_Y = m_Max.m_Y;   
        PMin.m_Y = m_Min.m_Y; 
    }
    else                           
    { 
        PMax.m_Y = m_Min.m_Y;   
        PMin.m_Y = m_Max.m_Y; 
    }

    if( Plane.m_Normal.m_Z > 0 )   
    { 
        PMax.m_Z = m_Max.m_Z;   
        PMin.m_Z = m_Min.m_Z; 
    }
    else                           
    { 
        PMax.m_Z = m_Min.m_Z;   
        PMin.m_Z = m_Max.m_Z; 
    }

    return (Plane.getDistance( PMax ) >= 0.0f) && 
           (Plane.getDistance( PMin ) <= 0.0f);
}

//------------------------------------------------------------------------------
inline
bool xbbox::Intersect( f32& t, const xvector3d& P0, const xvector3d& P1 ) const
{                      
    f32         PlaneD   [3];
    bool        PlaneUsed[3] = { true, true, true };
    f32         T        [3] = { -1, -1, -1 };
    xvector3d   Direction( P1 - P0 );
    s32         MaxPlane;
    s32         i;
    f32         Component;

    // setup a value until we have something better.
    t = 0.0f;

    // Consider relationship of each component of P0 to the box.
    for( i = 0; i < 3; i++ )
    {
        if     ( P0[i ] > m_Max[i ] )   { PlaneD[i]    = m_Max[i ]; }
        else if( P0[i ] < m_Min[i ] )   { PlaneD[i]    = m_Min[i ]; }
        else                          { PlaneUsed[i] = false;  }
    }

    // Is the starting point in the box?
    if( !PlaneUsed[0] && !PlaneUsed[1] && !PlaneUsed[2] )
        return true;

    // For each plane to be used, compute the distance to the plane.
    for( i = 0; i < 3; i++ )
    {
        if( PlaneUsed[i] && (Direction[i ] != 0.0f) )
            T[i] = (PlaneD[i] - P0[i ]) / Direction[i ];
    }

    // We need to know which plane had the largest distance.
    if( T[0] > T[1] )
    {
        MaxPlane = ((T[0] > T[2]) ? 0 : 2);
    }
    else
    {
        MaxPlane = ((T[1] > T[2]) ? 1 : 2);
    }

    // If the largest plane distance is less than zero, then there is no hit.
    if( T[MaxPlane] < 0.0f )
        return false;

    // See if the point we think is the hit point is a real hit.
    for( i = 0; i < 3; i++ )
    {
        // See if component 'i' of the hit point is on the box.
        if( i != MaxPlane )
        {
            Component = P0[i ] + T[MaxPlane] * Direction[i ];
            if( (Component < m_Min[i ]) || (Component > m_Max[i ]) )
            {
                // We missed!  Hit point was not on the box.
                return false;
            }
        }
    }

    // We have a verified hit.  setup t and we're done.
    t = T[MaxPlane];
    return true ;
}

//------------------------------------------------------------------------------
inline
bool xbbox::IntersectTriangleBBox( const xvector3d& P0,
                                   const xvector3d& P1,
                                   const xvector3d& P2 )  const
{
    xbbox Trixbbox;

    //
    // HANDLE X
    //
    {
        Trixbbox.m_Min.m_X = P0.m_X;
        Trixbbox.m_Max.m_X = P0.m_X;

        if( P2.m_X > P1.m_X )
        {
            if( P2.m_X > Trixbbox.m_Max.m_X )  Trixbbox.m_Max.m_X = P2.m_X;
            if( P1.m_X < Trixbbox.m_Min.m_X )  Trixbbox.m_Min.m_X = P1.m_X;
        }
        else
        {
            if( P1.m_X > Trixbbox.m_Max.m_X )  Trixbbox.m_Max.m_X = P1.m_X;
            if( P2.m_X < Trixbbox.m_Min.m_X )  Trixbbox.m_Min.m_X = P2.m_X;
        }

        // X's are solved so compare to xbbox.
        if( m_Min.m_X > Trixbbox.m_Max.m_X ) return false;
        if( m_Max.m_X < Trixbbox.m_Min.m_X ) return false;
    }

    //
    // HANDLE Z
    //
    {
        Trixbbox.m_Min.m_Z = P0.m_Z;
        Trixbbox.m_Max.m_Z = P0.m_Z;

        if( P2.m_Z > P1.m_Z )
        {
            if( P2.m_Z > Trixbbox.m_Max.m_Z )  Trixbbox.m_Max.m_Z = P2.m_Z;
            if( P1.m_Z < Trixbbox.m_Min.m_Z )  Trixbbox.m_Min.m_Z = P1.m_Z;
        }
        else
        {
            if( P1.m_Z > Trixbbox.m_Max.m_Z )  Trixbbox.m_Max.m_Z = P1.m_Z;
            if( P2.m_Z < Trixbbox.m_Min.m_Z )  Trixbbox.m_Min.m_Z = P2.m_Z;
        }

        // Y's are solved so compare to xbbox.
        if( m_Min.m_Z > Trixbbox.m_Max.m_Z ) return false;
        if( m_Max.m_Z < Trixbbox.m_Min.m_Z ) return false;
    }

    //
    // HANDLE Y
    //
    {
        Trixbbox.m_Min.m_Y = P0.m_Y;
        Trixbbox.m_Max.m_Y = P0.m_Y;

        if( P2.m_Y > P1.m_Y )
        {
            if( P2.m_Y > Trixbbox.m_Max.m_Y )  Trixbbox.m_Max.m_Y = P2.m_Y;
            if( P1.m_Y < Trixbbox.m_Min.m_Y )  Trixbbox.m_Min.m_Y = P1.m_Y;
        }
        else
        {
            if( P1.m_Y > Trixbbox.m_Max.m_Y )  Trixbbox.m_Max.m_Y = P1.m_Y;
            if( P2.m_Y < Trixbbox.m_Min.m_Y )  Trixbbox.m_Min.m_Y = P2.m_Y;
        }

        // Z's are solved so compare to xbbox.
        if( m_Min.m_Y > Trixbbox.m_Max.m_Y ) return false;
        if( m_Max.m_Y < Trixbbox.m_Min.m_Y ) return false;
    }

    return false;
}

//------------------------------------------------------------------------------
inline
bool xbbox::Contains( const xbbox& BBox  ) const
{
    if( BBox.m_Min.m_X >= m_Min.m_X &&
        BBox.m_Max.m_X <= m_Max.m_X &&
        BBox.m_Min.m_Z >= m_Min.m_Z &&
        BBox.m_Max.m_Z <= m_Max.m_Z &&
        BBox.m_Min.m_Y >= m_Min.m_Y &&
        BBox.m_Max.m_Y <= m_Max.m_Y )
        return true;

    return false;
}

//------------------------------------------------------------------------------
inline 
xbbox& xbbox::AddVerts( const xvector3d* pVerts, s32 nVerts )
{
    x_assert( pVerts );
    x_assert( nVerts > 0 );

    for( s32 i=0; i<nVerts; i++ )
    {
        m_Min = m_Min.getMin( pVerts[i] );
        m_Max = m_Max.getMax( pVerts[i] );
    }

    return *this;
}

//------------------------------------------------------------------------------
//can't just use xvector3d in place of of xvector3 for an array, because they have different sizes
inline 
xbbox& xbbox::AddVerts( const xvector3* pVerts, s32 nVerts )
{
    x_assert( pVerts );
    x_assert( nVerts > 0 );

    for( s32 i=0; i<nVerts; i++ )
    {
        m_Min = m_Min.getMin( pVerts[i] );
        m_Max = m_Max.getMax( pVerts[i] );
    }

    return *this;
}

//------------------------------------------------------------------------------
inline
void xbbox::getVerts( xvector3d* pVerts, s32 nVerts ) const
{
    x_assert( nVerts >= 8 );
    pVerts[0].setup( m_Min.m_X, m_Min.m_Y, m_Min.m_Z );
    pVerts[1].setup( m_Max.m_X, m_Min.m_Y, m_Min.m_Z );
    pVerts[2].setup( m_Max.m_X, m_Max.m_Y, m_Min.m_Z );
    pVerts[3].setup( m_Min.m_X, m_Max.m_Y, m_Min.m_Z );

    pVerts[4].setup( m_Min.m_X, m_Min.m_Y, m_Max.m_Z );
    pVerts[5].setup( m_Max.m_X, m_Min.m_Y, m_Max.m_Z );
    pVerts[6].setup( m_Max.m_X, m_Max.m_Y, m_Max.m_Z );
    pVerts[7].setup( m_Min.m_X, m_Max.m_Y, m_Max.m_Z );
}

//------------------------------------------------------------------------------
// Note: The distance will be zero if inside the box.
inline
f32 xbbox::getClosestVertex( xvector3d& ClosestVertex, const xvector3d& Point ) const
{
    // This will be modified to become the closest point if it's outside the box.
    ClosestVertex = Point;

    // get the distance from the bounding box along the each axis.
    f32 dist_to_max;
    f32 dist_to_min; 

    dist_to_max = ClosestVertex.m_X - m_Max.m_X;
    dist_to_min = m_Min.m_X - ClosestVertex.m_X;
    ClosestVertex.m_X += x_FSel(dist_to_max, -dist_to_max, 0.0f) + x_FSel(dist_to_min, dist_to_min, 0.0f);
    f32 dist_x         = x_FSel(dist_to_max,  dist_to_max, 0.0f) + x_FSel(dist_to_min, dist_to_min, 0.0f);

    dist_to_max = ClosestVertex.m_Y - m_Max.m_Y;
    dist_to_min = m_Min.m_Y - ClosestVertex.m_Y;
    ClosestVertex.m_Y += x_FSel(dist_to_max, -dist_to_max, 0.0f) + x_FSel(dist_to_min, dist_to_min, 0.0f);
    f32 dist_y        = x_FSel(dist_to_max,  dist_to_max, 0.0f) + x_FSel(dist_to_min, dist_to_min, 0.0f);

    dist_to_max = ClosestVertex.m_Z - m_Max.m_Z;
    dist_to_min = m_Min.m_Z - ClosestVertex.m_Z;
    ClosestVertex.m_Z += x_FSel(dist_to_max, -dist_to_max, 0.0f) + x_FSel(dist_to_min, dist_to_min, 0.0f);
    f32 dist_z         = x_FSel(dist_to_max,  dist_to_max, 0.0f) + x_FSel(dist_to_min, dist_to_min, 0.0f);

    // Squared distance = x^2 + y^2 + z^2
    return (x_Sqr(dist_x) + x_Sqr(dist_y) + x_Sqr(dist_z));
}

//------------------------------------------------------------------------------
inline 
xbbox operator + ( const xbbox& BBox1, const xbbox& BBox2 )
{
    return xbbox( BBox1 ) += BBox2;
}

//------------------------------------------------------------------------------
inline 
xbbox operator + ( const xbbox& BBox, const xvector3d& Point )
{
    return xbbox( BBox ) += Point;
}

//------------------------------------------------------------------------------
inline 
xbbox operator + ( const xvector3d& Point, const xbbox& BBox )
{
    return xbbox( BBox ) += Point;
}

//------------------------------------------------------------------------------
inline 
const xbbox& xbbox::operator += ( const xbbox& BBox )
{
    m_Min = m_Min.getMin( BBox.m_Min );
    m_Max = m_Max.getMax( BBox.m_Max );
    return *this;
}

//------------------------------------------------------------------------------
inline 
const xbbox& xbbox::operator += ( const xvector3d& Point )
{
    m_Min = m_Min.getMin( Point );
    m_Max = m_Max.getMax( Point );
    return *this;
}

