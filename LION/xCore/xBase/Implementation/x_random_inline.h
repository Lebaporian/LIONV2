//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//--------------------------------------------------------------------------
// The random generator seed
x_inline
void xrandom_small::setSeed64( u64 u ) noexcept
{
    m_W = u32(u);
    m_Z = u32(u>>32);
    
    // The seed can not be zero in the lower bits
    x_assert( m_W );
    
    // This one can not be zero neither but we will fix it for the user
    if( m_Z == 0 ) m_Z = 362436069;
}
    
//--------------------------------------------------------------------------
x_inline
void xrandom_small::setSeed32( u32 u ) noexcept
{
    m_W = u32(u);
    m_Z = 362436069;
    
    x_assert( m_W );
}
    
//--------------------------------------------------------------------------
// get the full seed.
x_inline
u64 xrandom_small::getSeed( void ) const noexcept
{
    u64 Seed = (u64(m_W)<<0) |
    (u64(m_Z)<<32);
    
    return Seed;
}
    
//--------------------------------------------------------------------------
// Produce a uniform random sample from the open interval (0, 1).
// The method will not return either end point.
x_inline
f64 xrandom_small::RandF64( void ) noexcept
{
    // 0 <= u < 2^32
    u32 u = Rand32();
    // The magic number below is 1/(2^32 + 2).
    // The result is strictly between 0 and 1.
    return (u + 1.0) * 2.328306435454494e-10;
}

//--------------------------------------------------------------------------
// Produce a uniform random sample from the open interval (0, 1).
// The method will not return either end point.
x_inline
f32 xrandom_small::RandF32( void ) noexcept
{
    return f32(RandF64());
}

//------------------------------------------------------------------------------
x_inline
f32 xrandom_small::RandF32( f32 Start, f32 End ) noexcept
{
    return Start+RandF32()*(End-Start);
}

//------------------------------------------------------------------------------
x_inline
s32 xrandom_small::Rand32( s32 Start, s32 End ) noexcept
{
    const s32 L = End-Start;
    return Start+(Rand32()%L);
}

//------------------------------------------------------------------------------
x_inline
f64 xrandom_small::RandF64( f64 Start, f64 End ) noexcept
{
    return Start+RandF64()*(End-Start);
}

//--------------------------------------------------------------------------
// This is the heart of the generator.
// It uses George Marsaglia's MWC algorithm to produce an unsigned integer.
// See http://www.bobwheeler.com/statistics/Password/MarsagliaPost.txt
x_inline
u32 xrandom_small::Rand32( void ) noexcept
{
    m_Z = 36969 * (m_Z & 65535) + (m_Z >> 16);
    m_W = 18000 * (m_W & 65535) + (m_W >> 16);
    return (m_Z << 16) + m_W;
}

//--------------------------------------------------------------------------
// Get normal (Gaussian) random sample with mean 0 and standard deviation 1
x_inline
f64 xrandom_small::Normal( void ) noexcept
{
    // Use Box-Muller algorithm
    f64 u1 = RandF64();
    f64 u2 = RandF64();
    f64 r = x_SqrtF64( -2.0f * x_LogF64(u1) );
    f64 theta = 2.0 * PI_64.m_Value * u2;
    return r*x_SinF64( xradian64{ theta } );
}

//--------------------------------------------------------------------------
// Get normal (Gaussian) random sample with specified mean and standard deviation
x_inline
f64 xrandom_small::Normal( f64 mean, f64 standardDeviation ) noexcept
{
    // Shape must be positive. Received {0}.
    x_assert(standardDeviation > 0.0 );
    return mean + standardDeviation * Normal();
}

//--------------------------------------------------------------------------
// Get exponential random sample with mean 1
x_inline
f64 xrandom_small::Exponential( void ) noexcept
{
    return x_LogF64( RandF64() );
}

//--------------------------------------------------------------------------
// Get exponential random sample with specified mean
x_inline
f64 xrandom_small::Exponential( f64 mean ) noexcept
{
    // Mean must be positive. Received {0}.
    x_assert(mean > 0.0);
    return mean * Exponential();
}

//--------------------------------------------------------------------------
x_inline
f64 xrandom_small::Gamma( f64 shape, f64 scale ) noexcept
{
    // Implementation based on "A Simple Method for Generating Gamma Variables"
    // by George Marsaglia and Wai Wan Tsang.  ACM Transactions on Mathematical Software
    // Vol 26, No 3, September 2000, pages 363-372.
    f64 d, c, x, xsquared, v, u;
    
    if (shape >= 1.0)
    {
        d = shape - 1.0/3.0;
        c = 1.0/x_SqrtF64(9.0*d);
        for (;;)
        {
            do
            {
                x = Normal();
                v = 1.0 + c*x;
            }
            while (v <= 0.0);
            v = v*v*v;
            u = RandF64();
            xsquared = x*x;
            if (u < 1.0 -.0331*xsquared*xsquared || x_LogF64(u) < 0.5*xsquared + d*(1.0 - v + x_LogF64(v)))
                return scale*d*v;
        }
    }
    else
    {
        // Shape must be positive. Received {0}
        x_assert( shape > 0.0 );
        
        f64 g = Gamma(shape+1.0, 1.0);
        f64 w = RandF64();
        return scale*g*x_PowF64(w, 1.0/shape);
    }
}

//--------------------------------------------------------------------------
x_inline
f64 xrandom_small::ChiSquare( f64 degreesOfFreedom ) noexcept
{
    // A chi squared distribution with n degrees of freedom
    // is a gamma distribution with shape n/2 and scale 2.
    return Gamma(0.5 * degreesOfFreedom, 2.0);
}

//--------------------------------------------------------------------------
x_inline
f64 xrandom_small::InverseGamma( f64 shape, f64 scale ) noexcept
{
    // If X is gamma(shape, scale) then
    // 1/Y is inverse gamma(shape, 1/scale)
    return 1.0 / Gamma(shape, 1.0 / scale);
}

//--------------------------------------------------------------------------
x_inline
f64 xrandom_small::Weibull( f64 shape, f64 scale ) noexcept
{
    // Shape and scale parameters must be positive. Recieved shape {0} and scale{1}.
    x_assert( shape > 0 && scale > 0 );
    
    return scale * x_PowF64(-x_LogF64(RandF64()), 1.0 / shape);
}

//--------------------------------------------------------------------------
x_inline
f64 xrandom_small::Cauchy( f64 median, f64 scale ) noexcept
{
    // Scale must be positive. Received {0}
    x_assert( scale > 0 );
    
    f64 p = RandF64();
    
    // Apply inverse of the Cauchy distribution function to a uniform
    return median + scale*x_TanF64( xradian64{ PI_64.m_Value*(p - 0.5) } );
}

//--------------------------------------------------------------------------
x_inline
f64 xrandom_small::StudentT( f64 degreesOfFreedom ) noexcept
{
    // Degrees of freedom must be positive. Received {0}.
    x_assert( degreesOfFreedom > 0 );
    
    // See Seminumerical Algorithms by Knuth
    f64 y1 = Normal();
    f64 y2 = ChiSquare(degreesOfFreedom);
    return y1 / x_SqrtF64(y2 / degreesOfFreedom);
}

//--------------------------------------------------------------------------
// The Laplace distribution is also known as the double exponential distribution.
x_inline
f64 xrandom_small::Laplace( f64 mean, f64 scale ) noexcept
{
    f64 u = RandF64();
    return (u < 0.5) ?
    mean + scale*x_LogF64(2.0*u) :
    mean - scale*x_LogF64(2*(1-u));
}

//--------------------------------------------------------------------------
x_inline
f64 xrandom_small::LogNormal( f64 mu, f64 sigma ) noexcept
{
    return x_ExpF64(Normal(mu, sigma));
}

//--------------------------------------------------------------------------
x_inline
f64 xrandom_small::Beta( f64 a, f64 b ) noexcept
{
    // Beta parameters must be positive. Received {0} and {1}.
    x_assert( a > 0 && b > 0 );
    
    // There are more efficient methods for generating beta samples.
    // However such methods are a little more efficient and much more complicated.
    // For an explanation of why the following method works, see
    // http://www.johndcook.com/distribution_chart.html#gamma_beta
    
    f64 u = Gamma(a, 1.0);
    f64 v = Gamma(b, 1.0);
    return u / (u + v);
}

