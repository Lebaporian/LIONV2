//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//==============================================================================
// FUNCTIONS
//==============================================================================


//------------------------------------------------------------------------------
inline
xirect::xirect( s32 Left, s32 Top, s32 Right, s32 Bottom )
{
    setup( Left, Top, Right, Bottom );
}

//------------------------------------------------------------------------------
inline
xirect& xirect::setup( s32 Left, s32 Top, s32 Right, s32 Bottom )
{
    m_Left   = Left;
    m_Top    = Top;
    m_Right  = Right;
    m_Bottom = Bottom;
    return *this;
}

//------------------------------------------------------------------------------
inline
void xirect::setZero( void )
{
    m_Left      = 0;
    m_Top       = 0;
    m_Right     = 0;
    m_Bottom    = 0;
}

//------------------------------------------------------------------------------
inline
void xirect::setMax( void )
{
    m_Left      =  X_S32_MAX;
    m_Top       =  X_S32_MAX;
    m_Right     = -X_S32_MAX;
    m_Bottom    = -X_S32_MAX;
}

//------------------------------------------------------------------------------
inline
bool xirect::Intersect( const xirect& Rect )
{
    return  ( m_Left   <= Rect.m_Right  ) &&
            ( m_Top    <= Rect.m_Bottom ) &&
            ( m_Right  >= Rect.m_Left   ) &&
            ( m_Bottom >= Rect.m_Top    );
}

//------------------------------------------------------------------------------
inline
bool xirect::Intersect( xirect& R, const xirect& Rect )
{
    if( Intersect( Rect ) == false )
        return( false );

    R.m_Left    = x_Max( m_Left,   Rect.m_Left    );
    R.m_Top     = x_Max( m_Top,    Rect.m_Top     );
    R.m_Right   = x_Min( m_Right,  Rect.m_Right   );
    R.m_Bottom  = x_Min( m_Bottom, Rect.m_Bottom  );

    return true ;
}

//------------------------------------------------------------------------------
inline
bool xirect::PointInRect( s32 X, s32 Y ) const
{
    return ((X >= m_Left) && (X <= m_Right) && (Y >= m_Top) && (Y <= m_Bottom));
}

//------------------------------------------------------------------------------
inline
xirect& xirect::AddPoint( s32 X, s32 Y )
{
    m_Left   = x_Min( m_Left  , X );
    m_Top    = x_Min( m_Top   , Y );
    m_Right  = x_Max( m_Right , X );
    m_Bottom = x_Max( m_Bottom, Y );
    return *this;
}

//------------------------------------------------------------------------------
inline
xirect& xirect::AddRect( const xirect& Rect )
{
    m_Left   = x_Min( m_Left  , Rect.m_Left   );
    m_Top    = x_Min( m_Top   , Rect.m_Top    );
    m_Right  = x_Max( m_Right , Rect.m_Right  );
    m_Bottom = x_Max( m_Bottom, Rect.m_Bottom );
    return *this;
}

//------------------------------------------------------------------------------
inline
s32 xirect::getWidth( void ) const
{
    return m_Right - m_Left;
}

//------------------------------------------------------------------------------
inline
s32 xirect::getHeight( void ) const
{
    return m_Bottom - m_Top;
}

//------------------------------------------------------------------------------
inline
xvector2 xirect::getSize( void ) const
{
    return xvector2( (f32)getWidth(), (f32)getHeight() );
}

//------------------------------------------------------------------------------
inline
xvector2 xirect::getCenter( void ) const
{
    return xvector2( (f32)getWidth()/2.0f, (f32)getHeight()/2.0f );
}

//------------------------------------------------------------------------------
inline
xirect& xirect::setWidth( s32 W )
{
    m_Right = m_Left + W;
    return *this;
}

//------------------------------------------------------------------------------
inline
xirect& xirect::setHeight( s32 H )
{
    m_Bottom = m_Top + H;
    return *this;
}

//------------------------------------------------------------------------------
inline
xirect& xirect::setSize( s32 W, s32 H )
{
    m_Right  = m_Left + W;
    m_Bottom = m_Top  + H;
    return *this;
}

//------------------------------------------------------------------------------
inline
xirect& xirect::Translate( s32 X, s32 Y )
{
    m_Left   += X;
    m_Top    += X;
    m_Right  += Y;
    m_Bottom += Y;
    return *this;
}

//------------------------------------------------------------------------------
inline
xirect& xirect::Inflate( s32 X, s32 Y )
{
    m_Left    -= X;
    m_Top     += X;
    m_Right   -= Y;
    m_Bottom  += Y;
    return *this;
}

//------------------------------------------------------------------------------
inline
xirect& xirect::Deflate( s32 X, s32 Y )
{
    m_Left    += X;
    m_Top     -= X;
    m_Right   += Y;
    m_Bottom  -= Y;
    return *this;
}

//------------------------------------------------------------------------------
inline
bool xirect::InRange( s32 Min, s32 Max ) const
{
    return (m_Left   >= Min) && (m_Left   <= Max) &&
           (m_Top    >= Min) && (m_Top    <= Max) &&
           (m_Right  >= Min) && (m_Right  <= Max) && 
           (m_Bottom >= Min) && (m_Bottom <= Max);
}

//------------------------------------------------------------------------------
inline
bool xirect::isEmpty( void ) const
{
    return( (m_Left>=m_Right) || (m_Top>=m_Bottom) );
}

//------------------------------------------------------------------------------
inline
bool xirect::operator == ( const xirect& R ) const
{
    return( (m_Left   == R.m_Left  ) &&
            (m_Top    == R.m_Top   ) &&
            (m_Right  == R.m_Right ) &&
            (m_Bottom == R.m_Bottom) );
}

//------------------------------------------------------------------------------
inline
bool xirect::operator != ( const xirect& R ) const
{
    return( (m_Left   != R.m_Left  ) ||
            (m_Top    != R.m_Top   ) ||
            (m_Right  != R.m_Right ) ||
            (m_Bottom != R.m_Bottom) );
}
