//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//==============================================================================
// FUNCTIONS
//==============================================================================

//------------------------------------------------------------------------------

constexpr
xvector3d::xvector3d( const f32x4& Register ) :
    m_X{ x_getf32x4ElementByIndex<0>(Register) },
    m_Y{ x_getf32x4ElementByIndex<1>(Register) },
    m_Z{ x_getf32x4ElementByIndex<2>(Register) } {}

//------------------------------------------------------------------------------
inline
f32* xvector3d::operator()( void )
{
    return &m_X;
}

//------------------------------------------------------------------------------
constexpr
xvector3d::xvector3d( const xvector3& V ) : m_X(V.m_X), m_Y(V.m_Y), m_Z(V.m_Z) {}

//------------------------------------------------------------------------------
constexpr
f32 xvector3d::operator []( const s32 i ) const
{
    return  x_assert( i >= 0 && i <= 3 ),
            (&m_X)[i];
}

//------------------------------------------------------------------------------
inline
f32& xvector3d::operator []( const s32 i )
{
    return  x_assert( i >= 0 && i <= 3 ),
            (&m_X)[i];
}

//------------------------------------------------------------------------------
inline 
void xvector3d::setZero( void )
{
    m_X=m_Y=m_Z=0;
}

//------------------------------------------------------------------------------
inline
void xvector3d::setIdentity( void )
{
    m_X=m_Y=m_Z=0;
}

//------------------------------------------------------------------------------
inline 
xvector3d& xvector3d::setup( f32 X, f32 Y, f32 Z )
{
    m_X = X;
    m_Y = Y;
    m_Z = Z;
	x_assert( isValid() );
    return *this;
}

//------------------------------------------------------------------------------
inline 
xvector3d& xvector3d::setup( const f32 n )
{
    m_X = n;
    m_Y = n;
    m_Z = n;
    x_assert( isValid() );
    return *this;
}

//------------------------------------------------------------------------------
inline 
f32 xvector3d::getLength( void ) const
{
    return x_Sqrt( m_X*m_X + m_Y*m_Y + m_Z*m_Z );
}

//------------------------------------------------------------------------------
constexpr
f32 xvector3d::getLengthSquared( void ) const
{
    return m_X*m_X + m_Y*m_Y + m_Z*m_Z;
}

//------------------------------------------------------------------------------
constexpr
f32 xvector3d::getDistanceSquare( const xvector3d& V ) const
{
    return x_Sqr(m_X - V.m_X) +
           x_Sqr(m_Y - V.m_Y) +
           x_Sqr(m_Z - V.m_Z);
}

//------------------------------------------------------------------------------
inline
f32 xvector3d::getDistance( const xvector3d& V ) const
{
    return x_Sqrt( getDistanceSquare(V) );
}

//------------------------------------------------------------------------------
inline 
xvector3d& xvector3d::Normalize( void )
{
    const f32 imag = x_InvSqrt( m_X*m_X + m_Y*m_Y + m_Z*m_Z );
    m_X *= imag;
    m_Y *= imag;
    m_Z *= imag;
    return *this;
}

//------------------------------------------------------------------------------
inline 
xvector3d& xvector3d::NormalizeSafe( void )
{
    const f32 sqrdis = m_X*m_X + m_Y*m_Y + m_Z*m_Z;

    if( sqrdis < 0.0001f )
    {
        m_X = 1;
        m_Y = 0;
        m_Z = 0;
        return *this;
    }

	const f32 imag = x_InvSqrt( sqrdis );

    m_X *= imag;
    m_Y *= imag;
    m_Z *= imag;
    return *this;
}

//------------------------------------------------------------------------------
inline 
const xvector3d& xvector3d::operator += ( const xvector3d& V )
{
    m_X += V.m_X;
    m_Y += V.m_Y;
    m_Z += V.m_Z;
    return *this;
}

//------------------------------------------------------------------------------
inline 
const xvector3d& xvector3d::operator -= ( const xvector3d& V )
{
    m_X -= V.m_X;
    m_Y -= V.m_Y;
    m_Z -= V.m_Z;
    return *this;
}

//------------------------------------------------------------------------------
inline 
const xvector3d& xvector3d::operator *= ( const xvector3d& V )
{
    m_X *= V.m_X;
    m_Y *= V.m_Y;
    m_Z *= V.m_Z;
    return *this;
}

//------------------------------------------------------------------------------
inline 
const xvector3d& xvector3d::operator *= ( f32 Scalar )
{
    m_X *= Scalar;
    m_Y *= Scalar;
    m_Z *= Scalar;
    return *this;
}

//------------------------------------------------------------------------------
inline 
const xvector3d& xvector3d::operator /= ( f32 Div )
{
    const f32 Scalar = 1.0f/Div;
    m_X *= Scalar;
    m_Y *= Scalar;
    m_Z *= Scalar;
    return *this;
}

//------------------------------------------------------------------------------
inline 
xvector3 operator / ( const xvector3d& V, f32 Div )
{
    const f32 Scalar = 1.0f/Div;
    return xvector3( V.m_X * Scalar, V.m_Y * Scalar, V.m_Z * Scalar );
}

//------------------------------------------------------------------------------
constexpr 
xvector3 operator * ( const xvector3d& V, f32 Scalar )
{
    return xvector3( V.m_X * Scalar, V.m_Y * Scalar, V.m_Z * Scalar );
}

//------------------------------------------------------------------------------
constexpr 
xvector3 operator * ( f32 Scalar, const xvector3d& V )
{
    return xvector3( V.m_X * Scalar, V.m_Y * Scalar, V.m_Z * Scalar );
}

//------------------------------------------------------------------------------
constexpr 
bool xvector3d::operator == ( const xvector3d& V ) const
{
    return 
    (x_Abs( V.m_X - m_X) <= XFLT_TOL) &&
    (x_Abs( V.m_Y - m_Y) <= XFLT_TOL) &&
    (x_Abs( V.m_Z - m_Z) <= XFLT_TOL);
}

//------------------------------------------------------------------------------
inline 
xvector3 xvector3d::getMin( const xvector3d& V ) const
{
    return { x_Min( V.m_X, m_X ), x_Min( V.m_Y, m_Y ), x_Min( V.m_Z, m_Z ) };
}

//------------------------------------------------------------------------------
inline 
xvector3 xvector3d::getMax( const xvector3d& V ) const
{
    return { x_Max( V.m_X, m_X ), x_Max( V.m_Y, m_Y ), x_Max( V.m_Z, m_Z ) };
}

//------------------------------------------------------------------------------
constexpr 
xvector3 operator + ( const xvector3d& V0, const xvector3d& V1 )
{
    return { V0.m_X + V1.m_X, V0.m_Y + V1.m_Y, V0.m_Z + V1.m_Z };
}

//------------------------------------------------------------------------------
constexpr 
xvector3 operator - ( const xvector3d& V0, const xvector3d& V1 )
{
    return { V0.m_X - V1.m_X, V0.m_Y - V1.m_Y, V0.m_Z - V1.m_Z };
}

//------------------------------------------------------------------------------
constexpr 
xvector3 operator * ( const xvector3d& V0, const xvector3d& V1 )
{
    return { V0.m_X * V1.m_X, V0.m_Y * V1.m_Y, V0.m_Z * V1.m_Z };
}

//------------------------------------------------------------------------------
constexpr 
xvector3 operator / ( const xvector3d& V0, const xvector3d& V1 )
{
    return { V0.m_X / V1.m_X, V0.m_Y / V1.m_Y, V0.m_Z / V1.m_Z };
}

//------------------------------------------------------------------------------
constexpr 
xvector3 operator - ( const xvector3d& V )
{
    return { -V.m_X, -V.m_Y, -V.m_Z };
}

//------------------------------------------------------------------------------
constexpr 
xvector3 xvector3d::getLerp( f32 t, const xvector3d& V ) const
{
    return { m_X + (( V.m_X - m_X ) * t),
             m_Y + (( V.m_Y - m_Y ) * t),
             m_Z + (( V.m_Z - m_Z ) * t) };
}

//------------------------------------------------------------------------------
constexpr 
f32 xvector3d::Dot( const xvector3d& V ) const
{
    return V.m_X * m_X + V.m_Y * m_Y + V.m_Z * m_Z;
}

//------------------------------------------------------------------------------
constexpr 
xvector3 xvector3d::Cross( const xvector3d& V ) const
{
    return { m_Y * V.m_Z - m_Z * V.m_Y,
             m_Z * V.m_X - m_X * V.m_Z,
             m_X * V.m_Y - m_Y * V.m_X };   
}


//------------------------------------------------------------------------------
x_forceinline 
bool xvector3d::isValid( void ) const
{
    return x_isValid(m_X) && x_isValid(m_Y) && x_isValid(m_Z);
}

//------------------------------------------------------------------------------
inline
xvector3d& xvector3d::Abs( void )
{
     m_X = x_Abs( m_X );
     m_Y = x_Abs( m_Y );
     m_Z = x_Abs( m_Z );
    return *this;
}

//------------------------------------------------------------------------------
constexpr
bool xvector3d::isInrange( const f32 Min, const f32 Max ) const
{
    return x_isInrange( m_X, Min, Max ) && x_isInrange( m_Y, Min, Max ) && x_isInrange( m_Z, Min, Max );
}

//------------------------------------------------------------------------------
constexpr
xvector3 xvector3d::getOneOver( void ) const
{
    return xvector3( 1.0f/m_X, 1.0f/m_Y, 1.0f/m_Z  );
}

/*
//------------------------------------------------------------------------------
inline
xvector3d xvector3d::getEulerZYZ( void ) const
{
    // http://vered.rose.utoronto.ca/people/david_dir/GEMS/GEMS.html
}
*/
