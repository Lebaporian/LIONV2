//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//==============================================================================

template< u32 T_CRC, int T_LENGTH > x_inline
s32 xcmdline::AddCmdSwitch(
    xstring_crcstr<xchar, T_CRC, T_LENGTH>      Name,
    s32                                         MinArgCount,
    s32                                         MaxArgCount,
    s32                                         nMinTimes,
    s32                                         nMaxTimes,
    bool                                        MustFallowOrder,
    type                                        Type,
    bool                                        bMainSwitch,
    s32                                         iParentID ) noexcept
{
    cmd_def& CmdDef         = m_CmdDef.append();

    CmdDef.m_Name           = Name;
    CmdDef.m_crcName        = T_CRC;
    CmdDef.m_MinArgCount    = MinArgCount;      
    CmdDef.m_MaxArgCount    = MaxArgCount;      
    CmdDef.m_nMaxTimes      = nMaxTimes;           
    CmdDef.m_nMinTimes      = nMinTimes;           
    CmdDef.m_RefCount       = 0;         
    CmdDef.m_bFallowOrder   = MustFallowOrder;     
    CmdDef.m_Type           = Type;             
    CmdDef.m_bMainSwitch    = bMainSwitch; 
    CmdDef.m_iParentID      = iParentID;
  
    return m_CmdDef.getCount<int>()-1;
}

//==============================================================================
x_inline
s32 xcmdline::getCommandCount( void ) const noexcept
{
    return m_Command.getCount<int>();
}

//==============================================================================
x_inline
s32 xcmdline::getArgumentCount( void ) const noexcept
{
    return m_Arguments.getCount<int>();
}

//==============================================================================
x_inline
u32 xcmdline::getCmdCRC( s32 Index ) const noexcept
{
    return m_CmdDef[ m_Command[Index].m_iCmdDef ].m_crcName;
}

//==============================================================================
x_inline
s32 xcmdline::getCmdArgumentOffset( s32 Index ) const noexcept
{
    return m_Command[Index].m_iArg;
}

//==============================================================================
x_inline
s32 xcmdline::getCmdArgumentCount ( s32 Index ) const noexcept
{
    return m_Command[Index].m_ArgCount;
}

//==============================================================================
x_inline
const xstring& xcmdline::getCmdName( s32 Index ) const noexcept
{
    return m_CmdDef[ m_Command[Index].m_iCmdDef ].m_Name;
}

//==============================================================================
x_inline
const xstring& xcmdline::getArgument( s32 Index ) const noexcept
{
    return m_Arguments[Index];
}

//==============================================================================
x_inline
bool xcmdline::DoesUserNeedsHelp( void ) const noexcept
{
    return m_bNeedHelp;
}


