//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

//------------------------------------------------------------------------------
template< class T > x_inline
s32 xbitstream::SerializeOutRLE( const xbuffer_view<T> Data ) noexcept
{
    const int nSamples = Data.template getCount<int>();
    s32     nBitsPerSample;
    s32     nBitsPerCount;
    s32     MaxRunLength;
    s32     TotalBitsWritten = 0;
    T       MaxSample = Data[0];
    T       MinSample = Data[0];

    //
    // Compute number of bits per sample
    //
    {
        for( s32 i=0; i<nSamples; i++ )
        {
            MaxSample = x_Max(MaxSample,s32(Data[i]));
            MinSample = x_Min(MinSample,s32(Data[i]));
        }
        nBitsPerSample = x_Log2IntRoundUp( MaxSample - MinSample );
    }
    x_assert( nBitsPerSample <= sizeof(T)*8 );

    //
    // Compute number of bits per count and max run length
    //
    {
        s32 BestCountBits  = 0;
        s32 BestBitsNeeded = X_S32_MAX;
        for( s32 i=3; i<8; i++ )
        {
            s32 MaxRunLength = (1<<i)-1;
            s32 nRuns        = 0;
            s32 RunStartI    = 0;
            s32 I            = 0;
            while( I < nSamples )
            {
                // Start a new run if we need to
                if( (I==0) || 
                    (Data[I] != Data[RunStartI]) ||
                    ((I-RunStartI+1) > MaxRunLength) )
                {
                    nRuns++;
                    RunStartI = I;
                }

                I++;
            }

            // Compute number of bits needed. Remember to add on bits for
            // terminator.
            s32 nBitsNeeded = ((nRuns+1) * i) + (nBitsPerSample*nSamples);
            if( nBitsNeeded < BestBitsNeeded )
            {
                BestBitsNeeded = nBitsNeeded;
                BestCountBits  = i;
            }
        }

        MaxRunLength = (1<<BestCountBits)-1;
        nBitsPerCount = BestCountBits;
    }

    //
    // Pack bitcounts into bitstream
    //
    SerializeOut( nBitsPerCount,  3, 8  );   
    SerializeOut( u32(MinSample), sizeof(T)*8 );
    SerializeOut( u32(MaxSample), sizeof(T)*8 );
    
    TotalBitsWritten += x_Log2IntRoundUp( 8-3 );
    TotalBitsWritten += sizeof(T)*8*2;

    //
    // Pack samples into bitstream
    //
    {
        s32 I = 0;
        //s32 TotalBitsWritten = BS.GetCursor();

        while( I < nSamples )
        {
            // Start a new run
            s32 RunStartI = I;
            s32 RunLength = 0;

            // Determine run length
            while( 1 )
            {
                if( I == nSamples ) break;
                if( Data[I] != Data[RunStartI] ) break;
                if( (I-RunStartI+1) > MaxRunLength) break;
                I++;
                RunLength++;
            }
            x_assert( RunLength <= MaxRunLength );
            
            // Pack Count
            SerializeOut( RunLength, nBitsPerCount );
            SerializeOut( Data[RunStartI], MinSample, MaxSample );
            TotalBitsWritten += ( nBitsPerCount + nBitsPerSample);
        }

        // Add terminator
        SerializeOut( 0, nBitsPerCount );
        TotalBitsWritten += nBitsPerCount;
    }

    return TotalBitsWritten;
}

//------------------------------------------------------------------------------
template< class T > x_inline
void xbitstream::SerializeInRLE( xbuffer_view<T> Data ) noexcept
{
    u32 nBitsPerCount;
    T   Min, Max;
    u32 C;
    T V;

    //
    // Unpack bitcounts from bitstream
    //
    SerializeIn( nBitsPerCount,  3, 8  );   
    SerializeIn( Min, sizeof(T)*8 );
    SerializeIn( Max, sizeof(T)*8 );
    
    //
    // Loop until all samples are decompressed
    //
    s32 nSamples = 0;
    s32 iCursor  = 0;
    while( 1 )
    {
        // Read Count
        SerializeIn( C, nBitsPerCount );

        // Increment total samples
        nSamples += C;

        // If we hit terminator break out
        if( C==0 ) break;

        // Read Value and duplicate
        SerializeIn( V, Min, Max );
        while( C-- ) Data[iCursor++] = V;
    }

    x_assert( nSamples == iCursor );
}
