//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

// Description:
//     This class serves as a container for a texture and a bitmap. The class can
//     handle cubemaps as well as multiple frame animations. Note that there is not
//     information about the playback of the animation. The class may also contain
//     mip levels for any texture.
//
//<CODE>
//
// Note that the Offsets is from after the offset table (begging of the actual data)
//
//                                     Memory Layout
// m_pMip ---------------------------> +-------------------+
//                                     | mip offset array  |
//          mip offset array           +-------------------+ <-- offset base
// m_pMip -> +------------+            |frame 0 (bitmap 1) |  ^
//           | s32 Offset |  ------->  | Mip0 Data         |  |
//           +------------+            |                   |  |
//           | s32 Offset |  ------->  | Mip1 Data         |  |
//           +------------+            |                   |  | FrameSize
//           | s32 Offset |  ------->  | Mip2 Data         |  |
//           +------------+            |                   |  |
//           | s32 Offset |  ------->  | Mip3 Data         |  |
//           +------------+            |                   |  |
//           | s32 Offset |  ------->  | Mip4 Data         |  |
//           +------------+            |                   |  v
//                                     +-------------------+
//     *  Note that previous offsets   |frame 1            |
//        Still work in bitmap 2       | ..                |
//        as well (same size)          | ...               |
//                                     |                   |
//     *  If the bitmap will have a    +-------------------+
//        palette it will be part of   +-------------------+
//        the mip data as well,        +-------------------+
//        at the top.
//
// There are a few compressed texture types supported. Here is a quick over view of some of them:
//
//      * PVR1 also known as PVRTC - This is Imagination’s version one of its widely used PowerVR texture compression.
//        It supports a 2/4bpp versions and RGBA/RGB as well. It is not block base rather it has 2 low rest texture
//        that are combine with a larger gray scale texture. More info:
//        http://blog.imgtec.com/powervr/pvrtc-the-most-efficient-texture-compression-standard-for-the-mobile-graphics-world
//
//      * PVR2 also know as PVRTC2 - This is Imagination's newly updated compression formats. When possible use this over version one.
//        http://blog.imgtec.com/powervr/pvrtc2-taking-texture-compression-to-a-new-dimension
//
//      * ETCn - ETC1 (Ericsson texture compression) and ETC2/EAC (backwards compatible with ETC1 and mandatory in the OpenGL ES 3.0 graphics standard)
//        http://en.wikipedia.org/wiki/Ericsson_Texture_Compression
//
//      * ASTC - (Adaptive scalable texture compression), an upcoming optional extension for both OpenGL and OpenGL ES
//        http://en.wikipedia.org/wiki/Adaptive_Scalable_Texture_Compression
//
//      * ATITC - (ATI texture compression)
//
//      * S3TC also know as DXTn - (S3 texture compression), also called DXTn, DXTC or BCn
//        http://en.wikipedia.org/wiki/S3_Texture_Compression
//
//      * Vulkan reference: https://www.khronos.org/registry/dataformat/specs/1.1/dataformat.1.1.pdf 
//                            https://www.khronos.org/registry/vulkan/specs/1.0/xhtml/vkspec.html
//      * NVidia Overview on texture formats: https://developer.nvidia.com/astc-texture-compression-for-game-assets
//
//</CODE>
//
// Todo:
//     The class in unfinish
//
// See Also:
//     xcolor
//==============================================================================

class xbitmap 
{
    x_object_type( xbitmap, is_linear, is_not_copyable, is_not_movable  );

public:

    // Bit wise formatting for the enumeration.
    // FORMAT_(LOW BITS elements first then moving to HIGH BITS)
    // byte order       0    1    2    3    
    // bit order:    0    8    16   24   32
    //               | R8 | G8 | B8 | A8 |
    enum format : u8
    {
        FORMAT_NULL,

        //
        // Uncompress formats
        //
        FORMAT_R4G4B4A4     = xcolor::FMT_16_RGBA_4444,
        FORMAT_R5G6B5       = xcolor::FMT_16_RGB_565,
        FORMAT_A1B5G5R5     = xcolor::FMT_16_ABGR_1555,
        FORMAT_R8G8B8       = xcolor::FMT_24_RGB_888,
        FORMAT_R8G8B8U8     = xcolor::FMT_32_RGBU_8888,
        FORMAT_R8G8B8A8     = xcolor::FMT_32_RGBA_8888,         // PRIMARY FORMAT (xcolor)
        FORMAT_XCOLOR       = xcolor::FMT_XCOLOR,
        FORMAT_A8R8G8B8     = xcolor::FMT_32_ARGB_8888,
        FORMAT_U8R8G8B8     = xcolor::FMT_32_URGB_8888,
        
        FORMAT_XCOLOR_END   = xcolor::FMT_END,                  // end of the range of xcolor

        //
        // Compression formats
        //
        FORMAT_PAL4_R8G8B8A8,                                   // 4 bpp Index + 16  RGBA8888 palette
        FORMAT_PAL8_R8G8B8A8,                                   // 8 bpp Index + 256 RGBA8888 palette
        
        // Ericsson Texture Compression (ETC)
        FORMAT_ETC2_4RGB,                                       
        FORMAT_ETC2_4RGBA1,                                   
        FORMAT_ETC2_8RGBA,                                     

        // S3TC Compressed Texture Image Formats 
        FORMAT_BC1_4RGB,                                        // DXT1_RGB 
        FORMAT_BC1_4RGBA1,                                      // DXT1_RGBA
        FORMAT_BC2_8RGBA,                                       // DXT3_RGBA
        FORMAT_BC3_8RGBA,                                       // DXT5_RGBA

        // RGTC Compressed Texture Image Formats
        FORMAT_BC4_4R,                                          // High quality R  (good for suplemental alpha)
        FORMAT_BC5_8RG,                                         // High Quality RG (good for normal maps)

        // BPTC Compressed Texture Image Formats 
        FORMAT_BC6H_8RGB_FLOAT,                                 // Floating point compression    (good for normal maps)
        FORMAT_BC7_8RGBA,                                       // High quality RGBA compression (good for normal maps) 

        // ASTC stands for Adaptive Scalable Texture Compression
        FORMAT_ASTC_4x4_8RGB,                                   // 8.00bpp
        FORMAT_ASTC_5x4_6RGB,                                   // 6.40bpp
        FORMAT_ASTC_5x5_5RGB,                                   // 5.12bpp (good for normal maps)
        FORMAT_ASTC_6x5_4RGB,                                   // 4.27bpp
        FORMAT_ASTC_6x6_4RGB,                                   // 3.56bpp
        FORMAT_ASTC_8x5_3RGB,                                   // 3.20bpp
        FORMAT_ASTC_8x6_3RGB,                                   // 2.67bpp
        FORMAT_ASTC_8x8_2RGB,                                   // 2.00bpp
        FORMAT_ASTC_10x5_3RGB,                                  // 2.56bpp
        FORMAT_ASTC_10x6_2RGB,                                  // 2.13bpp
        FORMAT_ASTC_10x8_2RGB,                                  // 1.60bpp
        FORMAT_ASTC_10x10_1RGB,                                 // 1.28bpp
        FORMAT_ASTC_12x10_1RGB,                                 // 1.07bpp
        FORMAT_ASTC_12x12_1RGB,                                 // 0.89bpp

        // PVR compression modes
        FORMAT_PVR1_2RGB,                                       
        FORMAT_PVR1_2RGBA,
        FORMAT_PVR1_4RGB,
        FORMAT_PVR1_4RGBA,
        FORMAT_PVR2_2RGBA,
        FORMAT_PVR2_4RGBA,

        //
        // Frame buffer Formats
        //
        FORMAT_D24S8_FLOAT,                                     // Floating point depth and 8bit stencil
        FORMAT_D24S8,                                           // Depth 24 bits and 8 bit Stencil    
        FORMAT_R8,
        FORMAT_R32,                                             
        FORMAT_R8G8,                                            
        FORMAT_R16G16B16A16,                                    
        FORMAT_R16G16B16A16_FLOAT,                                  
        FORMAT_A2R10G10B10,                                     
        FORMAT_B11G11R11_FLOAT,                                       

        //
        // End
        //
        FORMAT_TOTAL
    };

    enum color_space : bool
    {
        COLOR_SPACE_LINEAR  = false,
        COLOR_SPACE_SRGB    = true,
    };

public:

//                                            xbitmap             ( xserialfile& ){ } //static_assert( sizeof(*this) == 32 ); }
   constexpr                                xbitmap                 ( void )                                        noexcept = default;
                                           ~xbitmap                 ( void )                                        noexcept;
    x_forceinline                           xbitmap                 ( xbuffer_view<xbyte> Data, u32 Width, u32 Height, bool bReleaseWhenDone ) noexcept;

                bool                        Load                    ( const char* FileName )                        noexcept;
                bool                        Save                    ( const char* FileName )                const   noexcept;
                void                        Kill                    ( void )                                        noexcept;
//            bool                            SaveTGA             ( const xstring FileName ) const;
//            void                            SerializeIO         ( xserialfile& SerialFile ) const;
    
    inline      void                        setOwnMemory            ( bool bOwnMemory )                             noexcept { m_Flags.m_OWNS_MEMORY    = bOwnMemory; }
    inline      void                        setHasAlphaInfo         ( bool bAlphaInfo )                             noexcept { m_Flags.m_HAS_ALPHA_INFO = bAlphaInfo; }
                void                        ComputeHasAlphaInfo     ( void )                                        noexcept;
                void                        ComputePremultiplyAlpha ( void )                                        noexcept;

    inline      bool                        hasAlphaInfo            ( void )                                const   noexcept;
                bool                        hasAlphaChannel         ( void )                                const   noexcept;
    
    constexpr   bool                        isValid                 ( void )                                const   noexcept { return !!m_pData;                     }
    constexpr   bool                        isSquare                ( void )                                const   noexcept;
    constexpr   bool                        isPowerOfTwo            ( void )                                const   noexcept;
    constexpr   bool                        isLinear                ( void )                                const   noexcept { return m_Flags.m_LINEAR_SPACE;        }
    constexpr   bool                        isSigned                ( void )                                const   noexcept { return m_Flags.m_SIGNED_PIXELS;       }
    constexpr   u32                         getWidth                ( void )                                const   noexcept { return m_Width;                       }
    constexpr   u32                         getHeight               ( void )                                const   noexcept { return m_Height;                      }
    constexpr   format                      getFormat               ( void )                                const   noexcept { return m_Flags.m_FORMAT;              }
    inline      void                        setFormat               ( const format Format )                         noexcept { m_Flags.m_FORMAT = Format;            }
    inline      void                        setColorSpace           ( const color_space ColorSpace )                noexcept { m_Flags.m_LINEAR_SPACE = ColorSpace;  }
    constexpr   xuptr                       getFrameSize            ( void )                                const   noexcept { return m_FrameSize;                   }
    inline      void                        Copy                    ( const xbitmap& Src )                          noexcept { CreateFromMips( xbuffer_view<xbitmap>{const_cast<xbitmap*>(&Src), 1} );  }

    constexpr   xuptr                       getDataSize             ( void )                                const   noexcept { return m_DataSize;                    }
    constexpr   int                         getMipCount             ( void )                                const   noexcept { return m_nMips;                       }
    x_incppfile void                        FlipImageInY            ( void )                                        noexcept;
    template< typename T >
    inline      xbuffer_view<T>             getMip                  ( const int Mip, const int Frame = 0 )          noexcept;
    template< typename T >
    inline      const xbuffer_view<T>       getMip                  ( const int Mip, const int Frame = 0 )  const   noexcept;
    inline      xuptr                       getMipSize              ( int Mip )                             const   noexcept;
    inline      int                         getFullMipChainCount    ( void )                                const   noexcept;
    
                void                        CreateResizedBitmap     (   xbitmap&    Dest        , 
                                                                        const int   FinalWidth  , 
                                                                        const int   FinalHeight )           const   noexcept;
    
                void                        setDefaultTexture       ( void )                                        noexcept;
    static      const xbitmap&              getDefaultBitmap        ( void )                                        noexcept;
    
                void                        CreateBitmap            ( const u32 Width, const u32 Height )           noexcept;
    
                void                        CreateFromMips          ( const xbuffer_view<xbitmap> MipList )         noexcept;
    
                void                        setupFromColor          (   const u32                   Width     ,
                                                                        const u32                   Height    ,
                                                                              xbuffer_view<xcolor>  Data      ,
                                                                        const bool                  bFreeMemoryOnDestruction = true ) noexcept;
    
                void                        setup                   (   const u32                   Width                         ,
                                                                        const u32                   Height                        ,
                                                                        const xbitmap::format       BitmapFormat                  ,
                                                                        const xuptr                 FrameSize                     ,
                                                                              xbuffer_view<xbyte>   Data                          ,
                                                                        const bool                  bFreeMemoryOnDestruction      ,
                                                                        const int                   nMips                         ,
                                                                        const int                   nFrames                       ) noexcept;

    

/*
    void                    ConvertBitmap       ( s32 Bpp, xcolor::format Format );
    void                    ConvertBitmap       ( xbitmap& Bitmap, s32 Bpp, xcolor::format Format ) const;    

    u32                     GetPixel            ( s32 X, s32 Y, s32 Mip = 0 ) const;
    void                    SetPixel            ( s32 X, s32 Y, u32 Pixel, s32 Mip = 0 );
    xcolor                  GetPixelColor       ( s32 X, s32 Y, s32 Mip = 0 ) const;
    xcolor                  GetBilinearColor    ( f32 ParamU, f32 ParamV, bool Clamp=FALSE, s32 Mip = 0 ) const;
    void                    SetPixelColor       ( xcolor Color, s32 X, s32 Y, s32 Mip = 0 );
*/
    
protected:

    X_DEFBITS( bit_pack_fields,
               u16,
               0x00000000,
               X_DEFBITS_ARG ( bool,        CUBEMAP,               1 ),        // Tells if this bitmap is a cubemap 
               X_DEFBITS_ARG ( bool,        OWNS_MEMORY,           1 ),        // if the xbitmap is allowed to free the memory
               X_DEFBITS_ARG ( bool,        HAS_ALPHA_INFO,        1 ),        // it tells that the picture has alpha information
               X_DEFBITS_ARG ( bool,        ALPHA_PREMULTIPLIED,   1 ),        // Tells it the alpha has already been premultiplied
               X_DEFBITS_ARG ( color_space, LINEAR_SPACE,          1 ),        // What is the color space for the bitmap
               X_DEFBITS_ARG ( bool,        CAN_WRAP_U,            1 ),        // Tells if it can wrap around U
               X_DEFBITS_ARG ( bool,        CAN_WRAP_V,            1 ),        // Tells if it can wrap around V
               X_DEFBITS_ARG ( bool,        CAN_MIRROR_WRAP_U,     1 ),        // Tells if it can mirror wrap around U
               X_DEFBITS_ARG ( bool,        CAN_MIRROR_WRAP_V,     1 ),        // Tells if it can mirror wrap around V
               X_DEFBITS_ARG ( bool,        SIGNED_PIXELS,         1 ),        // Tells if the pixels in the bitmap are suppose to be signed bits
               X_DEFBITS_ARG( format,       FORMAT,                x_Log2IntRoundUp((int)FORMAT_TOTAL   - 1)  )
    );

    struct mip
    {
        s32                     m_Offset;                           // Offset in xbitmap::m_pData for a mip's data
    };


    inline      const void*         getMipPtr( const int Mip, const int Frame = 0 ) const noexcept;
    inline      void*               getMipPtr( const int Mip, const int Frame = 0 )       noexcept;

public:

    union
    {
        mip*                    m_pData         { nullptr };
        xdataptr<xbyte>         m_RawData;                          // +8 pointer to the data
    };
    u64                         m_DataSize      { 0 };              // +8 total data size in bytes
    u32                         m_FrameSize     { 0 };              // +4 Size of one frame of data.(one bitmap)
    u32                         m_Height        { 0 };              // +4 height in pixels
    u32                         m_Width         { 0 };              // +4 width in pixels
    bit_pack_fields             m_Flags         {};                 // +2 all flags including the format of the bitmap
    u8                          m_nMips         { 0 };              // +1 Number of mips
    u8                          m_nFrames       { 0 };              // +1 Number of bitmaps in here (example cube maps == 6)
                                                                    // 32 bytes total
};
static_assert( sizeof(xbitmap) == 32, "The bitmap structure should always be 32bytes long" );

