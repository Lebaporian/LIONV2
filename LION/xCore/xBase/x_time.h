//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//------------------------------------------------------------------------------
// Description:
//      timer class
//------------------------------------------------------------------------------
class xtimer : public std::chrono::high_resolution_clock
{
    x_object_type( xtimer, is_linear )

public:
    
    using nanoseconds   = std::chrono::nanoseconds;
    using milliseconds  = std::chrono::milliseconds;
    using seconds       = std::chrono::seconds;
    using ticks         = rep;

public:

    constexpr                                   xtimer              ( void )                            noexcept {}
    x_inline                    void            Start               ( void )                            noexcept { m_Start = now();                                                                            }
    x_inline                    void            End                 ( void )                            noexcept { m_End   = now();                                                                            }
    x_inline        static      double          ToMilliseconds      ( const nanoseconds&  ns  )         noexcept { return ns.count() * (1.0 / 1000000.0);                                                      }
    x_inline        static      double          ToMilliseconds      ( const seconds&      s   )         noexcept { return s.count()  * (1000.0);                                                               }
    x_inline        static      double          ToSeconds           ( const milliseconds& ms  )         noexcept { return ms.count() * (1.0 / 1000.0);                                                         }
    x_inline        static      double          ToSeconds           ( const nanoseconds&  ns  )         noexcept { return ns.count() * (1.0 / 1000000000.0);                                                   }
    x_inline                    void            Trip                ( void )                            noexcept { End(); Start();                                                                             }
    x_inline                    nanoseconds     TripNanoseconds     ( void )                            noexcept { End(); const nanoseconds  t = getNanoseconds();  Start(); return t;                         }
    x_inline                    milliseconds    TripMilliseconds    ( void )                            noexcept { End(); const milliseconds t = getMilliseconds(); Start(); return t;                         }
    x_inline                    seconds         TripSeconds         ( void )                            noexcept { End(); const seconds      t = getSeconds();      Start(); return t;                         }
    x_inline                    nanoseconds     getNanoseconds      ( void )                    const   noexcept { const nanoseconds  ns = std::chrono::duration_cast<nanoseconds>(m_End-m_Start);  return ns; }
    x_inline                    milliseconds    getMilliseconds     ( void )                    const   noexcept { const milliseconds ms = std::chrono::duration_cast<milliseconds>(m_End-m_Start); return ms; }
    x_inline                    seconds         getSeconds          ( void )                    const   noexcept { const seconds      s  = std::chrono::duration_cast<seconds>(m_End-m_Start);      return s;  }
    x_forceinline   static      double          getNowMs            ( void )                            noexcept { return ToMilliseconds( std::chrono::duration_cast<xtimer::nanoseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()) ); }
    x_forceinline   static      double          getNowS             ( void )                            noexcept { return ToSeconds( std::chrono::duration_cast<xtimer::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()) ); }
    
protected:
    
    time_point        m_Start       {};
    time_point        m_End         {};
};

//------------------------------------------------------------------------------
// Description:
//      timer class
//------------------------------------------------------------------------------
