//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

/////////////////////////////////////////////////////////////////////////////////
//
// MAKE SURE ANY DATA YOU PUT IN PACKETS USES x_NetworkEndian() SINCE DIFFERENT
// ENDIAN MACHINES COULD RECIVE THE DATA.
//
//  Packet
//               +----------------+
//  Base Header  | u2  protocol   | [00]-base | [01]-unreliable | [10]-reliable | [11]-inorder
//  Unreliable   | u30 crcseq     | crcseq = crc^sequence || sequence = crcseq^crc
//               +----------------+
//  Reliable     | u32 ack_bits   | bits used to acknowledge the packets that we have received
//               +----------------+
//  InOrder      | u16 order_seq  | Real sequence which the order is based on
//               +----------------+
//               |                |
//               | u8 user_data[] | The actual user data
//               |                |
//               +----------------+
//
// TODO: 1. Everything in the header could be converted to 16 bits or less except for the ack_bits.
//       2. [00]-base could also be replace with the meaning of mix packet where
//          a packet could contain a mix of the 3 where each section will have a length u16 additional field
//       4. The CRC should be change to a security hash since UDP/IP already supports the CRC
//       5. RemoteSequences... needs a bit more thinking to see if we really need them...
//       6. Handling of client disconnects is not done 
//       7. We need to add a heard-bit for the reliable protocol... since it continuously needs to
//          ack packets. (heard-bit could be identify by having the user data size == 0 )
//       7. Packet encryption could be added
//       8. Always more testing will be great.
/////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////

class xnet_udp_mesh_base
{
    x_object_type( xnet_udp_mesh_base, is_linear, is_not_movable, is_not_copyable )
    xnet_udp_mesh_base() = default;
public:

    using t_socket_container    = xphvector<xnet_socket,16>;
    using t_hsocket             = t_socket_container::t_handle; 
};

class xnet_udp_mesh;

/////////////////////////////////////////////////////////////////////////////////

class xnet_base_protocol
{
    x_object_type( xnet_base_protocol, is_linear, is_not_movable, is_not_copyable )

public:

    using t_handle  = xhandle<xnet_base_protocol>;
    using t_hsocket = xnet_udp_mesh_base::t_socket_container::t_handle; 

    enum errors : u8
    {
        ERR_OK,
        ERR_FAILURE,
        ERR_SOCKET,
        ERR_TIMEOUT,
        ERR_FATAL,
        ERR_ALERT_LOW,
        ERR_ALERT_MID,
        ERR_ALERT_HIGH
    };

    using err = x_err<errors>;

    // All the supported protocols
    enum class protocol : u8
    {                                   // FEATURES:
        BASE,                           //      Filters base on distance between the local sequence and the packet sequence, as well as incorrect CRC.
        NONRELIABLE,                    //      BASE        + filters older packets base on the sequence 
        RELIABLE,                       //      NONRELIABLE + guarantees packet arrival 
        INORDER,                        //      RELIABLE    + guarantees the exact same order that they were sent 
        ENUM_COUNT
    };

    x_units( int, local_sequence );
    x_units( int, remote_sequence );

    x_constexprvar int MAX_PACKET_SIZE              = 1024;
    x_constexprvar int MAX_NONRELIABLE_PACKET_SIZE  = MAX_PACKET_SIZE             - ( 4 );
    x_constexprvar int MAX_RELIABLE_PACKET_SIZE     = MAX_NONRELIABLE_PACKET_SIZE - ( 4 );
    x_constexprvar int MAX_INORDER_PACKET_SIZE      = MAX_RELIABLE_PACKET_SIZE    - ( 2 );

protected:

    x_forceinline                                   xnet_base_protocol      ( void )                                                                            noexcept {}
    virtual                                        ~xnet_base_protocol      ( void )                                                                            noexcept = default;
    virtual                     err                 HandleRecivePacket      ( t_hsocket hSocket, xbuffer_view<xbyte> Buffer, s32& Size )                        noexcept = 0;
    x_incppfile                 void                setupConnection         ( xnet_address RemoteAddress, xnet_udp_mesh& Mesh )                                 noexcept;
    x_incppfile     static      u32                 setPacketHeader         ( local_sequence& LocalSequence, xbuffer_view<xbyte> Buffer, protocol Protocol )    noexcept;
    x_incppfile                 err                 getInfoPacketHeader     ( local_sequence LocalSequence, xbuffer_view<xbyte> Buffer, remote_sequence& PacketSequence ) noexcept;  
    x_incppfile     static      protocol            getProtocol             ( const void* pData )                                                               noexcept;
    x_incppfile                 xnet_socket::err    WriteToSocket           ( t_hsocket hLocalSocket, const xbuffer_view<xbyte>& Buffer )                       noexcept;
    x_incppfile                 void                AddClientInPendingList  ( void )                                                                            noexcept;
    virtual                     bool                getPendingPacketData    ( xbuffer_view<xbyte> Packet, s32& Size  )                                          noexcept = 0;
    virtual                     void                vResetClient            ( void )                                                                            noexcept;

protected:

    xnet_address            m_ConnectionAddress     {};
    xnet_udp_mesh*          m_pMesh                 { nullptr };
    xnet_base_protocol*     m_pPendingNext          { nullptr };         // Link list of clients which has packets pending

    xtimer                  m_TimeSinceLastRecived  {};                  // Timer for heart-beats and such
    u32                     m_BytesSent             {0};
    u32                     m_PacketsSent           {0};
    u32                     m_BytesReceived         {0};
    u32                     m_PackagesReceived      {0};

    friend class xnet_udp_mesh;
};

/////////////////////////////////////////////////////////////////////////////////

class xnet_nonreliable_protocol : public xnet_base_protocol
{
public:

    x_incppfile             xnet_socket::err    SendNonreliablePacket       ( t_hsocket hLocalSocket, const xbuffer_view<xbyte> Buffer )                        noexcept;
    x_incppfile     static  xnet_socket::err    SendBroadcast               ( s32 DestinationPort, xnet_socket& LocalSocket, xbuffer_view<xbyte> Buffer )       noexcept;

protected:

    x_incppfile             err                 onRecivePacket              ( xbuffer_view<xbyte> Packet, s32& Size, remote_sequence Sequence )                 noexcept;
    virtual                 void                vResetClient                ( void )                                                                            noexcept override;

protected:

    local_sequence      m_NonRealibleLocalSequence  { 0 };
    remote_sequence     m_NonRealibleRemoteSequence { 0 };

    friend class xnet_udp_mesh;
};

/////////////////////////////////////////////////////////////////////////////////

class xnet_reliable_protocol : public xnet_nonreliable_protocol
{
public:

    x_incppfile     xnet_socket::err     SendReliablePacket      ( t_hsocket hSocket, const xbuffer_view<xbyte> Buffer )                                    noexcept;

protected:

    struct pending_packet
    {
        protocol                        m_Protocol  {};
        local_sequence                  m_Sequence  {};
        s32                             m_Size      {};
        pending_packet*                 m_pNext     {};
        pending_packet*                 m_pPrev     {};
        xarray<xbyte,MAX_PACKET_SIZE>   m_Data      {};
    };

protected:

    x_incppfile                         xnet_reliable_protocol  ( void )                                                                                    noexcept;
    x_incppfile     err                 onRecivePacket          ( t_hsocket hSocket, xbuffer_view<xbyte> Packet, s32& Size, remote_sequence Sequence )      noexcept;
    x_incppfile     void                HandleAcknowledge       ( t_hsocket hSocket, remote_sequence RemoteLocalSequence, u32 AckBits )                     noexcept;
    x_incppfile     xnet_socket::err    SendReliablePacket      ( t_hsocket hSocket, const xbuffer_view<xbyte> Packet, protocol Protocol )                  noexcept;
    virtual         void                vResetClient            ( void )                                                                                    noexcept override;
    x_incppfile     err                 Update                  ( t_hsocket hLocalSocket )                                                                  noexcept;

protected:

    s32                                 m_TotalInQueue          {0};
    u32                                 m_RecivedPacketsMask    {0};
    xarray<pending_packet*,128>         m_PendingQueue          {{0}};      // Waiting to be confirm
    pending_packet*                     m_pReliablePoolFreeList {nullptr}; // Free packet list
    xndptr<pending_packet>              m_PendingPacketPool     {};         // The actual allocation of packets
    local_sequence                      m_RealibleSequence      {0};
    s32                                 m_nResentPackets        {0};
    bool                                m_bForcePing            {false};
    xtimer                              m_Timer                 {};         // Timer for heart-beats and such

    friend class xnet_udp_mesh;
};

/////////////////////////////////////////////////////////////////////////////////

class xnet_inorder_protocol : public xnet_reliable_protocol
{
public:

    x_incppfile     xnet_socket::err    SendInOrderPacket       ( t_hsocket hSocket, xbuffer_view<xbyte> Buffer )                                           noexcept;

protected:

    x_incppfile     err                 onRecivePacket          ( t_hsocket hSocket, xbuffer_view<xbyte> Packet, s32& Size, remote_sequence Sequence )      noexcept;
    virtual         bool                getPendingPacketData    ( xbuffer_view<xbyte> Packet, s32& Size )                                                   noexcept override;
    virtual         void                vResetClient            ( void )                                                                                    noexcept override;

protected:

    pending_packet*     m_pPendingInOrder           { nullptr };        // Queue of pending packets 
    pending_packet*     m_pPendingInOrderLast       { nullptr };        // Points to the last node in the queue
    local_sequence      m_InOrderLocalSequence      { 0 };
    remote_sequence     m_InOrderRemoteSequence     { 0 };

    friend class xnet_udp_mesh;
};

/////////////////////////////////////////////////////////////////////////////////

class xnet_udp_client final : public xnet_inorder_protocol
{
public:
        
        enum type
        {
            TYPE_UNKOWN,
            TYPE_CERTIFIED,
            TYPE_LOCAL
        };

public:

    x_inline        const xnet_address&     getAddress              ( void )                                                    const   noexcept { return m_ConnectionAddress; } 
    x_inline        type                    getClientType           ( void )                                                    const   noexcept { return m_ClientType; }
    x_incppfile     void                    CertifyClient           ( void )                                                            noexcept;
    x_inline        void                    setLocalClient          ( void )                                                            noexcept { m_ClientType = TYPE_LOCAL; }

protected:

    x_inline        void                    setupClient             ( type Type )                                                       noexcept { m_ClientType = Type; }
    virtual         err                     HandleRecivePacket      ( t_hsocket hSocket, xbuffer_view<xbyte> Packet, s32& Size )        noexcept override;
    virtual         void                    vResetClient            ( void )                                                            noexcept override;
 
protected:

    type    m_ClientType{ TYPE_UNKOWN };

    friend class xnet_udp_mesh;
};

/////////////////////////////////////////////////////////////////////////////////

class xnet_udp_mesh : public xnet_udp_mesh_base
{
public:

    using t_client_container    = xphvector<xnet_udp_client,128>;
    using t_hclient             = t_client_container::t_handle; 
    using err                   = xnet_udp_client::err;
    using errors                = xnet_udp_client::errors;
     
public:

    x_incppfile     t_hsocket           openSocket              ( xnet_address::port Port )                                                     noexcept;
    x_incppfile     err                 getSocketPackets        (   t_hsocket           hLocalSocket,
                                                                    xbuffer_view<xbyte> Packet, 
                                                                    s32&                outSize, 
                                                                    t_hclient&          hClient, 
                                                                    bool                bBlock = false )                                        noexcept;
    x_forceinline   xnet_socket&        getSocket               ( t_hsocket hSocket )                                                           noexcept { return m_SocketList(hSocket); }
    x_incppfile     t_hclient           CreateClient            ( xnet_address Address )                                                        noexcept;
    x_incppfile     void                DeleteClient            ( t_hclient hClient )                                                           noexcept;
    x_forceconst    int                 getClientCount          ( void )                                                                const   noexcept { return m_lClient.getCount<int>(); }
    x_forceinline   xnet_udp_client&    getClient               ( s32 Index )                                                                   noexcept { return m_lClient[Index]; }
    x_forceinline   xnet_udp_client&    getClient               ( t_hclient hClient )                                                           noexcept { return m_lClient(hClient); }
    x_incppfile     t_hclient           findClient              ( const xnet_address& FromAddr )                                                noexcept;

protected:

    x_incppfile     t_hsocket           FindOrCreateSocket      ( xnet_address::port Port )                                                     noexcept;
    x_incppfile     xnet_socket::err    WriteToSocket           ( t_hsocket hLocalSocket, xbuffer_view<xbyte> Buffer, xnet_address Address )    noexcept;

protected:

    t_client_container              m_lClient                   {};
    t_socket_container              m_SocketList                {};
    xnet_base_protocol*             m_pPendingPacketClientList  { nullptr };

protected:

    friend class xnet_base_protocol; 
};
