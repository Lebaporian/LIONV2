//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once
//------------------------------------------------------------------------------
// Description:
//     This class is meant to be use as a way to compress a serialization of data.
//     It is most commonly use for saving games or sending packets over the network
//     It uses the an arithmetic range coder to compress the bits together.
//     There is not symbol table use for compression so it is not the most efficient 
//     it could be but it always perform as expected 
//	
//<P>  There are two ways to pack floats each way has its plus and minuses.
//     Try each and see which one you like better. 
//
//<P>  When setting the min/max the range is this [Min,Max]
//
// Example:
//<CODE> 
// void main( void )
// {
//      u32         Full32bits      = 0xf2309abc;
//      u32         Full32bitsTest  = 0;
//      xbitstream  BitStream;
//      
//      // Pack something
//      BitStream.setupPacking( 1024 );
//      BitStream.Serialize( Full32bits, 32 );
//      auto PackedData = BitStream.getResultOwner( Length );
//      
//      // Unpack it
//      BitStream.setupUnpacking( PackedDataView.ViewTo( Length ) );
//      BitStream.Serialize( Full32bitsTest, 32 );
//      
//      x_assert( Full32bitsTest ==  Full32bits );
// }
//</CODE>                         
// TODO:
//       * Allow user to write raw data
//       * Add 64 bits support
//------------------------------------------------------------------------------
class xbitstream
{
    x_object_type( xbitstream, is_linear, is_not_copyable, is_not_movable )

public:

    constexpr                               xbitstream              ( void )                                        noexcept {}
    x_forceinline                          ~xbitstream              ( void )                                        noexcept { if( m_Mode == mode::UNPACKING_AS_FAKEOWNER ) (void)m_WriteBuffer.TransferOwnership(); }
    x_incppfile         void                setupPacking            ( xuptr InitialBufferSize )                     noexcept;
    x_incppfile         void                setupUnpackingView      ( const xbuffer_view<xbyte>   xBuffer )         noexcept;
    x_incppfile         void                setupUnpackingOwner     ( xafptr<xbyte>& xBuffer, int Length )          noexcept;

                        void                Serialize               ( char* pString, s32 MaxChars )                 noexcept;

    x_forceinline       void                Serialize               ( f32& Float, f32 Min, f32 Max, s32 nBits=15 )  noexcept { if( m_Mode == mode::PACKING ) PackFloat( Float, Min, Max, nBits );    else Float = UnpackFloat( Min, Max, nBits );    }
    x_forceinline       void                Serialize               ( f32& Value )                                  noexcept { if( m_Mode == mode::PACKING ) PackFlags( *(u32*)&Value, 32 );         else *((u32*)&Value) = (u32)UnpackFlags32( 32 ); }

    x_forceinline       void                Serialize64             ( u64& Value, u64 Min, u64 Max )                noexcept { if( m_Mode == mode::PACKING ) Pack64( Value, Min, Max );              else Value = Unpack64( Min, Max );     }
    x_forceinline       void                Serialize64             ( s64& Value, s64 Min, s64 Max )                noexcept { if( m_Mode == mode::PACKING ) Pack64( Value, Min, Max );              else Value = (s64)Unpack64( Min, Max );     }
    x_forceinline       void                Serialize64             ( u64& Value, s32 MaxBits )                     noexcept { if( m_Mode == mode::PACKING ) PackFlags( Value, MaxBits );            else Value = UnpackFlags64( MaxBits ); }
    x_forceinline       void                Serialize64             ( s64& Value, s32 MaxBits )                     noexcept { if( m_Mode == mode::PACKING ) PackFlags( (u64)Value, MaxBits );       else Value = UnpackFlags64( MaxBits ); }

    template< class T >
    x_forceinline       void                Serialize               ( T& Value, s32 Min, s32 Max )                  noexcept { if( m_Mode == mode::PACKING ) Pack32( Value, Min, Max );              else Value = (T)Unpack32( Min, Max );     }

    template< class T >
    x_forceinline       void                Serialize               ( T& Value, s32 MaxBits = sizeof(T)*8 )         noexcept { if( m_Mode == mode::PACKING ) PackFlags( u32(Value), MaxBits );       else Value = (T)UnpackFlags32( MaxBits ); }


    template< class T >
    x_forceinline       void                SerializeIn             ( T& Value, s32 Min, s32 Max )                  noexcept { x_assert( m_Mode != mode::PACKING ); Value = (T)Unpack32( Min, Max ); }

    template< class T >
    x_forceinline       void                SerializeIn             ( T& Value, s32 MaxBits = sizeof(T)*8 )         noexcept { x_assert( m_Mode != mode::PACKING ); Value = (T)UnpackFlags32( MaxBits );  }

    x_forceinline       void                SerializeIn             ( f32& Value )                                  noexcept { x_assert( m_Mode != mode::PACKING ); *((u32*)&Value) = (u32)UnpackFlags32( 32 );  }

    template< class T >
    x_forceinline       void                SerializeOut            ( T Value, s32 Min, s32 Max )                   noexcept { x_assert( m_Mode == mode::PACKING ); Pack32( Value, Min, Max ); }

    template< class T >
    x_forceinline       void                SerializeOut            ( const T Value, s32 MaxBits = sizeof(T)*8 )    noexcept { x_assert( m_Mode == mode::PACKING ); PackFlags( u32(Value), MaxBits ); }

    x_forceinline       void                SerializeOut            ( f32 Value )                                   noexcept { x_assert( m_Mode == mode::PACKING ); PackFlags( *(u32*)&Value, 32 );  }

    template< class T >
    x_inline            s32                 SerializeOutRLE         ( const xbuffer_view<T> Buffer )                noexcept;

    template< class T >
    x_inline            void                SerializeInRLE          ( xbuffer_view<T> Buffer )                      noexcept;

    x_forceinline       xbuffer_view<xbyte> getResultView           ( void )                                        noexcept { PackFlush(); return m_WriteBuffer.ViewTo( m_Cursor ); }
    x_forceinline       xafptr<xbyte>       getResultOwner          ( s32& Length )                                 noexcept { PackFlush(); Length = m_Cursor; return m_WriteBuffer; }
    x_forceconst        s32                 getPackedLength         ( void )                                const   noexcept { return m_Cursor; }
    x_incppfile         void                PackFloat               ( f32 Value, f32 Min, f32 Max, s32 nBits=15 )   noexcept;
    x_incppfile         void                PackFloat               ( f32 Value, bool bRound, s32 nExponentBits, s32 nMantissaBits ) noexcept;
    x_incppfile         void                Pack32                  ( s32 Value, s32 Min, s32 Max )                 noexcept;
    x_incppfile         void                Pack64                  ( s64 Value, s64 Min, s64 Max )                 noexcept;
    x_incppfile         void                PackFlags               ( u32 Flags, s32 MaxBits )                      noexcept;
    x_incppfile         void                PackFlags               ( u64 Flags, s32 MaxBits )                      noexcept;
    x_incppfile         void                PackQuaternion          ( const xquaternion& Q )                        noexcept;
    x_incppfile         bool                HasAnythingBeenPacked   ( void )                                        noexcept;

    x_incppfile         f32                 UnpackFloat             ( f32 Min, f32 Max, s32 nBits )                 noexcept;
    x_incppfile         f32                 UnpackFloat             ( s32 nExponentBits, s32 nMantissaBits )        noexcept;
    x_incppfile         s32                 Unpack32                ( s32 Min, s32 Max )                            noexcept;
    x_incppfile         s64                 Unpack64                ( s64 Min, s64 Max )                            noexcept;
    x_incppfile         u32                 UnpackFlags32           ( s32 MaxBits )                                 noexcept;
    x_incppfile         u64                 UnpackFlags64           ( s32 MaxBits )                                 noexcept;
    x_incppfile         void                UnpackQuaternion        ( xquaternion& Q )                              noexcept;
    x_incppfile         bool                HasAnyUnpackDataLeft    ( void )                                        noexcept;

protected:

    enum class mode
    {
        PACKING,
        UNPACKING_AS_FAKEOWNER,
        UNPACKING_AS_OWNER,
    };

protected:

    x_incppfile         void                SysPack                 ( u64 Value, s64 Limit )                        noexcept;
    x_incppfile         void                PackOutputByte          ( u32 Value )                                   noexcept;
    x_incppfile         void                PackOutputBit           ( s32 Value )                                   noexcept;
    x_incppfile         void                PackFlush               ( void )                                        noexcept;
    x_incppfile         void                PackRenormalize         ( void )                                        noexcept;
    x_incppfile         void                PackCarry               ( void )                                        noexcept;

    x_incppfile         u64                 SysUnpack               ( u64 Limit)                                    noexcept;
    x_incppfile         u32                 UnpackNextByte          ( void )                                        noexcept;
    x_incppfile         void                UnpackFlush             ( void )                                        noexcept;
    x_incppfile         void                UnpackRenormalize       ( void )                                        noexcept;

    x_incppfile         void                StartAccess             ( void )                                        noexcept;
    x_incppfile         void                EndAccess               ( void )                                        noexcept;

protected:

    mode                    m_Mode          {};
    u64                     m_Low           {};
    u64                     m_Range         {};
    u64                     m_Code          {};
    s32                     m_Cursor        {};
    s32                     m_nBytes        {};

    union
    {
        xafptr<xbyte>       m_WriteBuffer   {};             // Used when PACKING 
        const xafptr<xbyte> m_ReadBuffer;                   // Used when UNPACKING, This can be a real owner when UNPACKING_AS_OWNER otherwise is not a real owner
    };
};

