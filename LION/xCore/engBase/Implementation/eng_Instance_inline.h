//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//-------------------------------------------------------------------------------------------
extern "C" xuptr eng_InternalCreateInstance( void* pInstance, const void* pSetup, void* pxGlobalContext );

//-------------------------------------------------------------------------------------------
inline
eng_instance::err eng_instance::CreateInstance( xndptr_s<eng_instance>& Instance, const eng_instance::setup& Setup ) noexcept
{
    auto    pInst       = reinterpret_cast<void*>(&Instance);
    auto    pSetup      = reinterpret_cast<const void*>(&Setup);
    auto    pGContext   = reinterpret_cast<void*>(&g_context::get());
    xuptr   UErr        = eng_InternalCreateInstance( pInst, pSetup, pGContext );
    return eng_instance::err{ reinterpret_cast<const char*>(UErr), eng_instance::ERR_FAILURE };
}


