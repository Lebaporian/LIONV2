//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

namespace debug
{
    void getValidationLayers( instance& Instance, xvector<const char*>& Layers );

    std::string VulkanErrorString( VkResult errorCode );

    // Default debug callback
    VkBool32 messageCallback(
        VkDebugReportFlagsEXT       flags,
        VkDebugReportObjectTypeEXT  objType,
        uint64_t                    srcObject,
        size_t                      location,
        int32_t                     msgCode,
        const char*                 pLayerPrefix,
        const char*                 pMsg,
        void*                       pUserData );

    // Load debug function pointers and set debug callback
    // if callBack is NULL, default message callback will be used
    bool setupDebugging(
        VkInstance                  instance, 
        VkDebugReportFlagsEXT       flags, 
        VkDebugReportCallbackEXT    callBack );

    // Clear debug callback
    void freeDebugCallback( VkInstance instance );
}
