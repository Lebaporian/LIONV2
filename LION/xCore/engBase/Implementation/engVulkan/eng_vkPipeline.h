//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

class rendering_pipeline : public x_ll_share_object 
{
    x_object_type( rendering_pipeline, is_linear, is_not_copyable, is_not_movable );

public:

    struct setup
    {
    public:

        enum default_pipelines
        {
            SOLID_PIPELINE
        };

    public:

                setup               (   VkRenderPass                                    RenderPass,
                                        xbuffer_view<VkPipelineShaderStageCreateInfo>   ShaderStages,
                                        VkPipelineLayout                                aVkPipelineLayout, 
                                        const VkPipelineVertexInputStateCreateInfo&     aVkPipeLineSetupInfo, 
                                        default_pipelines                               DefaultPipeLine = SOLID_PIPELINE ) noexcept;

                setup               (   VkRenderPass                                    RenderPass,  
                                        xbuffer_view<VkPipelineShaderStageCreateInfo>   ShaderStages,
                                        VkPipelineLayout                                aVkPipelineLayout, 
                                        const VkPipelineVertexInputStateCreateInfo&     aVkPipeLineSetupInfo, 
                                        eng_draw::pipeline                              DrawPipeLine,
                                        bool                                            bRenderPolys ) noexcept;

                setup               (   const VkRenderPass                                    RenderPass,
                                        const xbuffer_view<VkPipelineShaderStageCreateInfo>   ShaderStages,
                                        const VkPipelineLayout                                aVkPipelineLayout,
                                        const VkPipelineVertexInputStateCreateInfo&           aVkPipeLineSetupInfo,
                                        const VkPrimitiveTopology                             PrimitiveTopology,
                                        const engpipeline&                                    Pipeline ) noexcept;  



        void    setupSolidPipeline  (   VkRenderPass                                    RenderPass, 
                                        xbuffer_view<VkPipelineShaderStageCreateInfo>   ShaderStages,
                                        VkPipelineLayout                                aVkPipelineLayout, 
                                        const VkPipelineVertexInputStateCreateInfo&     VkPipeLineSetupInfo ) noexcept;

    public:

        VkRenderPass                                            m_VKRenderPass              {};
        VkGraphicsPipelineCreateInfo                            m_VkPipelineCreateInfo      {};
        VkPipelineInputAssemblyStateCreateInfo                  m_VkInputAssemblyState      {};
        VkPipelineRasterizationStateCreateInfo                  m_VkRasterizationState      {};
        VkPipelineColorBlendStateCreateInfo                     m_VkColorBlendState         {};
        xarray<VkPipelineColorBlendAttachmentState,1>           m_lVkBlendAttachmentState   {};
        VkPipelineViewportStateCreateInfo                       m_VkViewportState           {};
        VkPipelineDynamicStateCreateInfo                        m_VkDynamicState            {};
        xarray<VkDynamicState,2>                                m_lVkDynamicStateEnables    {};
        VkPipelineDepthStencilStateCreateInfo                   m_VkDepthStencilState       {};
        VkPipelineMultisampleStateCreateInfo                    m_VkMultisampleState        {};
        xarray<VkPipelineShaderStageCreateInfo,2>               m_lVkShaderStages           {};
    };

public:

    constexpr                       rendering_pipeline  ( void )                                                                    noexcept = default;
    constexpr       bool            isValid             ( void )                                                    const           noexcept { return !!m_VkPipeline; }
                    void            Initialize          ( 
                        device&                                         Device,
                        const setup&                                    Setup, 
                        const VkPipelineCache                           pipelineCache ) noexcept;
    constexpr       VkPipeline      getVkPipeline       ( void )                                                    const           noexcept { return m_VkPipeline; }

protected:

    VkPipeline                                      m_VkPipeline    { VK_NULL_HANDLE };
};

//-----------------------------------------------------------------------------------------

class rendering_pipeline_hash
{
    x_object_type( rendering_pipeline_hash, is_linear, is_not_copyable, is_not_movable );

public:

    rendering_pipeline& getDrawPipeLine( 
        VkRenderPass                                    VKRenderPass,
        const eng_draw::pipeline                        DrawPipeLine, 
        const bool                                      bRenderPolys )              noexcept;

    rendering_pipeline& getPipeLine( 
        VkRenderPass                                    VKRenderPass,
        material_informed&                              MaterialInformed,
        const engpipeline&                              EngPipeline,
        const u32                                       InformedMaterialMixHash  )  noexcept;
  
    rendering_pipeline_hash( device& Device ) : m_Device{ Device } {  m_HashTable.Initialize( 1024 ); }

protected:

    // We want our hash table to do nothing while we wait
    using hash_table = x_ll_mrmw_hash<rendering_pipeline, u32, x_lk_spinlock::do_nothing>;

protected:

    device&                                         m_Device;
    hash_table                                      m_HashTable             {};
    VkRenderPass                                    m_RenderPass            {};
    VkPipelineLayout                                m_VkPipelineLayout      {};
    VkPipelineVertexInputStateCreateInfo            m_VkPipeLineSetupInfo   {};
};
