//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

// Contains all Vulkan objects that are required to store and use a texture
// Note that this repository contains a texture loader (vulkantextureloader.h)
// that encapsulates texture loading functionality in a class that is used
// in subsequent demos
class texture : public eng_texture
{
public:

    constexpr                           texture                 ( device& Device )                                                  noexcept : m_Device{ Device } {}
    inline      device&                 getTheDevice            ( void )                                                            noexcept { return m_Device; }
    constexpr   const device&           getTheDevice            ( void )                                                    const   noexcept { return m_Device; }

                void                    setupFrom               (   VkCommandBuffer CmdBuffer, const xbitmap& Bitmap )              noexcept;
                VkDescriptorSet         getDrawDescriptorSet    ( const VkDescriptorImageInfo& DescriptorImageInfo, int Binding )   const   noexcept;
    static      VkFilter                ConvertToVKFilter             ( const filter  Filter  )                                     noexcept;
    static      VkSamplerMipmapMode     ConvertToVKSamplerMipmapMode  ( const mipmap  Mipmap  )                                     noexcept;
    static      VkSamplerAddressMode    ConvertToVKAddressMode        ( const wrap    Wrap    )                                     noexcept;
    constexpr   VkImageView             getView                 ( void )                                                    const   noexcept { return m_View;             }
    virtual     void                    Release                 ( void )                                                            noexcept override;

protected:

    struct format
    {
        VkFormat            m_VKFormat_LinearUnsigned;
        VkFormat            m_VKFormat_LinearSigned;
        VkFormat            m_VKFormat_SRGB;
    };

protected:

    VkFormat    getVKFormat     ( const xbitmap::format Fmt, const bool bLinear, const bool bSinged ) const noexcept;
    void        setImageLayout  (   VkCommandBuffer         CmdBuffer, 
                                    VkImage                 image, 
                                    VkImageAspectFlags      aspectMask, 
                                    VkImageLayout           oldImageLayout, 
                                    VkImageLayout           newImageLayout, 
                                    VkImageSubresourceRange subresourceRange ) noexcept;

protected:

    device&                                             m_Device;
    VkImage                                             m_Image             { nullptr };
    VkImageView                                         m_View              { nullptr };
    VkDeviceMemory                                      m_DeviceMemory      { nullptr };
};
