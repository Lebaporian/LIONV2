//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

class memory_pool
{
    //-----------------------------------------------------------------------
public:

    enum error : u8
    {
        ERR_OK,
        ERR_DEVICE_MEMORY_FAIL,
        ERR_BLOCK_FAIL
    };

    using err = x_err<error>;

    struct allocation
    {
        VkDeviceMemory      m_VKDeviceMemory;
        VkDeviceSize        m_Offset;        
        VkDeviceSize        m_Size;
        xbyte*              m_pSystemPtr;
    };

    //-----------------------------------------------------------------------
public:

    constexpr                   memory_pool         ( eng_vk::device& Device )                                                                       noexcept : m_Device{ Device }{}
    inline                     ~memory_pool         ( void )                                                                                         noexcept;
    inline      void            Free                ( const allocation& Allocation )                                                                 noexcept;
    inline      err             Alloc               ( allocation& Allocation, const VkDeviceSize aSize, const VkMemoryPropertyFlags VKMemPropFlags ) noexcept;

    //-----------------------------------------------------------------------
protected:

    enum { CHUNCK_DEFAULT_ALLOCATION_SIZE = 1 << 25 };
    using spinlk = x_lk_spinlock::base< x_lk_spinlock::reentrance, x_lk_spinlock::do_nothing >;

    struct device_mem_allocation 
    {
        VkDeviceMemory              m_VKDeviceMemory        { VK_NULL_HANDLE };
        VkMemoryPropertyFlags       m_VKMemoryPropFlags     { VK_NULL_HANDLE };
        VkDeviceSize                m_VKDeviceSize          { VK_NULL_HANDLE };
        xbyte*                      m_pSystemPtr            { nullptr };
    };

    struct block 
    {
        xbyte*                      m_pSystemPtr            {};
        VkDeviceSize                m_Offset                {};
        VkDeviceSize                m_Size                  {};
        bool                        m_isFree                {};
    };
 
    struct device_memory 
    {
        xbyte*                      m_pSystemPtr            {};
        xvector<block>              m_lBlocks               {};
        VkDeviceMemory              m_VKDeviceMemory        {};
        VkMemoryPropertyFlags       m_VKMemPropFlags        {};
        VkDeviceSize                m_VKDeviceSize          {};
    };
 
protected:

    inline      void            AddDeviceMemory     ( device_mem_allocation& Alloc )                                                                    noexcept;
    inline      err             DeviceAllocate      ( device_mem_allocation& Alloc, VkMemoryPropertyFlags VKMemoryPropFlags, VkDeviceSize Size )        noexcept;

protected:
#if _X_DEBUG
    x_debug_linear_quantum              m_Debug_LQ  {};
#endif

    eng_vk::device&                     m_Device;
    spinlk                              m_SpinLock              {};
    xvector<device_memory>              m_lDeviceAllocations    {};
};

