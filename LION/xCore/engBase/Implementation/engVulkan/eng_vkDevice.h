//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

class device : public eng_device
{
public:

    struct events
    {
        x_message::event<>      m_JustAfterPageFlip     {};                 // Event thrown just before the page flip
        x_message::event<>      m_JustBeforePageFlip    {};                 // Event thrown just after the page flip
        x_message::event<>      m_InitializeSubSystems  {};                 // Event thrown for any device subsystem that must also be initialized
    };

public:

    constexpr                       device                      ( instance& Instance )                                                  noexcept : m_Instance{ Instance } {}
    inline      events&             getEvents                   ( void )                                                                noexcept { return m_Events;                                 }
    inline      auto&               getMemoryPool               ( void )                                                                noexcept { return m_MemoryPool;                             }
    constexpr   auto                getVKDevice                 ( void )                                                        const   noexcept { return m_VKDevice;                               }
    inline      auto&               getInstance                 ( void )                                                                noexcept { return m_Instance;                               }
    constexpr   auto                getVKInstance               ( void )                                                        const   noexcept { return m_Instance.getVKInstance();               }
    constexpr   auto                getVKPhysicalDevice         ( void )                                                        const   noexcept { return m_VKPhysicalDevice;                       }
    constexpr   auto                getVKDeviceMemProps         ( void )                                                        const   noexcept { return m_VKDeviceMemoryProperties;               }
    constexpr   auto                getFrameNumber              ( void )                                                        const   noexcept { return m_FrameNumber;                            }
    constexpr   auto                getFrameIndex               ( void )                                                        const   noexcept { return static_cast<int>(m_FrameNumber&1);        }
    constexpr   auto                getQueueNodeIndex           ( void )                                                        const   noexcept { return m_iQueueNodeIndex;                        }
    constexpr   auto                getMainQueue                ( void )                                                        const   noexcept { return m_VKQueue;                                }
    constexpr   auto                getVKPostPresentCmdBuffer   ( void )                                                        const   noexcept { return m_VKPostPresentCmdBuffer;                 }
    inline      auto                getVKMainCmdBuffer          ( void )                                                                noexcept { return m_lVKMainCmdBuffers[getFrameIndex()];     }
    inline      auto&               getVKMainCmdBufferList      ( void )                                                                noexcept { return m_lVKMainCmdBuffers;                      }
    inline      auto                getVKSetupCmdBuffer         ( void )                                                                noexcept { return getPerThreadData().m_VKSetupCmdBuffer;    }
    inline      auto&               getDrawSystem               ( void )                                                                noexcept { return m_DrawSystem;          }
    constexpr   auto                getVKPipelineCash           ( void )                                                        const   noexcept { return m_VKPipelineCache;                        }                       
    inline      auto&               getRenderPipelineHash       ( void )                                                                noexcept { return m_PipeLineHash;                           }          
                err                 Initialize                  (   const xndptr<VkQueueFamilyProperties>&  DeviceProps, 
                                                                    const int                               iQueueGraphics, 
                                                                    const VkPhysicalDevice&                 PhysicalDevice, 
                                                                    const bool                              enableValidation )          noexcept;
                void                InitializeDraw              ( VkRenderPass VKRenderPass )                                           noexcept 
                { 
                    m_DrawSystem.Initialize(VKRenderPass,m_VKPipelineCache);
                }
                err                 getSupportedDepthFormat     ( VkFormat& DepthFormat )                                       const   noexcept;
                bool                getMemoryType               ( uint32_t typeBits, VkFlags properties, uint32_t& typeIndex )  const   noexcept;
                err                 CreateVKSetupCommandBuffer  ( void )                                                                noexcept;
                err                 FlushVKSetupCommandBuffer   ( void )                                                                noexcept;
                void                AttachWindow                ( window& Window );
                err                 PrepareWindowAttachment     ( const int iQueueNodeIndex  )                                          noexcept;

                void                onEndRender                 ( void )                                                                noexcept;
                void                onPageFlip                  ( void )                                                                noexcept;
                display_cmds&       getDisplayCmdList           ( const f32 Order, window* pWindow )                                    noexcept;
                void                SubmitDisplayCmdList        ( display_cmds& CmdList )                                               noexcept;
    inline      auto&               getDrawDoubleBuffer         ( void )                                                                noexcept { return getPerThreadData().m_Draw.m_PagedVertIndexData; }


                void                ReleaseTexture              ( texture& Texture )                                                    noexcept;
                void                ReleaseSampler              ( sampler& Sampler )                                                    noexcept;
                void                ReleaseMaterial             ( material_type& Material )                                             noexcept;
                void                ReleaseMaterialInformed     ( material_informed& Material )                                         noexcept;

protected:

    struct draw_perthread
    {
        draw_perthread(  device& Device ) :
            m_PagedVertIndexData{ Device }
            {}

        buffer_double_paged                             m_PagedVertIndexData;                               // Used mainly for draw
    };

    struct perthread_data
    {
        perthread_data( device& Device ) : 
            m_Draw{ Device },
            m_DisplayCmdPool{ Device }
            {}

        VkCommandBuffer                                 m_VKSetupCmdBuffer          { VK_NULL_HANDLE };     // Command buffer used for setup
        display_cmd_pool                                m_DisplayCmdPool;                                   // Dynamic Command buffers used to render a frame
        draw_perthread                                  m_Draw;
    };

    using lk_general  = x_lk_spinlock::base<x_lk_spinlock::non_reentrance>;

protected:

                VkResult            CreateDevice            ( const VkDeviceQueueCreateInfo& queueCreateInfo, const bool enableValidation ) noexcept;
    inline      perthread_data&     getPerThreadData        ( void )                                                                        noexcept { return m_PerThreadData[ x_scheduler::getWorkerUID() ]; }
                err                 CreateCommandPools      ( const int iQueueNodeIndex )                                                   noexcept;
                err                 CreatePipelineCache     ( void )                                                                        noexcept;
                err                 CreateMainCommandBuffers( void )                                                                        noexcept;

    virtual     err                 CreatePipeline          ( eng_pipeline::handle& hPipeline, const eng_pipeline::setup& Setup )           noexcept override;
    virtual     err                 CreateTexture           ( eng_texture::handle&  hTexture,  const xbitmap& Bitmap )                      noexcept override;
    virtual     err                 CreateSampler           ( eng_sampler::handle&  hSampler,  const eng_sampler::setup& Setup )            noexcept override;
    virtual     err                 CreateMaterialType      ( eng_material_type::handle& hMaterial, const eng_material_type::setup& Setup ) noexcept override;
    virtual     err                 CreateMaterialInformed  ( eng_material_informed::handle& hMaterial, const eng_material_informed::setup& Setup ) noexcept override;
    virtual     err                 CreateVertexShader      ( eng_shader_vert::handle& hShaderVert, const eng_shader_vert::setup& Setup )   noexcept override;
    virtual     err                 CreateFragmentShader    ( eng_shader_frag::handle& hShaderFrag, const eng_shader_frag::setup& Setup )   noexcept override;
    virtual     err                 CreateGeometryShader    ( eng_shader_geom::handle& hShaderGeom, const eng_shader_geom::setup& Setup )   noexcept override;
    virtual     err                 CreateUniformStream     ( eng_uniform_stream::handle& hUniformStream, const eng_uniform_stream::setup& Setup )   noexcept override;

    virtual     device_type         getDeviceType           ( void ) const                                                                  noexcept override;
    virtual     void                Release                     ( void )                                                                        noexcept override {}

protected:

    VkDevice                                        m_VKDevice                  { VK_NULL_HANDLE };     // The selected vulkan device
    xndptr<perthread_data>                          m_PerThreadData             { };                    // Data that we will need for different threads
    VkQueue                                         m_VKQueue                   { VK_NULL_HANDLE };     // Handle to the device graphics queue that command buffers are submitted to
    memory_pool                                     m_MemoryPool                { *this          };     // All threads will use this memory pool, we have made it thread safe.
    VkPipelineCache                                 m_VKPipelineCache           { VK_NULL_HANDLE };     // Pipeline cache object
    u64                                             m_FrameNumber               { 0              };     // Frame number... used two switch buffer and such
    int                                             m_iQueueNodeIndex           { -1             };
       
    VkCommandPool                                   m_VKMainCmdPool             { VK_NULL_HANDLE };     // Command buffer pool
    xarray<VkCommandBuffer, 2>                      m_lVKMainCmdBuffers         { nullptr        };     // This double buffer cmd buffer is used as the main cmd list 
    VkCommandBuffer                                 m_VKPostPresentCmdBuffer    { VK_NULL_HANDLE };     // Command buffer for submitting a post present barrier 
    instance&                                       m_Instance;
    VkPhysicalDevice                                m_VKPhysicalDevice          { };
    VkPhysicalDeviceMemoryProperties                m_VKDeviceMemoryProperties  { VK_NULL_HANDLE };     // Stores all available memory (type) properties for the physical device

    draw_system                                     m_DrawSystem                { *this          };
    events                                          m_Events                    {};                     // Event throun by the device class
    rendering_pipeline_hash                         m_PipeLineHash              { *this         };
    x_ll_page_pool_jitc<texture,256>                m_TexturePool               {};
    x_ll_page_pool_jitc<sampler,256>                m_SamplerPool               {};
    x_ll_page_pool_jitc<material_type,256>          m_MaterialTypePool          {};
    x_ll_page_pool_jitc<material_informed,256>      m_MaterialInformedPool      {};
    x_ll_page_pool_jitc<shader_vert,256>            m_ShaderVertPool            {};
    x_ll_page_pool_jitc<shader_frag,256>            m_ShaderFragPool            {};
    x_ll_page_pool_jitc<shader_geom,256>            m_ShaderGeomPool            {};
    x_ll_page_pool_jitc<uniform_stream,256>         m_UniformStreamPool         {};
    x_ll_page_pool_jitc<engpipeline,32>             m_EngPipelinePool           {};
    
protected:

    friend class draw_system;
};
 