//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//-----------------------------------------------------------------------
// Shader
//-----------------------------------------------------------------------
namespace tools
{
    class shader
    {
    public:

        enum errors : u8 
        {
            ERR_OK,
            ERR_FILE,
            ERR_VKFAILURE
        };

        using err = x_err<errors>;

    public:
                            shader                  ( void ) = delete;
                            shader                  ( device& Device )                                                                                      noexcept : m_Device{ Device } {}
                    err     LoadVertexShader        ( const char* fileName,                                        const char* pShaderEntryPoint = "main" ) noexcept;
                    err     LoadFragmentShader      ( const char* fileName,                                        const char* pShaderEntryPoint = "main" ) noexcept;
                    err     LoadShader              ( const char* fileName,           VkShaderStageFlagBits stage, const char* pShaderEntryPoint = "main" ) noexcept; 
                    err     LoadGLSL                ( const char* fileName,           VkShaderStageFlagBits stage, const char* pShaderEntryPoint = "main" ) noexcept;
                    err     LoadSVP                 ( const char* fileName,           VkShaderStageFlagBits stage, const char* pShaderEntryPoint = "main" ) noexcept;
                    err     CreateShader            ( const xbuffer_view<xbyte> Data, VkShaderStageFlagBits stage, const char* pShaderEntryPoint = "main" ) noexcept;
                    err     CreateShaderGLSL        ( const xbuffer_view<xbyte> Data, VkShaderStageFlagBits stage, const char* pShaderEntryPoint = "main" ) noexcept;
        constexpr   auto&   getStateCreateInfo      ( void ) const                                                                                          noexcept { return m_VkShaderStageCreateInfo; }

    protected:

        device&                             m_Device;   
        VkPipelineShaderStageCreateInfo     m_VkShaderStageCreateInfo   {};
    };

    //-----------------------------------------------------------------------

    class shader_vertex : protected shader
    {
    public:

        using errors    = shader::errors;
        using err       = shader::err;

    public:
                            shader_vertex       ( device& Device )                                                          noexcept : shader{ Device } {}
                    err     Load                ( const char* fileName, const char* pShaderEntryPoint = "main" )            noexcept    { return shader::LoadFragmentShader ( fileName, pShaderEntryPoint); }
                    err     Create              ( const xbuffer_view<xbyte>& Data, const char* pShaderEntryPoint = "main" ) noexcept    { return shader::CreateShader       ( Data, VK_SHADER_STAGE_FRAGMENT_BIT, pShaderEntryPoint ); }
        constexpr   auto&   getStateCreateInfo  ( void ) const                                                              noexcept    { return shader::m_VkShaderStageCreateInfo; }
    };

    //-----------------------------------------------------------------------

    class shader_fragment : protected shader
    {
    public:

        using errors    = shader::errors;
        using err       = shader::err;

    public:

                            shader_fragment     ( device& Device )                                                          noexcept : shader{ Device } {}
                    err     Load                ( const char* fileName, const char* pShaderEntryPoint = "main" )            noexcept    { return shader::LoadVertexShader   ( fileName, pShaderEntryPoint); }
                    err     Create              ( const xbuffer_view<xbyte>& Data, const char* pShaderEntryPoint = "main" ) noexcept    { return shader::CreateShader       ( Data, VK_SHADER_STAGE_VERTEX_BIT, pShaderEntryPoint ); }
        constexpr   auto&   getStateCreateInfo  ( void ) const                                                              noexcept    { return shader::m_VkShaderStageCreateInfo; }
    };
}

//-----------------------------------------------------------------------
// Buffer
//-----------------------------------------------------------------------
/*
namespace tools
{
    class buffer
    {
    public:

        enum errors : u8 
        {
            ERR_OK,
            ERR_VKFAILURE,
            ERR_OUT_OF_MEMORY
        };

        using err = x_err<errors>;

        enum type
        {
            TYPE_NULL,
            TYPE_VERTEX_BUFFER,
            TYPE_INDEX_BUFFER,
            TYPE_UNIFORM_BUFFER,
        };

    public:

        err                         CreateBuffer        ( int EntrySize, xuptr Count, type Type, const void* pData = nullptr );
        void*                       CreateMap           ( xuptr StartIndex = 0, xuptr Count = ~0 );
        void                        ReleaseMap          ( void );
        void                        TransferData        ( xuptr DestinationIndex, const void* pSrcData, xuptr Count = ~0 );
        VkBuffer&                   getVkBuffer         ( void )        { return m_VkBuffer;            }
        VkDeviceMemory&             getVkDeviceMemory   ( void )        { return m_VkDeviceMemory;      }
        constexpr xuptr             getByteSize         ( void ) const  { return m_EntrySize*m_Count;   }
        constexpr xuptr             getCount            ( void ) const  { return m_Count;               }
        constexpr xuptr             getElementSize      ( void ) const  { return m_EntrySize;           }

    protected:

    err                             Create              ( eng_instance::base& Engine, const int EntrySize, const xuptr Count, const type Type );

    public:

        int                                             m_EntrySize         {0};
        xuptr                                           m_Count             {0};
        VkBuffer                                        m_VkBuffer          {};
        VkDeviceMemory                                  m_VkDeviceMemory    {};
    };

 }
 */


//-----------------------------------------------------------------------
// vertex descriptors 
//-----------------------------------------------------------------------
namespace tools
{
    class vertex_desc
    {
    public:
    
        enum attribute_src : u8
        {
            ATTR_SRC_NULL,
            ATTR_SRC_U8x4_F,
            ATTR_SRC_S8x4_F,
            ATTR_SRC_U8x4_I,
            ATTR_SRC_S8x4_I,
            ATTR_SRC_U16x2_F,
            ATTR_SRC_S16x2_F,
            ATTR_SRC_U16x2_I,
            ATTR_SRC_S16x2_I,
            ATTR_SRC_U16x4_F,
            ATTR_SRC_S16x4_F,
            ATTR_SRC_U16x4_I,
            ATTR_SRC_S16x4_I,
            ATTR_SRC_F32x1,
            ATTR_SRC_F32x2,
            ATTR_SRC_F32x3,
            ATTR_SRC_F32x4,
            ATTR_SRC_ENUM_COUNT
        };
    
        // Decoding the attr_usage: First element is the "unique id", then the description.
        // note that it is invalid to have two elements with the same unique id.
        enum attribute_usage : u8
        {
            ATTR_USAGE_NULL,
            ATTR_USAGE_POSITION,
            ATTR_USAGE_2_COMPACT_WEIGHTS,            // A 4d vector with upto 2 weight and 2 bone indices
            ATTR_USAGE_4_WEIGHTS,                    // A 4d vector with upto 4 weights 
            ATTR_USAGE_4_INDICES,                    // A 4d vector with upto 4 indices 
            ATTR_USAGE_TANGENT,
            ATTR_USAGE_BINORMAL,
            ATTR_USAGE_NORMAL,
            ATTR_USAGE_00_PARAMETRIC_UV,
            ATTR_USAGE_01_PARAMETRIC_UV,
            ATTR_USAGE_02_PARAMETRIC_UV,
            ATTR_USAGE_03_PARAMETRIC_UV,
            ATTR_USAGE_04_PARAMETRIC_UV,
            ATTR_USAGE_05_PARAMETRIC_UV,
            ATTR_USAGE_06_PARAMETRIC_UV,
            ATTR_USAGE_07_PARAMETRIC_UV,
            ATTR_USAGE_08_PARAMETRIC_UV,
            ATTR_USAGE_09_PARAMETRIC_UV,
            ATTR_USAGE_00_FULLRANGE_UV,
            ATTR_USAGE_01_FULLRANGE_UV,
            ATTR_USAGE_02_FULLRANGE_UV,
            ATTR_USAGE_03_FULLRANGE_UV,
            ATTR_USAGE_04_FULLRANGE_UV,
            ATTR_USAGE_05_FULLRANGE_UV,
            ATTR_USAGE_06_FULLRANGE_UV,
            ATTR_USAGE_07_FULLRANGE_UV,
            ATTR_USAGE_08_FULLRANGE_UV,
            ATTR_USAGE_09_FULLRANGE_UV,
            ATTR_USAGE_00_RGBA,
            ATTR_USAGE_01_RGBA,
            ATTR_USAGE_02_RGBA,
            ATTR_USAGE_03_RGBA,
            ATTR_USAGE_04_RGBA,
            ATTR_USAGE_05_RGBA,
            ATTR_USAGE_06_RGBA,
            ATTR_USAGE_00_GENERIC_V1,
            ATTR_USAGE_01_GENERIC_V1,
            ATTR_USAGE_02_GENERIC_V1,
            ATTR_USAGE_03_GENERIC_V1,
            ATTR_USAGE_04_GENERIC_V1,
            ATTR_USAGE_00_GENERIC_V2,
            ATTR_USAGE_01_GENERIC_V2,
            ATTR_USAGE_02_GENERIC_V2,
            ATTR_USAGE_03_GENERIC_V2,
            ATTR_USAGE_04_GENERIC_V2,
            ATTR_USAGE_00_GENERIC_V3,
            ATTR_USAGE_01_GENERIC_V3,
            ATTR_USAGE_02_GENERIC_V3,
            ATTR_USAGE_03_GENERIC_V3,
            ATTR_USAGE_04_GENERIC_V3,
            ATTR_USAGE_00_GENERIC_V4,
            ATTR_USAGE_01_GENERIC_V4,
            ATTR_USAGE_02_GENERIC_V4,
            ATTR_USAGE_03_GENERIC_V4,
            ATTR_USAGE_04_GENERIC_V4,
            ATTR_USAGE_ENUM_COUNT
        };

        struct attribute
        {
            u16                             m_Offset;
            attribute_src                   m_DataType;
            attribute_usage                 m_UsageType;

            void setup( attribute_usage UsageType, attribute_src DataType, int Offset )
            {
                x_assume( Offset >= 0 && Offset <= 0xffff );
            
                m_Offset    = static_cast<u16>(Offset);
                m_DataType  = DataType;
                m_UsageType = UsageType;
            }
        };
    
        struct attribute_link
        {
            u8                              m_Location;
            attribute_usage                 m_UsageType;

                        attribute_link      ( void ) = default;
            inline      attribute_link      ( int Location, attribute_usage Usage ) : 
                m_Location{ static_cast<u8>(Location) }, 
                m_UsageType{ Usage } 
            {
                x_assume( m_Location < 0xff );
            }

            void setup      ( int Location, attribute_usage UsageType )
            {
                x_assume( Location >= 0 && Location <= 0xff );

                m_Location  = static_cast<u8>(Location);
                m_UsageType = UsageType;
            }
        };

    public:

    void                                setup               (  int VertSize, xbuffer_view<attribute> Data )                                                 noexcept;
    void                                setup               ( int VertSize, xowner<const attribute*> pAttribute, int nAttributes, bool bFreeDataWhenDone )  noexcept;
    constexpr const attribute* const    getAttributeList    ( void ) const                                                                                  noexcept { return m_pData;        }
    constexpr int                       getAttributeCount   ( void ) const                                                                                  noexcept { return m_nAttributes;  }
    constexpr int                       getVertexSize       ( void ) const                                                                                  noexcept { return m_VertexSize;   }

    protected:

        xowner<const attribute*>        m_pData         { nullptr   };
        u16                             m_nAttributes   { 0         };
        u16                             m_VertexSize    { 0         };
        u8                              m_bFreeData     { false     };
    };
}

//-----------------------------------------------------------------------
// vertex pipeline  
//-----------------------------------------------------------------------
namespace tools
{
    class shader_program
    {
    public:

        void setup( tools::shader_vertex& ShaderVertex, tools::shader_vertex& ShaderFragment )
        {

        }

    protected:

        VkPipelineLayout                                    m_VkPipelineLayout          {};
        VkDescriptorSet                                     m_VkDescriptorSet           {};
        VkDescriptorSetLayout                               m_VkDescriptorSetLayout     {};
        VkDescriptorPool                                    m_VKDescriptorPool          {};
    };

};

//-----------------------------------------------------------------------
// vertex pipeline descriptors
//-----------------------------------------------------------------------
namespace tools
{
    class vertex_pipeline_descriptors 
    {
    public:
        
                    void            setup           ( int BindingIndex, const vertex_desc& VertexDesc, const xbuffer_view<vertex_desc::attribute_link>& Links ) noexcept;
        constexpr   const auto&     getCreationInfo ( void ) const                                                                                              noexcept { return m_VkPipeLineSetupInfo; }

    protected:

        VkVertexInputBindingDescription                 m_VkBindingDescription  {};
        xndptr<VkVertexInputAttributeDescription>       m_AttributeDescriptions {};
        VkPipelineVertexInputStateCreateInfo            m_VkPipeLineSetupInfo   {};
    };
};

//-----------------------------------------------------------------------
// tools
//-----------------------------------------------------------------------

namespace tools
{
    namespace initializers
    {
        inline VkImageMemoryBarrier ImageMemoryBarrier( void )
        {
            VkImageMemoryBarrier imageMemoryBarrier = {};
            imageMemoryBarrier.sType                = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
            imageMemoryBarrier.pNext                = nullptr;

            // Some default values
            imageMemoryBarrier.srcQueueFamilyIndex  = VK_QUEUE_FAMILY_IGNORED;
            imageMemoryBarrier.dstQueueFamilyIndex  = VK_QUEUE_FAMILY_IGNORED;
            return imageMemoryBarrier;
        }

        inline VkCommandBufferAllocateInfo commandBufferAllocateInfo( VkCommandPool commandPool, VkCommandBufferLevel level, uint32_t bufferCount )
        {
            VkCommandBufferAllocateInfo commandBufferAllocateInfo = {};
            commandBufferAllocateInfo.sType                 = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
            commandBufferAllocateInfo.commandPool           = commandPool;
            commandBufferAllocateInfo.level                 = level;
            commandBufferAllocateInfo.commandBufferCount    = bufferCount;
            return commandBufferAllocateInfo;
        }
    }

    //---------------------------------------------------------------------------

    x_errdef::err readBinaryFile( xafptr<xbyte>& Data, const char* filename ) noexcept;

    //---------------------------------------------------------------------------

    inline
    void setImageLayout(
        VkCommandBuffer cmdbuffer, 
        VkImage image, 
        VkImageAspectFlags aspectMask, 
        VkImageLayout oldImageLayout, 
        VkImageLayout newImageLayout,
        VkImageSubresourceRange subresourceRange)
    {
        // Create an image barrier object
        VkImageMemoryBarrier imageMemoryBarrier {};
        imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
        imageMemoryBarrier.pNext = NULL;
        imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            
        imageMemoryBarrier.oldLayout = oldImageLayout;
        imageMemoryBarrier.newLayout = newImageLayout;
        imageMemoryBarrier.image = image;
        imageMemoryBarrier.subresourceRange = subresourceRange;

        // Source layouts (old)
        // Source access mask controls actions that have to be finished on the old layout
        // before it will be transitioned to the new layout
        switch (oldImageLayout)
        {
        case VK_IMAGE_LAYOUT_UNDEFINED:
                // Image layout is undefined (or does not matter)
                // Only valid as initial layout
                // No flags required, listed only for completeness
                imageMemoryBarrier.srcAccessMask = 0;
                break;

        case VK_IMAGE_LAYOUT_PREINITIALIZED:
                // Image is preinitialized
                // Only valid as initial layout for linear images, preserves memory contents
                // Make sure host writes have been finished
                imageMemoryBarrier.srcAccessMask = VK_ACCESS_HOST_WRITE_BIT;
                break;

        case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL:
                // Image is a color attachment
                // Make sure any writes to the color buffer have been finished
                imageMemoryBarrier.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
                break;

        case VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL:
                // Image is a depth/stencil attachment
                // Make sure any writes to the depth/stencil buffer have been finished
                imageMemoryBarrier.srcAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
                break;

        case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL:
                // Image is a transfer source 
                // Make sure any reads from the image have been finished
                imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
                break;

        case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:
                // Image is a transfer destination
                // Make sure any writes to the image have been finished
                imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
                break;

        case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL:
                // Image is read by a shader
                // Make sure any shader reads from the image have been finished
                imageMemoryBarrier.srcAccessMask = VK_ACCESS_SHADER_READ_BIT;
                break;
        }

        // Target layouts (new)
        // Destination access mask controls the dependency for the new image layout
        switch (newImageLayout)
        {
        case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:
            // Image will be used as a transfer destination
            // Make sure any writes to the image have been finished
            imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
            break;

        case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL:
            // Image will be used as a transfer source
            // Make sure any reads from and writes to the image have been finished
            imageMemoryBarrier.srcAccessMask = imageMemoryBarrier.srcAccessMask | VK_ACCESS_TRANSFER_READ_BIT;
            imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
            break;

        case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL:
            // Image will be used as a color attachment
            // Make sure any writes to the color buffer have been finished
            imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
            imageMemoryBarrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
            break;

        case VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL:
            // Image layout will be used as a depth/stencil attachment
            // Make sure any writes to depth/stencil buffer have been finished
            imageMemoryBarrier.dstAccessMask |= VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
            break;

        case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL:
            // Image will be read in a shader (sampler, input attachment)
            // Make sure any writes to the image have been finished
            if (imageMemoryBarrier.srcAccessMask == 0)
            {
                imageMemoryBarrier.srcAccessMask = VK_ACCESS_HOST_WRITE_BIT | VK_ACCESS_TRANSFER_WRITE_BIT;
            }
            imageMemoryBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
            break;
        }

        // Put barrier on top
        VkPipelineStageFlags srcStageFlags  = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        VkPipelineStageFlags destStageFlags = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;

        // Put barrier inside setup command buffer
        vkCmdPipelineBarrier(
            cmdbuffer, 
            srcStageFlags, 
            destStageFlags, 
            0, 
            0, nullptr,
            0, nullptr,
            1, &imageMemoryBarrier);
    }

    //---------------------------------------------------------------------------

    // Fixed sub resource on first mip level and layer
    inline
    void setImageLayout(
        VkCommandBuffer cmdbuffer,
        VkImage image,
        VkImageAspectFlags aspectMask,
        VkImageLayout oldImageLayout,
        VkImageLayout newImageLayout)
    {
        VkImageSubresourceRange subresourceRange = {};
        subresourceRange.aspectMask = aspectMask;
        subresourceRange.baseMipLevel = 0;
        subresourceRange.levelCount = 1;
        subresourceRange.layerCount = 1;
        setImageLayout(cmdbuffer, image, aspectMask, oldImageLayout, newImageLayout, subresourceRange);
    }
/*
    // Create an image memory barrier for changing the layout of
    // an image and put it into an active command buffer
    // See chapter 11.4 "Image Layout" for details
    //todo : rename
    inline void setImageLayout( 
        VkCommandBuffer     cmdbuffer, 
        VkImage             image, 
        VkImageAspectFlags  aspectMask, 
        VkImageLayout       oldImageLayout, 
        VkImageLayout       newImageLayout )
    {
        // Create an image barrier object
        VkImageMemoryBarrier imageMemoryBarrier             = initializers::ImageMemoryBarrier();
        imageMemoryBarrier.oldLayout                        = oldImageLayout;
        imageMemoryBarrier.newLayout                        = newImageLayout;
        imageMemoryBarrier.image                            = image;
        imageMemoryBarrier.subresourceRange.aspectMask      = aspectMask;
        imageMemoryBarrier.subresourceRange.baseMipLevel    = 0;
        imageMemoryBarrier.subresourceRange.levelCount      = 1;
        imageMemoryBarrier.subresourceRange.layerCount      = 1;

        // Source layouts (old)

        // Undefined layout
        // Only allowed as initial layout!
        // Make sure any writes to the image have been finished
        if (oldImageLayout == VK_IMAGE_LAYOUT_UNDEFINED)
        {
            if( imageMemoryBarrier.srcQueueFamilyIndex != VK_QUEUE_FAMILY_IGNORED ) 
                imageMemoryBarrier.srcAccessMask = VK_ACCESS_HOST_WRITE_BIT | VK_ACCESS_TRANSFER_WRITE_BIT;
        }

        // Old layout is m_Color attachment
        // Make sure any writes to the m_Color buffer have been finished
        if (oldImageLayout == VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL) 
        {
            imageMemoryBarrier.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        }

        // Old layout is transfer source
        // Make sure any reads from the image have been finished
        if (oldImageLayout == VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL)
        {
            imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
        }

        // Old layout is shader read (sampler, input attachment)
        // Make sure any shader reads from the image have been finished
        if (oldImageLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
        {
            imageMemoryBarrier.srcAccessMask = VK_ACCESS_SHADER_READ_BIT;
        }

        // Target layouts (new)

        // New layout is transfer destination (copy, blit)
        // Make sure any copyies to the image have been finished
        if (newImageLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
        {
            imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        }

        // New layout is transfer source (copy, blit)
        // Make sure any reads from and writes to the image have been finished
        if (newImageLayout == VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL)
        {
            imageMemoryBarrier.srcAccessMask = imageMemoryBarrier.srcAccessMask | VK_ACCESS_TRANSFER_READ_BIT;
            imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
        }

        // New layout is m_Color attachment
        // Make sure any writes to the m_Color buffer hav been finished
        if (newImageLayout == VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL)
        {
            imageMemoryBarrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
            imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
        }

        // New layout is depth attachment
        // Make sure any writes to depth/stencil buffer have been finished
        if (newImageLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) 
        {
            imageMemoryBarrier.dstAccessMask = imageMemoryBarrier.srcAccessMask | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
        }

        // New layout is shader read (sampler, input attachment)
        // Make sure any writes to the image have been finished
        if (newImageLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
        {
            imageMemoryBarrier.srcAccessMask = VK_ACCESS_HOST_WRITE_BIT | VK_ACCESS_TRANSFER_WRITE_BIT;
            imageMemoryBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
        }


        // Put barrier on top
        VkPipelineStageFlags srcStageFlags  = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        VkPipelineStageFlags destStageFlags = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;

        // Put barrier inside setup command buffer
        vkCmdPipelineBarrier(
            cmdbuffer, 
            srcStageFlags, 
            destStageFlags, 
            0, 
            0, nullptr,
            0, nullptr,
            1, &imageMemoryBarrier);
    }
*/
}


//-----------------------------------------------------------------------
// Complex Buffers
//-----------------------------------------------------------------------
#if 0
namespace tools
{
   class buffer_paged
    {
    public:

        struct alloc_return { void* m_pStart; xuptr m_Offset; int m_iBuffer; };

        struct setup
        {
            buffer::type            m_Type                  { buffer::type::TYPE_NULL };
            int                     m_MaxBuffers            { -1 };
            int                     m_EntryPerBuffer        { -1 };
            int                     m_EntrySize             { -1 };
        };

    public:

        //-----------------------------------------------------------------------

        void        Initialize( const setup& Setup )
        {
            x_assert( Setup.m_Type              !=  buffer::type::TYPE_NULL );
            x_assert( Setup.m_MaxBuffers        >   0   );
            x_assert( Setup.m_EntryPerBuffer    >   10  );
            x_assert( Setup.m_EntrySize         >   1   );

            m_Type              = Setup.m_Type;
            m_MaxBuffers        = Setup.m_MaxBuffers;
            m_EntryPerBuffer    = Setup.m_EntryPerBuffer;
            m_EntrySize         = Setup.m_EntrySize;
            m_nActiveBuffers    = 0;
             
            m_EngBuffer.New( m_MaxBuffers );
            m_lEntries.New( m_MaxBuffers );
        }

        //-----------------------------------------------------------------------

        void         Upload( eng_instance::base& Engine )
        {
            x_lk_guard( m_SpinLock );

            for( int i = 0; i < m_nActiveBuffers; i++ )
            {
                auto& Entry = m_lEntries[i];
                x_assert( Entry.m_pStart );

                // Unmap any pending memory
                m_EngBuffer[ Entry.m_iBuffer ].ReleaseMap( Engine );
                
                // Reset the entry
                Entry.m_pStart          = nullptr;
                Entry.m_nEntriesLeft    = m_EntryPerBuffer;
            }
            m_nActiveBuffers = 0;
        }

        //-----------------------------------------------------------------------

        alloc_return Alloc( eng_instance::base& Engine, xuptr nEntires )
        {
            int             i;

            x_assert( nEntires < m_EntryPerBuffer );
            x_lk_guard( m_SpinLock );

            for( i = 0; i < m_nActiveBuffers; i++ )
            {
                if( m_lEntries[i].m_nEntriesLeft >= nEntires )
                    break;
            }

            if( i == m_nActiveBuffers )
            {
                auto& NewEngBuff = m_EngBuffer[m_nActiveBuffers ];
                auto& NewEntry   = m_lEntries[m_nActiveBuffers ];

                x_assert( m_nActiveBuffers < m_MaxBuffers );

                if( NewEngBuff.getCount() == 0 ) 
                    NewEngBuff.CreateBuffer( Engine, m_EntrySize, m_EntryPerBuffer, m_Type );

                NewEntry.m_pStart          = (xbyte*)NewEngBuff.CreateMap( Engine );
                NewEntry.m_nEntriesLeft    = m_EntryPerBuffer;
                NewEntry.m_iBuffer         = m_nActiveBuffers;

                i = m_nActiveBuffers;
                m_nActiveBuffers++;
            }

            alloc_return    Ret;
            Ret.m_Offset    = m_EntryPerBuffer - m_lEntries[i].m_nEntriesLeft;
            Ret.m_pStart    = &m_lEntries[i].m_pStart[ Ret.m_Offset * m_EntrySize ]; 
            Ret.m_iBuffer   = i;

            // Remove all entries used
            m_lEntries[i].m_nEntriesLeft -= nEntires;

            return Ret;

            /*
            xuptr       iLocation;
            const bool  bFound = x_BinSearch<entry>( x_buffer_view<entry>( m_lEntries, m_nActiveBuffers ), iLocation, [&]( const entry& Entry ) -> int
            {
                if( nEntires < Entry.m_nEntriesLeft ) return -1;
                return nEntires > Entry.m_nEntriesLeft;
            });

            if( bFound )
            {
                m_lEntries[iLocation].m_nEntriesLeft 
            }
            */
        }

        //-----------------------------------------------------------------------

        auto& getVkBuffer( s32 iBuffer ) { return m_EngBuffer[iBuffer].getVkBuffer(); }

    protected:

        struct entry
        {
            xbyte*      m_pStart        { nullptr   };
            xuptr       m_nEntriesLeft  { xuptr(~0) };
            int         m_iBuffer       { -1        };
        };

        using spinlk = x_lk_spinlock::base< x_lk_spinlock::non_reentrance, x_lk_spinlock::do_light_jobs >;

    protected:

        xndptr<buffer>      m_EngBuffer             {};
        xndptr<entry>       m_lEntries              {};
        spinlk              m_SpinLock              {};
        buffer::type        m_Type                  { buffer::type::TYPE_NULL };
        int                 m_MaxBuffers            { -1 };
        int                 m_EntryPerBuffer        { -1 };
        int                 m_EntrySize             { -1 };
        int                 m_nActiveBuffers        { -1 };
    };


    class buffer_double_paged
    {
    public:

        using alloc_return = buffer_paged::alloc_return;

        struct setup
        {
            int                     m_MaxVertexBuffers      { -1 };
            int                     m_MaxIndexBuffers       { -1 };
            int                     m_MaxVerticesPerBuffer  { -1 };
            int                     m_MaxIndicesPerBuffer   { -1 };
            int                     m_VertexEntrySize       { -1 };
        };

        void Initialize( const setup& Setup, const xbuffer_view<tools::vertex_desc::attribute> VertexDesc )
        {
            //
            // Vertex Buffer
            //
            {
                tools::buffer_paged::setup BufferSetup;
                BufferSetup.m_Type            = tools::buffer::TYPE_VERTEX_BUFFER;
                BufferSetup.m_MaxBuffers      = Setup.m_MaxVertexBuffers;
                BufferSetup.m_EntryPerBuffer  = Setup.m_MaxVerticesPerBuffer;
                BufferSetup.m_EntrySize       = Setup.m_VertexEntrySize;
                m_VertexBuffer[0].Initialize( BufferSetup );
                m_VertexBuffer[1].Initialize( BufferSetup );
            }

            //
            // Index Buffer
            //
            {
                tools::buffer_paged::setup BufferSetup;
                BufferSetup.m_Type            = tools::buffer::TYPE_INDEX_BUFFER;
                BufferSetup.m_MaxBuffers      = Setup.m_MaxIndexBuffers;
                BufferSetup.m_EntryPerBuffer  = Setup.m_MaxIndicesPerBuffer;
                BufferSetup.m_EntrySize       = sizeof(u32);
                m_IndexBuffer[0].Initialize( BufferSetup );
                m_IndexBuffer[1].Initialize( BufferSetup );
            }

            //
            // Initialize vertex descritors
            //
            m_VDesc.setup( Setup.m_VertexEntrySize, VertexDesc );
        }

        alloc_return AllocVertex( eng_instance::base& Engine, xuptr Count )
        {
            return m_VertexBuffer[m_iBuffer].Alloc( Engine, Count );
        }

        alloc_return AllocIndex( eng_instance::base& Engine, xuptr Count )
        {
            return m_IndexBuffer[m_iBuffer].Alloc( Engine, Count );
        }

        vertex_desc& getVertDesc    ( void ) { return m_VDesc; }

        auto& getVertexVkBuffer ( s32 iBuffer ) { return m_VertexBuffer[m_iBuffer].getVkBuffer( iBuffer ); }
        auto& getIndexVkBuffer ( s32 iBuffer ) { return m_IndexBuffer[m_iBuffer].getVkBuffer( iBuffer ); }

        void PageFlip( eng_instance::base& Engine )
        {
            m_VertexBuffer[m_iBuffer].Upload( Engine );
            m_IndexBuffer[m_iBuffer].Upload( Engine );
            m_iBuffer = 1 - m_iBuffer;

        }
    protected:

        vertex_desc                 m_VDesc             {};
        xarray<buffer_paged,2>      m_VertexBuffer      {};
        xarray<buffer_paged,2>      m_IndexBuffer       {};
        u32                         m_iBuffer           {0};
    };
}
#endif

//-----------------------------------------------------------------------
// Cmd Buffers
//-----------------------------------------------------------------------
namespace tools
{
    class cmd_buffer_double
    {
    public:
                            cmd_buffer_double   ( device& Device )                                              noexcept : m_Device{ Device } {}
        void                Initialize          ( void )                                                        noexcept;
        void                Kill                ( void )                                                        noexcept;
        VkCommandBuffer     getCmdBuffer        ( void )                                                        noexcept;
        VkCommandBuffer     RenderBegin         (   VkRenderPass                    GlobalRenderPass, 
                                                    VkFramebuffer                   Framebuffer,
                                                    const xbuffer_view<VkViewport>  ViewPort,
                                                    const xbuffer_view<VkRect2D>    Scissor )                   noexcept;
        void                RenderEnd           ( void )                                                        noexcept;
        void                SubmitCmds          ( void )                                                        noexcept;

    protected:
        
        device&                     m_Device;
        xarray<VkCommandBuffer,2>   m_CmdBuffers        {};
        VkCommandPool               m_CmdPool           {};
        uint32_t                    m_iCurrentBuffer    {~0u};
    };
}
