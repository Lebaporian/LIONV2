//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

class instance : public eng_instance
{
public:

                    err             Initialize          ( const eng_instance::setup& Setup )        noexcept;
    constexpr       VkInstance      getVKInstance       ( void ) const                              noexcept { return m_VkInstance;             }
    constexpr       const auto&     getErrorLogChannel  ( void ) const                              noexcept { return m_ErrorLogChannel;        }
    constexpr       const auto&     getInfoLogChannel   ( void ) const                              noexcept { return m_InfoLogChannel;         }
                    
protected:

    VkResult        CreateVKInstance    ( const bool enableValidation, const char* pAppName )       noexcept;
    err             EnumerateDevices    ( xndptr<VkPhysicalDevice>& PhysicalDevices )               noexcept;
    err             DeviceProperties    (   xndptr<VkQueueFamilyProperties>&      DeviceProps, 
                                            int&                                  iDeviceQueue, 
                                            const VkPhysicalDevice&               PhysicalDevice, 
                                          const VkQueueFlagBits                 VKQFlagBits
                                        ) noexcept;
    err             CreateWindow        ( eng_window*& pWindow, const eng_window::setup& Setup )    noexcept;
    err             CreateDevice        ( eng_device*& pDevice, const eng_device::setup& Setup )    noexcept;
    void            CreateLogConsole    ( void ) const                                              noexcept;
     virtual     void                Release                     ( void )                                                                        noexcept override {}

protected:

    VkInstance                          m_VkInstance                { VK_NULL_HANDLE    };
    HINSTANCE                           m_hInstance                 { 0                 };        
    xndptr<VkPhysicalDevice>            m_PhysicalDevices           {};
    bool                                m_bVerification             { false             };
    xvector<xndptr_s<eng_vk::device>>   m_lDevices                  {};
    xvector<xndptr_s<eng_vk::window>>   m_lWindows                  {};
    x_reporting::log_channel            m_ErrorLogChannel           {"ENG_INSTANCE - Error:"};
    x_reporting::log_channel            m_InfoLogChannel            {"ENG_INSTANCE - Info:"};

protected:

    friend class eng_instance;
};
