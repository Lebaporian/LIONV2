//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

class material_type final : public eng_material_type
{
public:
                                    material_type       ( void )                                            noexcept = delete;
                                    material_type       ( device& Device )                                  noexcept : m_Device{ Device } {}
                void                Initialize          ( const setup& Setup )                              noexcept;
    virtual     void                Release             ( void )                                            noexcept override;
                VkDescriptorSet     AllocDescriptorSet  ( void )                                            noexcept;
    constexpr   VkPipelineLayout    getVKPipelineLayout ( void )                                    const   noexcept { return m_VKPipelineLayout; }
    inline      const auto          getVKShaderStages   ( void )                                            noexcept { return xbuffer_view<VkPipelineShaderStageCreateInfo>{ m_ShaderStages, static_cast<xuptr>(m_nShaderStages) }; }

protected:

    class descriptor_pool
    {
        x_object_type( descriptor_pool, is_linear );

    public:
        VkDescriptorPool  m_VKDescriptorPool    {};
    };

    using lk_general  = x_lk_spinlock::base< x_lk_spinlock::non_reentrance, x_lk_spinlock::do_nothing >;

protected:

    device&                                         m_Device;
    
    int                                             m_nVKDescriptorSetLayout    {0};
    xarray<VkDescriptorSetLayout,1>                 m_VKDescriptorSetLayout     {};
    VkPipelineLayout                                m_VKPipelineLayout          {};
    x_locked_object<descriptor_pool, lk_general>    m_LockedVKDescriptorPool    {}; 

    int                                             m_nShaderStages             {0};
    xarray<VkPipelineShaderStageCreateInfo,3>       m_ShaderStages              {};

    /*
    class pool
    {
        x_object_type( pool, is_linear );

    public:

        pop( void )

    protected:

        struct node
        {
            int              m_nEntries         { 0 };
            VkDescriptorPool m_VKDescriptorPool { nullptr };
        };

        using link_list = x_linear_link_list<node>;
        
    protected:

        link_list m_PagePool;
    };

    using lk_general  = x_lk_spinlock::base< x_lk_spinlock::non_reentrance >;

    x_locked_object<pool, lk_general>   m_DescriptorPagePool          {};                     // This descriptor pool is used to allocate descriptors for the system
*/
};


