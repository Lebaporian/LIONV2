//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

#include "../eng_vkBase.h"

//------------------------------------------------------------------------------------
// NAMESPACE
//------------------------------------------------------------------------------------
namespace eng_vk{

//------------------------------------------------------------------------------- 
                       
VkDescriptorSet texture::getDrawDescriptorSet( const VkDescriptorImageInfo& DescriptorImageInfo, int Binding ) const noexcept
{
    // Allocate one descriptor set
    VkDescriptorSet VkDescriptorSet = m_Device.getDrawSystem().AllocDescriptorSet();

    // Bind the texture
    VkWriteDescriptorSet writeDescriptorSet = {};
    writeDescriptorSet.sType                = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    writeDescriptorSet.pNext                = nullptr;
    writeDescriptorSet.dstSet               = VkDescriptorSet;
    writeDescriptorSet.descriptorCount      = 1;
    writeDescriptorSet.descriptorType       = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    writeDescriptorSet.pImageInfo           = &DescriptorImageInfo;
    writeDescriptorSet.dstBinding           = Binding;

    vkUpdateDescriptorSets( m_Device.getVKDevice(), 1, &writeDescriptorSet, 0, nullptr );

    return VkDescriptorSet;
}

//-------------------------------------------------------------------------------                        

VkFilter texture::ConvertToVKFilter( const filter Filter ) noexcept
{
    static const xarray<VkFilter,FILTER_COUNT> FilterTable = []() noexcept -> auto
    {
        xarray<VkFilter,FILTER_COUNT> FilterTable {};
        FilterTable[FILTER_NEAREST]         =   VK_FILTER_NEAREST;
        FilterTable[FILTER_LINEAR]          =   VK_FILTER_LINEAR;
        FilterTable[FILTER_CUBIC_IMG]       =   VK_FILTER_CUBIC_IMG;
        return FilterTable;
    }();
    return FilterTable[Filter];
}

//-------------------------------------------------------------------------------                        

VkSamplerMipmapMode texture::ConvertToVKSamplerMipmapMode( const mipmap Mipmap ) noexcept
{
    static const xarray<VkSamplerMipmapMode,MIPMAP_COUNT> MipmapTable = []() noexcept -> auto
    {
        xarray<VkSamplerMipmapMode,MIPMAP_COUNT> MipmapTable {};
        MipmapTable[MIPMAP_NEAREST]             =   VK_SAMPLER_MIPMAP_MODE_NEAREST;
        MipmapTable[MIPMAP_LINEAR]              =   VK_SAMPLER_MIPMAP_MODE_LINEAR;
        return MipmapTable;
    }();
    return MipmapTable[Mipmap];
}

//-------------------------------------------------------------------------------                        

VkSamplerAddressMode texture::ConvertToVKAddressMode( const wrap Wrap ) noexcept
{
    static const xarray<VkSamplerAddressMode,WRAP_COUNT> AddressModeTable = []() noexcept -> auto
    {
        xarray<VkSamplerAddressMode,WRAP_COUNT> AddressModeTable {};
        AddressModeTable[WRAP_REPEAT]               =   VK_SAMPLER_ADDRESS_MODE_REPEAT;
        AddressModeTable[WRAP_CLAMP_EDGE]           =   VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
        AddressModeTable[WRAP_CLAMP_BORDER]         =   VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
        AddressModeTable[WRAP_MIRROR_REPEAT]        =   VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT;
        AddressModeTable[WRAP_MIRROR_CLAMP_EDGE]    =   VK_SAMPLER_ADDRESS_MODE_MIRROR_CLAMP_TO_EDGE;
        return AddressModeTable;
    }();
    return AddressModeTable[Wrap];
}

//-------------------------------------------------------------------------------                        

VkFormat texture::getVKFormat( const xbitmap::format Fmt, const bool bLinear, const bool bSinged ) const noexcept
{
    // https://www.khronos.org/registry/vulkan/specs/1.0/xhtml/vkspec.html 
    // VK Interpretation of Numeric Format
    // UNORM   - The components are unsigned normalized values in the range [0,1]
    // SNORM   - The components are signed normalized values in the range [-1,1]
    // USCALED - The components are unsigned integer values that get converted to floating-point in the range [0,2n-1]
    // SSCALED - The components are signed integer values that get converted to floating-point in the range [-2n-1,2n-1-1]
    // UINT    - The components are unsigned integer values in the range [0,2n-1]
    // SINT    - The components are signed integer values in the range [-2n-1,2n-1-1]
    // UFLOAT  - The components are unsigned floating-point numbers (used by packed, shared exponent, and some compressed formats)
    // SFLOAT  - The components are signed floating-point numbers
    // SRGB    - The R, G, and B components are unsigned normalized values that represent values using sRGB nonlinear encoding, 
    //           while the A component (if one exists) is a regular unsigned normalized value
    static const xarray<format,xbitmap::format::FORMAT_TOTAL> FormatTable = []() noexcept -> auto
    {
        xarray<format,xbitmap::format::FORMAT_TOTAL> FormatTable = { format{VK_FORMAT_UNDEFINED,VK_FORMAT_UNDEFINED,VK_FORMAT_UNDEFINED} };

        //                     XFormat                                           VKFormat_LinearUnsigned                VKFormat_LinearSigned                   VKFormat_SRGB
        //        ---------------------------------                           ----------------------------------    -----------------------------------    ------------------------------------
        FormatTable[xbitmap::format::FORMAT_R4G4B4A4            ] = format  { VK_FORMAT_R4G4B4A4_UNORM_PACK16     , VK_FORMAT_UNDEFINED                  , VK_FORMAT_UNDEFINED                  };
        FormatTable[xbitmap::format::FORMAT_R8G8B8              ] = format  { VK_FORMAT_R8G8B8_UNORM              , VK_FORMAT_R8G8B8_SNORM               , VK_FORMAT_R8G8B8_SRGB                };
        FormatTable[xbitmap::format::FORMAT_R8G8B8U8            ] = format  { VK_FORMAT_R8G8B8A8_UNORM            , VK_FORMAT_R8G8B8A8_SNORM             , VK_FORMAT_R8G8B8A8_SRGB              };
        FormatTable[xbitmap::format::FORMAT_R8G8B8A8            ] = format  { VK_FORMAT_R8G8B8A8_UNORM            , VK_FORMAT_R8G8B8A8_SNORM             , VK_FORMAT_R8G8B8A8_SRGB              };
        FormatTable[xbitmap::format::FORMAT_A8R8G8B8            ] = format  { VK_FORMAT_UNDEFINED                 , VK_FORMAT_UNDEFINED                  , VK_FORMAT_UNDEFINED                  };
        FormatTable[xbitmap::format::FORMAT_U8R8G8B8            ] = format  { VK_FORMAT_UNDEFINED                 , VK_FORMAT_UNDEFINED                  , VK_FORMAT_UNDEFINED                  };
        
        FormatTable[xbitmap::format::FORMAT_PAL4_R8G8B8A8       ] = format  { VK_FORMAT_UNDEFINED                 , VK_FORMAT_UNDEFINED                  , VK_FORMAT_UNDEFINED                  };
        FormatTable[xbitmap::format::FORMAT_PAL8_R8G8B8A8       ] = format  { VK_FORMAT_UNDEFINED                 , VK_FORMAT_UNDEFINED                  , VK_FORMAT_UNDEFINED                  };
        
        FormatTable[xbitmap::format::FORMAT_ETC2_4RGB           ] = format  { VK_FORMAT_ETC2_R8G8B8_UNORM_BLOCK   , VK_FORMAT_UNDEFINED                  , VK_FORMAT_ETC2_R8G8B8_SRGB_BLOCK     };
        FormatTable[xbitmap::format::FORMAT_ETC2_4RGBA1         ] = format  { VK_FORMAT_ETC2_R8G8B8A1_UNORM_BLOCK , VK_FORMAT_UNDEFINED                  , VK_FORMAT_ETC2_R8G8B8A1_SRGB_BLOCK   };
        FormatTable[xbitmap::format::FORMAT_ETC2_8RGBA          ] = format  { VK_FORMAT_ETC2_R8G8B8A8_UNORM_BLOCK , VK_FORMAT_UNDEFINED                  , VK_FORMAT_ETC2_R8G8B8A8_SRGB_BLOCK   };

        FormatTable[xbitmap::format::FORMAT_BC1_4RGB            ] = format  { VK_FORMAT_BC1_RGB_UNORM_BLOCK       , VK_FORMAT_UNDEFINED                  , VK_FORMAT_BC1_RGB_SRGB_BLOCK         };
        FormatTable[xbitmap::format::FORMAT_BC1_4RGBA1          ] = format  { VK_FORMAT_BC1_RGBA_UNORM_BLOCK      , VK_FORMAT_UNDEFINED                  , VK_FORMAT_BC1_RGBA_SRGB_BLOCK        };
        FormatTable[xbitmap::format::FORMAT_BC2_8RGBA           ] = format  { VK_FORMAT_BC2_UNORM_BLOCK           , VK_FORMAT_UNDEFINED                  , VK_FORMAT_BC2_SRGB_BLOCK             };
        FormatTable[xbitmap::format::FORMAT_BC3_8RGBA           ] = format  { VK_FORMAT_BC3_UNORM_BLOCK           , VK_FORMAT_UNDEFINED                  , VK_FORMAT_BC3_SRGB_BLOCK             };
        FormatTable[xbitmap::format::FORMAT_BC4_4R              ] = format  { VK_FORMAT_BC4_UNORM_BLOCK           , VK_FORMAT_BC4_SNORM_BLOCK            , VK_FORMAT_UNDEFINED                  };
        FormatTable[xbitmap::format::FORMAT_BC5_8RG             ] = format  { VK_FORMAT_BC5_UNORM_BLOCK           , VK_FORMAT_BC5_SNORM_BLOCK            , VK_FORMAT_UNDEFINED                  };
        FormatTable[xbitmap::format::FORMAT_BC6H_8RGB_FLOAT     ] = format  { VK_FORMAT_BC6H_UFLOAT_BLOCK         , VK_FORMAT_BC6H_SFLOAT_BLOCK          , VK_FORMAT_UNDEFINED                  };
        FormatTable[xbitmap::format::FORMAT_BC7_8RGBA           ] = format  { VK_FORMAT_BC7_UNORM_BLOCK           , VK_FORMAT_UNDEFINED                  , VK_FORMAT_BC7_SRGB_BLOCK             };
        
        FormatTable[xbitmap::format::FORMAT_ASTC_4x4_8RGB       ] = format  { VK_FORMAT_ASTC_4x4_UNORM_BLOCK      , VK_FORMAT_UNDEFINED                  , VK_FORMAT_ASTC_4x4_SRGB_BLOCK        };               
        FormatTable[xbitmap::format::FORMAT_ASTC_5x4_6RGB       ] = format  { VK_FORMAT_ASTC_5x4_UNORM_BLOCK      , VK_FORMAT_UNDEFINED                  , VK_FORMAT_ASTC_5x4_SRGB_BLOCK        };               
        FormatTable[xbitmap::format::FORMAT_ASTC_5x5_5RGB       ] = format  { VK_FORMAT_ASTC_5x5_UNORM_BLOCK      , VK_FORMAT_UNDEFINED                  , VK_FORMAT_ASTC_5x5_SRGB_BLOCK        };               
        FormatTable[xbitmap::format::FORMAT_ASTC_6x5_4RGB       ] = format  { VK_FORMAT_ASTC_6x5_UNORM_BLOCK      , VK_FORMAT_UNDEFINED                  , VK_FORMAT_ASTC_6x5_SRGB_BLOCK        };               
        FormatTable[xbitmap::format::FORMAT_ASTC_6x6_4RGB       ] = format  { VK_FORMAT_ASTC_6x6_UNORM_BLOCK      , VK_FORMAT_UNDEFINED                  , VK_FORMAT_ASTC_6x6_SRGB_BLOCK        };               
        FormatTable[xbitmap::format::FORMAT_ASTC_8x5_3RGB       ] = format  { VK_FORMAT_ASTC_8x5_UNORM_BLOCK      , VK_FORMAT_UNDEFINED                  , VK_FORMAT_ASTC_8x5_SRGB_BLOCK        };               
        FormatTable[xbitmap::format::FORMAT_ASTC_8x6_3RGB       ] = format  { VK_FORMAT_ASTC_8x6_UNORM_BLOCK      , VK_FORMAT_UNDEFINED                  , VK_FORMAT_ASTC_8x6_SRGB_BLOCK        };               
        FormatTable[xbitmap::format::FORMAT_ASTC_8x8_2RGB       ] = format  { VK_FORMAT_ASTC_8x8_UNORM_BLOCK      , VK_FORMAT_UNDEFINED                  , VK_FORMAT_ASTC_8x8_SRGB_BLOCK        };               
        FormatTable[xbitmap::format::FORMAT_ASTC_10x5_3RGB      ] = format  { VK_FORMAT_ASTC_10x5_UNORM_BLOCK     , VK_FORMAT_UNDEFINED                  , VK_FORMAT_ASTC_10x5_SRGB_BLOCK       };               
        FormatTable[xbitmap::format::FORMAT_ASTC_10x6_2RGB      ] = format  { VK_FORMAT_ASTC_10x6_UNORM_BLOCK     , VK_FORMAT_UNDEFINED                  , VK_FORMAT_ASTC_10x6_SRGB_BLOCK       };               
        FormatTable[xbitmap::format::FORMAT_ASTC_10x8_2RGB      ] = format  { VK_FORMAT_ASTC_10x8_UNORM_BLOCK     , VK_FORMAT_UNDEFINED                  , VK_FORMAT_ASTC_10x8_SRGB_BLOCK       };               
        FormatTable[xbitmap::format::FORMAT_ASTC_10x10_1RGB     ] = format  { VK_FORMAT_ASTC_10x10_UNORM_BLOCK    , VK_FORMAT_UNDEFINED                  , VK_FORMAT_ASTC_10x10_SRGB_BLOCK      };               
        FormatTable[xbitmap::format::FORMAT_ASTC_12x10_1RGB     ] = format  { VK_FORMAT_ASTC_12x10_UNORM_BLOCK    , VK_FORMAT_UNDEFINED                  , VK_FORMAT_ASTC_12x10_SRGB_BLOCK      };               
        FormatTable[xbitmap::format::FORMAT_ASTC_12x12_1RGB     ] = format  { VK_FORMAT_ASTC_12x12_UNORM_BLOCK    , VK_FORMAT_UNDEFINED                  , VK_FORMAT_ASTC_12x12_SRGB_BLOCK      };               

        FormatTable[xbitmap::format::FORMAT_PVR1_2RGB           ] = format  { VK_FORMAT_UNDEFINED                 , VK_FORMAT_UNDEFINED                  , VK_FORMAT_UNDEFINED                  };               
        FormatTable[xbitmap::format::FORMAT_PVR1_2RGBA          ] = format  { VK_FORMAT_UNDEFINED                 , VK_FORMAT_UNDEFINED                  , VK_FORMAT_UNDEFINED                  };
        FormatTable[xbitmap::format::FORMAT_PVR1_4RGB           ] = format  { VK_FORMAT_UNDEFINED                 , VK_FORMAT_UNDEFINED                  , VK_FORMAT_UNDEFINED                  };
        FormatTable[xbitmap::format::FORMAT_PVR1_4RGBA          ] = format  { VK_FORMAT_UNDEFINED                 , VK_FORMAT_UNDEFINED                  , VK_FORMAT_UNDEFINED                  };
        FormatTable[xbitmap::format::FORMAT_PVR2_2RGBA          ] = format  { VK_FORMAT_UNDEFINED                 , VK_FORMAT_UNDEFINED                  , VK_FORMAT_UNDEFINED                  };
        FormatTable[xbitmap::format::FORMAT_PVR2_4RGBA          ] = format  { VK_FORMAT_UNDEFINED                 , VK_FORMAT_UNDEFINED                  , VK_FORMAT_UNDEFINED                  };

        FormatTable[xbitmap::format::FORMAT_D24S8_FLOAT         ] = format  { VK_FORMAT_D32_SFLOAT_S8_UINT        , VK_FORMAT_D32_SFLOAT_S8_UINT         , VK_FORMAT_UNDEFINED                  };
        FormatTable[xbitmap::format::FORMAT_D24S8               ] = format  { VK_FORMAT_D24_UNORM_S8_UINT         , VK_FORMAT_UNDEFINED                  , VK_FORMAT_UNDEFINED                  };
        FormatTable[xbitmap::format::FORMAT_R8                  ] = format  { VK_FORMAT_R8_UNORM                  , VK_FORMAT_R8_SNORM                   , VK_FORMAT_R8_SRGB                    };
        FormatTable[xbitmap::format::FORMAT_R32                 ] = format  { VK_FORMAT_R32_UINT                  , VK_FORMAT_R32_SINT                   , VK_FORMAT_UNDEFINED                  };
        FormatTable[xbitmap::format::FORMAT_R8G8                ] = format  { VK_FORMAT_R8G8_UNORM                , VK_FORMAT_R8G8_SNORM                 , VK_FORMAT_R8G8_SRGB                  };
        FormatTable[xbitmap::format::FORMAT_R16G16B16A16        ] = format  { VK_FORMAT_R16G16B16A16_UNORM        , VK_FORMAT_R16G16B16A16_SNORM         , VK_FORMAT_UNDEFINED                  };
        FormatTable[xbitmap::format::FORMAT_R16G16B16A16_FLOAT  ] = format  { VK_FORMAT_R16G16B16A16_SFLOAT       , VK_FORMAT_R16G16B16A16_SFLOAT        , VK_FORMAT_UNDEFINED                  };
        FormatTable[xbitmap::format::FORMAT_A2R10G10B10         ] = format  { VK_FORMAT_A2R10G10B10_UNORM_PACK32  , VK_FORMAT_A2R10G10B10_SNORM_PACK32   , VK_FORMAT_UNDEFINED                  };
        FormatTable[xbitmap::format::FORMAT_B11G11R11_FLOAT     ] = format  { VK_FORMAT_B10G11R11_UFLOAT_PACK32   , VK_FORMAT_UNDEFINED                  , VK_FORMAT_UNDEFINED                  };

        return FormatTable;
    }();  

    const format& Entry = FormatTable[Fmt];
    if( bLinear )
    {
        if( bSinged ) return Entry.m_VKFormat_LinearSigned;
        return Entry.m_VKFormat_LinearUnsigned;
    }

    return Entry.m_VKFormat_SRGB;
}

//-------------------------------------------------------------------------------                        
// Create an image memory barrier for changing the layout of
// an image and put it into an active command buffer
void texture::setImageLayout(
    VkCommandBuffer         CmdBuffer, 
    VkImage                 image, 
    VkImageAspectFlags      aspectMask, 
    VkImageLayout           oldImageLayout, 
    VkImageLayout           newImageLayout,
    VkImageSubresourceRange subresourceRange ) noexcept
{
// Create an image barrier object
        VkImageMemoryBarrier imageMemoryBarrier = {};
        imageMemoryBarrier.sType                            = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
        imageMemoryBarrier.pNext                            = nullptr;
        imageMemoryBarrier.srcQueueFamilyIndex              = VK_QUEUE_FAMILY_IGNORED;
        imageMemoryBarrier.dstQueueFamilyIndex              = VK_QUEUE_FAMILY_IGNORED;
        imageMemoryBarrier.oldLayout                        = oldImageLayout;
        imageMemoryBarrier.newLayout                        = newImageLayout;
        imageMemoryBarrier.image                            = image;
        imageMemoryBarrier.subresourceRange                 = subresourceRange;

        // Only sets masks for layouts used in this example
        // For a more complete version that can be used with other layouts see vkTools::setImageLayout

        // Source layouts (old)
        switch (oldImageLayout)
        {
        case VK_IMAGE_LAYOUT_UNDEFINED:
            // Only valid as initial layout, memory contents are not preserved
            // Can be accessed directly, no source dependency required
            imageMemoryBarrier.srcAccessMask = 0;
            break;
        case VK_IMAGE_LAYOUT_PREINITIALIZED:
            // Only valid as initial layout for linear images, preserves memory contents
            // Make sure host writes to the image have been finished
            imageMemoryBarrier.srcAccessMask = VK_ACCESS_HOST_WRITE_BIT;
            break;
        case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:
            // Old layout is transfer destination
            // Make sure any writes to the image have been finished
            imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
            break;
        }
        
        // Target layouts (new)
        switch (newImageLayout)
        {
        case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL:
            // Transfer source (copy, blit)
            // Make sure any reads from the image have been finished
            imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
            break;
        case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:
            // Transfer destination (copy, blit)
            // Make sure any writes to the image have been finished
            imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
            break;
        case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL:
            // Shader read (sampler, input attachment)
            imageMemoryBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
            break;
        }

        // Put barrier on top of pipeline
        VkPipelineStageFlags srcStageFlags = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        VkPipelineStageFlags destStageFlags = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;

        // Put barrier inside setup command buffer
        vkCmdPipelineBarrier(
            CmdBuffer,
            srcStageFlags, 
            destStageFlags, 
            VK_FLAGS_NONE, 
            0, nullptr,
            0, nullptr,
            1, &imageMemoryBarrier);
}

//-------------------------------------------------------------------------------                        

void texture::setupFrom( VkCommandBuffer CmdBuffer, const xbitmap& Bitmap ) noexcept
{
    VkFormatProperties  VKFormatProps;
    const VkFormat      VKFormat            = getVKFormat( Bitmap.getFormat(), Bitmap.isLinear(), Bitmap.isSigned() );
    VkBool32            forceLinearTiling   = false;
    VkImageLayout       VKImageLayout       { VK_IMAGE_LAYOUT_UNDEFINED };

    x_assert( VKFormat != VK_FORMAT_UNDEFINED );
    x_assert( CmdBuffer );

    m_Width     = Bitmap.getWidth();
    m_Height    = Bitmap.getHeight();
    m_nMips     = Bitmap.getMipCount();

    // Get device properites for the requested texture format
    vkGetPhysicalDeviceFormatProperties( m_Device.getVKPhysicalDevice(), VKFormat, &VKFormatProps );

    // Only use linear tiling if requested (and supported by the device)
    // Support for linear tiling is mostly limited, so prefer to use
    // optimal tiling instead
    // On most implementations linear tiling will only support a very
    // limited amount of formats and features (mip maps, cubemaps, arrays, etc.)
    const VkBool32 useStaging = [&] 
    {
        // Only use linear tiling if forced
        if (forceLinearTiling)
        {
            // Don't use linear if format is not supported for (linear) shader sampling
            return !( VKFormatProps.linearTilingFeatures & VK_FORMAT_FEATURE_SAMPLED_IMAGE_BIT );
        }

        return true;
    }();

    VkMemoryAllocateInfo memAllocInfo {};
    memAllocInfo.sType              = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    memAllocInfo.pNext              = nullptr;
    memAllocInfo.allocationSize     = 0;
    memAllocInfo.memoryTypeIndex    = 0;

    VkMemoryRequirements memReqs = {};

    if (useStaging)
    {
        // Create a host-visible staging buffer that contains the raw image data
        VkBuffer        stagingBuffer;
        VkDeviceMemory  stagingMemory;

        VkBufferCreateInfo bufferCreateInfo = {};

        bufferCreateInfo.sType          = VkStructureType::VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        bufferCreateInfo.pNext          = nullptr;
        bufferCreateInfo.size           = Bitmap.getFrameSize();
        // This buffer is used as a transfer source for the buffer copy
        bufferCreateInfo.usage          = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
        bufferCreateInfo.sharingMode    = VK_SHARING_MODE_EXCLUSIVE;
        bufferCreateInfo.flags          = 0;

            
        auto VKErr = vkCreateBuffer( m_Device.getVKDevice(), &bufferCreateInfo, nullptr, &stagingBuffer );
        x_assert( !VKErr );

        // Get memory requirements for the staging buffer (alignment, memory type bits)
        vkGetBufferMemoryRequirements( m_Device.getVKDevice(), stagingBuffer, &memReqs );

        memAllocInfo.allocationSize = memReqs.size;
        // Get memory type index for a host visible buffer
        m_Device.getMemoryType( memReqs.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, memAllocInfo.memoryTypeIndex );

        VKErr = vkAllocateMemory( m_Device.getVKDevice(), &memAllocInfo, nullptr, &stagingMemory);
        x_assert( !VKErr );
        VKErr = vkBindBufferMemory(m_Device.getVKDevice(), stagingBuffer, stagingMemory, 0 );
        x_assert( !VKErr );

        // Copy texture data into staging buffer
        uint8_t *data;
        VKErr = vkMapMemory( m_Device.getVKDevice(), stagingMemory, 0, memReqs.size, 0, (void **)&data);
        x_assert( !VKErr );

        memcpy(data, Bitmap.getMip<xbyte>(0), Bitmap.getFrameSize() );
        vkUnmapMemory( m_Device.getVKDevice(), stagingMemory);

        // Setup buffer copy regions for each mip level
        xvector<VkBufferImageCopy> bufferCopyRegions;
        uint32_t offset = 0;

        // Load mip levels into linear textures that are used to copy from
        s32 Width   = m_Width;
        s32 Height  = m_Height;

        for (int i = 0; i < m_nMips; i++)
        {
            VkBufferImageCopy& bufferCopyRegion = bufferCopyRegions.append();
            bufferCopyRegion.imageSubresource.aspectMask        = VK_IMAGE_ASPECT_COLOR_BIT;
            bufferCopyRegion.imageSubresource.mipLevel          = i;
            bufferCopyRegion.imageSubresource.baseArrayLayer    = 0;
            bufferCopyRegion.imageSubresource.layerCount        = 1;
            bufferCopyRegion.imageExtent.width                  = Width;      Width   >>= 1;
            bufferCopyRegion.imageExtent.height                 = Height;     Height  >>= 1;
            bufferCopyRegion.imageExtent.depth                  = 1;
            bufferCopyRegion.bufferOffset                       = offset;

            offset += (uint32_t)Bitmap.getMipSize(i);
        }

        // Create optimal tiled target image
        VkImageCreateInfo imageCreateInfo = {};
        imageCreateInfo.sType               = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
        imageCreateInfo.pNext               = nullptr;
        imageCreateInfo.imageType           = VK_IMAGE_TYPE_2D;
        imageCreateInfo.format              = VKFormat;
        imageCreateInfo.mipLevels           = m_nMips;
        imageCreateInfo.arrayLayers         = 1;
        imageCreateInfo.samples             = VK_SAMPLE_COUNT_1_BIT;
        imageCreateInfo.tiling              = VK_IMAGE_TILING_OPTIMAL;
        imageCreateInfo.usage               = VK_IMAGE_USAGE_SAMPLED_BIT;
        imageCreateInfo.sharingMode         = VK_SHARING_MODE_EXCLUSIVE;
        // Set initial layout of the image to undefined
        imageCreateInfo.initialLayout       = VK_IMAGE_LAYOUT_UNDEFINED;
        imageCreateInfo.extent              = { static_cast<uint32_t>(m_Width), 
                                                static_cast<uint32_t>(m_Height), 1 };
        imageCreateInfo.usage               = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;

        VKErr = vkCreateImage( m_Device.getVKDevice(), &imageCreateInfo, nullptr, &m_Image );
        x_assert(!VKErr);

        vkGetImageMemoryRequirements (m_Device.getVKDevice(), m_Image, &memReqs);

        memAllocInfo.allocationSize = memReqs.size;
        m_Device.getMemoryType(memReqs.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,memAllocInfo.memoryTypeIndex);

        VKErr = vkAllocateMemory( m_Device.getVKDevice(), &memAllocInfo, nullptr, &m_DeviceMemory);
        x_assert(!VKErr);
        VKErr = vkBindImageMemory( m_Device.getVKDevice(), m_Image, m_DeviceMemory, 0);
        x_assert(!VKErr);

        //VkCommandBuffer copyCmd = VulkanExampleBase::createCommandBuffer(VK_COMMAND_BUFFER_LEVEL_PRIMARY, true);

        // Image barrier for optimal image

        // The sub resource range describes the regions of the image we will be transition
        VkImageSubresourceRange subresourceRange = {};
        // Image only contains color data
        subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        // Start at first mip level
        subresourceRange.baseMipLevel = 0;
        // We will transition on all mip levels
        subresourceRange.levelCount = m_nMips;
        // The 2D texture only has one layer
        subresourceRange.layerCount = 1;

        // Optimal image will be used as destination for the copy, so we must transfer from our
        // initial undefined image layout to the transfer destination layout
        setImageLayout(
            CmdBuffer,
            m_Image,
            VK_IMAGE_ASPECT_COLOR_BIT,
            VK_IMAGE_LAYOUT_UNDEFINED,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            subresourceRange);

        // Copy mip levels from staging buffer
        vkCmdCopyBufferToImage(
            CmdBuffer,
            stagingBuffer,
            m_Image,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            static_cast<uint32_t>(bufferCopyRegions.getCount()),
            bufferCopyRegions);

        // Change texture image layout to shader read after all mip levels have been copied
        VKImageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        setImageLayout(
            CmdBuffer,
            m_Image,
            VK_IMAGE_ASPECT_COLOR_BIT,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            VKImageLayout,
            subresourceRange);

        // Clean up staging resources
        m_Device.FlushVKSetupCommandBuffer();
        m_Device.CreateVKSetupCommandBuffer();
    }
    else
    {
        // Prefer using optimal tiling, as linear tiling 
        // may support only a small set of features 
        // depending on implementation (e.g. no mip maps, only one layer, etc.)

        VkImage mappableImage;
        VkDeviceMemory mappableMemory;

        // Load mip map level 0 to linear tiling image
        VkImageCreateInfo imageCreateInfo = {};
        imageCreateInfo.sType               = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
        imageCreateInfo.pNext               = nullptr;
        imageCreateInfo.imageType           = VK_IMAGE_TYPE_2D;
        imageCreateInfo.format              = VKFormat;
        imageCreateInfo.mipLevels           = 1;
        imageCreateInfo.arrayLayers         = 1;
        imageCreateInfo.samples             = VK_SAMPLE_COUNT_1_BIT;
        imageCreateInfo.tiling              = VK_IMAGE_TILING_LINEAR;
        imageCreateInfo.usage               = VK_IMAGE_USAGE_SAMPLED_BIT;
        imageCreateInfo.sharingMode         = VK_SHARING_MODE_EXCLUSIVE;
        imageCreateInfo.initialLayout       = VK_IMAGE_LAYOUT_PREINITIALIZED;
        imageCreateInfo.extent              = { static_cast<uint32_t>(m_Width), 
                                                static_cast<uint32_t>(m_Height), 1 };
        auto VKErr = vkCreateImage( m_Device.getVKDevice(), &imageCreateInfo, nullptr, &mappableImage);
        x_assert( !VKErr );

        // Get memory requirements for this image 
        // like size and alignment
        vkGetImageMemoryRequirements( m_Device.getVKDevice(), mappableImage, &memReqs);
        // Set memory allocation size to required memory size
        memAllocInfo.allocationSize = memReqs.size;

        // Get memory type that can be mapped to host memory
        m_Device.getMemoryType(memReqs.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, memAllocInfo.memoryTypeIndex);

        // Allocate host memory
        VKErr = vkAllocateMemory(m_Device.getVKDevice(), &memAllocInfo, nullptr, &mappableMemory);
        x_assert( !VKErr );

        // Bind allocated image for use
        VKErr = vkBindImageMemory(m_Device.getVKDevice(), mappableImage, mappableMemory, 0);
        x_assert( !VKErr );

        // Get sub resource layout
        // Mip map count, array layer, etc.
        VkImageSubresource subRes = {};
        subRes.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;

        VkSubresourceLayout subResLayout;
        void *data;

        // Get sub resources layout 
        // Includes row pitch, size offsets, etc.
        vkGetImageSubresourceLayout( m_Device.getVKDevice(), mappableImage, &subRes, &subResLayout);

        // Map image memory
        VKErr = vkMapMemory( m_Device.getVKDevice(), mappableMemory, 0, memReqs.size, 0, &data);
        x_assert( !VKErr );

        // Copy image data into memory
        memcpy(data, Bitmap.getMip<xbyte>(subRes.mipLevel), Bitmap.getMipSize(subRes.mipLevel) );

        vkUnmapMemory( m_Device.getVKDevice(), mappableMemory);

        // Linear tiled images don't need to be staged
        // and can be directly used as textures
        m_Image         = mappableImage;
        m_DeviceMemory  = mappableMemory;
        VKImageLayout   = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
            
        //VkCommandBuffer copyCmd = VulkanExampleBase::createCommandBuffer(VK_COMMAND_BUFFER_LEVEL_PRIMARY, true);

        // Setup image memory barrier transfer image to shader read layout

        // The sub resource range describes the regions of the image we will be transition
        VkImageSubresourceRange subresourceRange = {};
        // Image only contains color data
        subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        // Start at first mip level
        subresourceRange.baseMipLevel = 0;
        // Only one mip level, most implementations won't support more for linear tiled images
        subresourceRange.levelCount = 1;
        // The 2D texture only has one layer
        subresourceRange.layerCount = 1;

        setImageLayout(
            CmdBuffer, 
            m_Image,
            VK_IMAGE_ASPECT_COLOR_BIT, 
            VK_IMAGE_LAYOUT_PREINITIALIZED,
            VKImageLayout,
            subresourceRange);

        m_Device.FlushVKSetupCommandBuffer();
        m_Device.CreateVKSetupCommandBuffer();
    }

    // Create image view
    // Textures are not directly accessed by the shaders and
    // are abstracted by image views containing additional
    // information and sub resource ranges
    VkImageViewCreateInfo view = {};
    view.sType                              = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    view.pNext                              = nullptr;
    view.image                              = VK_NULL_HANDLE;
    view.viewType                           = VK_IMAGE_VIEW_TYPE_2D;
    view.format                             = VKFormat;
    view.components                         = { VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A };
    view.subresourceRange.aspectMask        = VK_IMAGE_ASPECT_COLOR_BIT;
    view.subresourceRange.baseMipLevel      = 0;
    view.subresourceRange.baseArrayLayer    = 0;
    view.subresourceRange.layerCount        = 1;

    // Linear tiling usually won't support mip maps
    // Only set mip map count if optimal tiling is used
    view.subresourceRange.levelCount        = (useStaging) ? m_nMips : 1;
    view.image                              = m_Image;
    auto VKErr = vkCreateImageView( m_Device.getVKDevice(), &view, nullptr, &m_View );
    x_assert( !VKErr );
}

//-------------------------------------------------------------------------------                        

void texture::Release( void ) noexcept
{

}


//------------------------------------------------------------------------------------
// DONE WITH NAMESPACE
//------------------------------------------------------------------------------------
}



