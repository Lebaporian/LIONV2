//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

class sampler final : public eng_sampler
{
public:

//    inline      static  auto            ComputeHash         ( const setup& Setup )                          noexcept { return x_memCRC32( xbuffer_view<xbyte>{ reinterpret_cast<xbyte*>(const_cast<setup*>(&Setup)), sizeof(Setup) } ); }
//    constexpr           auto            getHash             ( void )                                const   noexcept { return m_Handle;  }
    constexpr           VkSampler               getVKSampler            ( void )                                    const   noexcept { return m_VKSampler; }
                        void                    Initialize              ( const setup& Setup, const device& Device )        noexcept;
    virtual             void                    Release                 ( void )                                            noexcept override;
    inline              VkDescriptorSet         getDrawDescriptorSet    ( const int iBind )                         const   noexcept { return HandleToObject<texture>( m_hTexture ).getDrawDescriptorSet( getDescriptorImageInfo(), iBind ); }
                        VkDescriptorImageInfo   getDescriptorImageInfo  ( void )                                    const   noexcept  
    {
        // Image descriptor for the color map texture
        VkDescriptorImageInfo DescriptorImageInfo = {};
        auto& Texture = HandleToObject<texture>( m_hTexture );
        DescriptorImageInfo.imageLayout     = VK_IMAGE_LAYOUT_GENERAL;
        DescriptorImageInfo.sampler         = m_VKSampler;
        DescriptorImageInfo.imageView       = Texture.getView();
        return DescriptorImageInfo;
    }


    constexpr   static  auto            ConvertSampler      ( const mipmap_sampler MipmapSampler )              noexcept { return ( MipmapSampler == MIPMAP_SAMPLER_LINEAR ) ? VK_FILTER_LINEAR : VK_FILTER_NEAREST; }
    constexpr   static  auto            ConvertMipmapMode   ( const mipmap_mode MipmapMode )                    noexcept { return ( MipmapMode == MIPMAP_MODE_LINEAR ) ? VK_SAMPLER_MIPMAP_MODE_LINEAR : VK_SAMPLER_MIPMAP_MODE_NEAREST; }
    inline      static  auto            ConvertAddressMode  ( const address_mode AddressMode )                  noexcept
    {
        switch( AddressMode )
        {
            case ADDRESS_MODE_WRAP:         return VK_SAMPLER_ADDRESS_MODE_REPEAT;
            case ADDRESS_MODE_CLAMP:        return VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
            case ADDRESS_MODE_BORDER:       return VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
            case ADDRESS_MODE_MIRROR:       return VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT;
            case ADDRESS_MODE_MIRROR_CLAMP: return VK_SAMPLER_ADDRESS_MODE_MIRROR_CLAMP_TO_EDGE;
            default: x_assert( false );
        }
        return VK_SAMPLER_ADDRESS_MODE_REPEAT;
    }
  
protected:

    VkSampler       m_VKSampler         { nullptr };
    u64             m_Handle            { 0 };
};

