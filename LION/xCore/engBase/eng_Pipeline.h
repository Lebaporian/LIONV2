//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

class eng_pipeline : public eng_base
{
public:

    using handle    = x_ll_share_ref<eng_pipeline>;

public:

    struct primitive
    {
        enum class raster : u8
        {
            FILL,
            WIRELINE,
            POINT,
            ENUM_COUNT
        };

        enum class front_face : u8
        {
            COUNTER_CLOCKWISE,
            CLOCKWISE,
            ENUM_COUNT
        };

        enum class cull  : u8
        {
            NONE,        
            FRONT,
            BACK,
            ALL,     
            ENUM_COUNT     
        };

        constexpr primitive( void ) :
            m_LineWidth                 { 1 },
            m_Raster                    { raster::FILL },
            m_FrontFace                 { front_face::COUNTER_CLOCKWISE },
            m_Cull                      { cull::BACK },
            m_bRasterizerDiscardEnable  {} {}



        float           m_LineWidth;                        // is the width of rasterized line segments

        raster          m_Raster                    :  x_Log2IntRoundUp( static_cast<int>(raster::ENUM_COUNT) - 1 );
        front_face      m_FrontFace                 :  x_Log2IntRoundUp( static_cast<int>(front_face::ENUM_COUNT)  - 1 );
        cull            m_Cull                      :  x_Log2IntRoundUp( static_cast<int>(cull::ENUM_COUNT)  - 1 );

        bool            m_bRasterizerDiscardEnable  : 1;    // Controls whether primitives are discarded immediately before the rasterization stage.
    };
   
    // Blend Equation: Screen.color = ( SourceColor.rgb * m_ColorSrcFactor ) m_ColorOperation ( DestinationColor.rgb * m_ColorDstFactor )
    // Blend Equation: Screen.alpha = ( SourceColor.a   * m_AlphaSrcFactor ) m_AlphaOperation ( DestinationColor.a   * m_AlphaDstFactor )
    struct blend
    {
        enum class factor : u8
        {
            ZERO,
            ONE,
            SRC_COLOR,
            ONE_MINUS_SRC_COLOR,
            DST_COLOR,
            ONE_MINUS_DST_COLOR,
            SRC_ALPHA,
            ONE_MINUS_SRC_ALPHA,
            DST_ALPHA,
            ONE_MINUS_DST_ALPHA,
            CONSTANT_COLOR,
            ONE_MINUS_CONSTANT_COLOR,
            CONSTANT_ALPHA,
            ONE_MINUS_CONSTANT_ALPHA,
            SRC_ALPHA_SATURATE,
            SRC1_COLOR,
            ONE_MINUS_SRC1_COLOR,
            SRC1_ALPHA,
            ONE_MINUS_SRC1_ALPHA,
            ENUM_COUNT
        };

        enum class op : u8
        {
            ADD,
            SUBTRACT,
            REVERSE_SUBTRACT,
            MIN,
            MAX,
            ENUM_COUNT
        };

        constexpr blend ( void ) noexcept :
            m_ColorWriteMask    { 0xf },
            m_ColorSrcFactor    { factor::ONE },
            m_ColorDstFactor    { factor::ZERO },
            m_ColorOperation    { op::ADD },
            m_AlphaSrcFactor    { factor::ONE },
            m_AlphaDstFactor    { factor::ZERO },
            m_AlphaOperation    { op::ADD }, 
            m_bEnable           { false } {}

        inline      void    setupBlendOff               ( void ) noexcept;
        inline      void    setupAlphaOriginal          ( void ) noexcept;
        inline      void    setupAlphaPreMultiply       ( void ) noexcept;
        inline      void    setupMuliply                ( void ) noexcept;
        inline      void    setupAdd                    ( void ) noexcept;
        inline      void    setupSubSrcFromDest         ( void ) noexcept;
        inline      void    setupSubDestFromSrc         ( void ) noexcept;

        u8          m_ColorWriteMask    { 0xff };

        factor      m_ColorSrcFactor    : x_Log2IntRoundUp( static_cast<int>(factor::ENUM_COUNT) - 1 );
        factor      m_ColorDstFactor    : x_Log2IntRoundUp( static_cast<int>(factor::ENUM_COUNT) - 1 );
        op          m_ColorOperation    : x_Log2IntRoundUp( static_cast<int>(op::ENUM_COUNT)     - 1 );

        factor      m_AlphaSrcFactor    : x_Log2IntRoundUp( static_cast<int>(factor::ENUM_COUNT) - 1 );
        factor      m_AlphaDstFactor    : x_Log2IntRoundUp( static_cast<int>(factor::ENUM_COUNT) - 1 );
        op          m_AlphaOperation    : x_Log2IntRoundUp( static_cast<int>(op::ENUM_COUNT)     - 1 );

        bool        m_bEnable           : 1;
    };

    struct depth_stencil
    {
        enum class depth_compare : u8
        {
            LESS,
            LESS_OR_EQUAL,
            GREATER,
            NOT_EQUAL,
            GREATER_OR_EQUAL,
            EQUAL,
            NEVER,
            ALWAYS,
            ENUM_COUNT
         };

        enum class stencil_op : u8
        {
            KEEP,
            ZERO,
            REPLACE,
            INCREMENT_AND_CLAMP,
            DECREMENT_AND_CLAMP,
            INVERT,
            INCREMENT_AND_WRAP,
            DECREMENT_AND_WRAP,
            ENUM_COUNT
        };

        struct stencil_operation_state 
        {
            constexpr stencil_operation_state( void ) noexcept :
                m_FailOp        { stencil_op::KEEP },
                m_PassOp        { stencil_op::KEEP },
                m_DepthFailOp   { stencil_op::KEEP },
                m_CompareOp     { depth_compare::ALWAYS } {};

            u32             m_CompareMask       { ~0u };
            u32             m_WriteMask         { ~0u };
            u32             m_Reference         {  0u };         // is an integer reference value that is used in the unsigned stencil comparison
            stencil_op      m_FailOp            : x_Log2IntRoundUp( static_cast<int>(stencil_op::ENUM_COUNT)    - 1 );
            stencil_op      m_PassOp            : x_Log2IntRoundUp( static_cast<int>(stencil_op::ENUM_COUNT)    - 1 );
            stencil_op      m_DepthFailOp       : x_Log2IntRoundUp( static_cast<int>(stencil_op::ENUM_COUNT)    - 1 );
            depth_compare   m_CompareOp         : x_Log2IntRoundUp( static_cast<int>(depth_compare::ENUM_COUNT) - 1 );
        };

        constexpr depth_stencil( void ) noexcept :
            m_DepthCompare              { depth_compare::LESS_OR_EQUAL  },
            m_bDepthTestEnable          { true  },
            m_bDepthWriteEnable         { true  },
            m_bDepthClampEnable         { false },
            m_bDepthBiasEnable          { false },
            m_bDepthBoundsTestEnable    { false },
            m_StencilTestEnable         { false } {}

        float                       m_DepthMaxBounds            { 0 };
        float                       m_DepthMinBounds            { 0 };

        float                       m_DepthBiasConstantFactor   { 0 };      // is a scalar factor controlling the constant depth value added to each fragment
        float                       m_DepthBiasClamp            { 0 };      // is the maximum (or minimum) depth bias of a fragment
        float                       m_DepthBiasSlopeFactor      { 0 };      // is a scalar factor applied to a fragment�s slope in depth bias calculations

        stencil_operation_state     m_StencilFrontFace          {};
        stencil_operation_state     m_StencilBackFace           {};
        depth_compare               m_DepthCompare              : x_Log2IntRoundUp( static_cast<int>(depth_compare::ENUM_COUNT)    - 1 );

        bool                        m_bDepthTestEnable          : 1;
        bool                        m_bDepthWriteEnable         : 1;
        bool                        m_bDepthClampEnable         : 1;                        // Clamp the fragment�s depth values instead of clipping by FarZ
        bool                        m_bDepthBiasEnable          : 1;
        bool                        m_bDepthBoundsTestEnable    : 1;

        bool                        m_StencilTestEnable         : 1;
    };

    struct info
    {
        depth_stencil               m_DepthStencil      {};
        blend                       m_Blend             {};
        primitive                   m_Primitive         {};
    };

    struct setup : public info
    {
    };

public:

    const info&     getInfo( void ) const noexcept { return m_Info; }

protected:

    info            m_Info    {};
};
