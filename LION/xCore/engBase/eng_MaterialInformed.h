//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

class eng_material_informed : public eng_base
{
public:

    using handle    = x_ll_share_ref<eng_material_informed>;

public:

    struct sampler_binding
    {
        int                                     m_iBind         {};         //  register number where this texture binds
        eng_sampler::handle                     m_hSampler      {};         //  the sampler use in this register
    };

    struct setup
    {
        setup( const xbuffer_view<sampler_binding>& Bindings ) :  
            m_SamplerBindings{ Bindings } {}

        const xbuffer_view<sampler_binding>&    m_SamplerBindings;
        eng_material_type::handle               m_hMaterialType;
    };

protected:

    eng_material_type::handle               m_hMaterialType     {};
    xarray<sampler_binding, 16>             m_SamplerBinding    {};
    xarray<eng_uniform_stream::handle, 16>  m_hUniforms         {};         // Pool of handles for all the types of uniforms in proper order (Vert, geom, Frag)
    int                                     m_nVertexUniforms   {0};        
    int                                     m_nGeometricUniforms{0};         
    int                                     m_nFragmentUniforms {0};
};
