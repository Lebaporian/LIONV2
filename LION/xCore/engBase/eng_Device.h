//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

class eng_device : public eng_base
{
public:

    enum errors : u8
    {
        ERR_OK,
        ERR_FAILURE,
    };

    using err       = x_err<errors>;
    using handle    = x_ll_share_ref<eng_device>;

    enum device_type : u8
    {
        DEVICE_TYPE_RENDER,
        DEVICE_TYPE_COMPUTE_ONLY,
        DEVICE_TYPE_COPY,
        DEVICE_TYPE_COUNT,
        DEVICE_TYPE_DEFAULT = DEVICE_TYPE_RENDER
    };

    struct setup
    {
        device_type     m_DeviceType    { DEVICE_TYPE_DEFAULT };
    };

public:

    virtual     err                 CreateTexture           ( eng_texture::handle&              hTexture,       const xbitmap&                      Bitmap )    noexcept = 0;
    virtual     err                 CreatePipeline          ( eng_pipeline::handle&             hPipeline,      const eng_pipeline::setup&          Setup )     noexcept = 0;
    virtual     err                 CreateSampler           ( eng_sampler::handle&              hSampler,       const eng_sampler::setup&           Setup )     noexcept = 0;
    virtual     err                 CreateMaterialType      ( eng_material_type::handle&        hMaterial,      const eng_material_type::setup&     Setup )     noexcept = 0;
    virtual     err                 CreateMaterialInformed  ( eng_material_informed::handle&    hMaterial,      const eng_material_informed::setup& Setup )     noexcept = 0;
    virtual     err                 CreateVertexShader      ( eng_shader_vert::handle&          hShaderVert,    const eng_shader_vert::setup&       Setup )     noexcept = 0;
    virtual     err                 CreateFragmentShader    ( eng_shader_frag::handle&          hShaderFrag,    const eng_shader_frag::setup&       Setup )     noexcept = 0;
    virtual     err                 CreateGeometryShader    ( eng_shader_geom::handle&          hShaderGeom,    const eng_shader_geom::setup&       Setup )     noexcept = 0;
    virtual     err                 CreateUniformStream     ( eng_uniform_stream::handle&       hUniformStream, const eng_uniform_stream::setup&    Setup )     noexcept = 0;
    virtual     device_type         getDeviceType           ( void ) const                                                                                      noexcept = 0;
};


