//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

class eng_draw : public eng_base
{
public:

    struct vertex
    {
        constexpr               vertex      ( void ) = default;
        constexpr               vertex      ( float X, float Y, float Z, float U, float V, xcolor Color )   noexcept;
        constexpr               vertex      ( const xvector3d& Pos, const xvector2& UV, xcolor Color )      noexcept;
        constexpr               vertex      ( const xvector3d& Pos, float U, float V, xcolor Color )        noexcept;
        constexpr               vertex      ( const xvector3d& Pos, xcolor Color )                          noexcept;
        constexpr               vertex      ( const xvector3d& Pos )                                        noexcept;
        constexpr               vertex      ( float X, float Y, float Z )                                   noexcept;
        constexpr               vertex      ( float X, float Y, float Z, xcolor Color )                     noexcept;

        inline      void        setup       ( const xvector3d& Pos, const xvector2& UV, xcolor Color )      noexcept;
        inline      void        setup       ( const xvector3d& Pos, f32 U, f32 V, xcolor Color )            noexcept;
        inline      void        setup       ( const xvector3d& Pos, xcolor Color )                          noexcept;
        inline      void        setup       ( const xvector3d& Pos )                                        noexcept;
        inline      void        setup       ( float X, float Y, float Z )                                   noexcept;
        inline      void        setup       ( float X, float Y, float Z, xcolor Color )                     noexcept;
        inline      void        setup       ( float X, float Y, float Z, float U, float V, xcolor Color )   noexcept;

        xvector3d   m_Position  {};
        xvector2    m_UV        {};
        xcolor      m_Color     {};
    };

    // What type of blending you would like to have
    // Reference material: https://developer.nvidia.com/content/alpha-blending-pre-or-not-pre
    enum blend : u8
    {
        BLEND_OFF,                  // Default: No blending modes enable
        BLEND_ALPHA_ORIGINAL,       // SRC.color * SRC.a + DST.Color * (1 - SRC.a) --- This is the standard alpha blend which is wrong for many reasons         
        BLEND_ALPHA_PREMULTIPLY,    // SRC.color + DST.Color * (1 - SRC.a) --- This is the modern form for alpha blending and requires the color to be premultiply by the alpha      
        BLEND_MUL,                  // SRC.color * DST.Color
        BLEND_ADD,                  // SRC.color + DST.Color
        BLEND_SUB_SRC_FROM_DST,     // DST.Color - SRC.color
        BLEND_SUB_DST_FROM_SRC,     // SRC.color - DST.Color
        BLEND_COUNT
    };

    // Choose one
    enum zbuffer : u8
    {
        ZBUFFER_ON,                 // Default: Use the ZBuffer
        ZBUFFER_OFF,                // Ignore the ZBuffer
        ZBUFFER_READ_ONLY,          // Read but dont write to the Zbuffer
        ZBUFFER_COUNT
    };

    // Choose one
    enum raster : u8
    {
        RASTER_SOLID,               // Default: Renders the primitives as solid       
        RASTER_WIRE_FRAME,          // Renders the primitives in wire-frame
        RASTER_COUNT
    };

    // Choose one
    enum cull : u8
    {
        CULL_BACK,                  // Default: Only CCW wise triangle will be render (front facing)
        CULL_FRONT,                 // Only CW wise triangle will be render  (back facing)
        CULL_OFF,                   // Render all triangles independently of where they are facing
        CULL_COUNT
    };

    // Choose one
    enum misc : u8
    {
        MISC_OFF,                   // Default: Does nothing
        MISC_FLUSH,                 // Forces a flush
        MISC_CUSTOM,                // Ignores seting modes
        MISC_COUNT
    };

    // pipeline that the draw will use
    X_DEFBITS( pipeline,
               u16,
               0x00000000,
               X_DEFBITS_ARG( blend,        BLEND,          x_Log2IntRoundUp((int)BLEND_COUNT   - 1)   ),
               X_DEFBITS_ARG( zbuffer,      ZBUFFER,        x_Log2IntRoundUp((int)ZBUFFER_COUNT - 1)   ),
               X_DEFBITS_ARG( raster,       RASTER,         x_Log2IntRoundUp((int)RASTER_COUNT  - 1)   ),
               X_DEFBITS_ARG( cull,         CULL,           x_Log2IntRoundUp((int)CULL_COUNT    - 1)   ),
               X_DEFBITS_ARG( misc,         MISC,           x_Log2IntRoundUp((int)MISC_COUNT    - 1)   )
    );

    static_assert( ((int)CULL_COUNT    - 1) == 2, "" );
    static_assert( x_Log2IntRoundUp((int)CULL_COUNT    - 1) == 2, "" ); 

    struct buffers
    {
        xbuffer_view<vertex>     m_Vertices;
        xbuffer_view<u32>        m_Indices;

        inline void FillIndexAsUnconnectedLines( void ) noexcept
        { 
            xbuffer_view<u32>::t_counter i = 0; 
            for( u32& I : x_iter_ref( i, m_Indices ) ) 
                I = static_cast<u32>(i); 
        }
    };

public:

    // Driver functions
    virtual             buffers             popBuffers          ( const int nVertices, const int nIndices )                                                 noexcept = 0;
    virtual             void                DrawBufferTriangles ( const xmatrix4& L2C, int IndexOffset = 0, int IndexCount = -1 )                           noexcept = 0;
    virtual             void                DrawBufferLines     ( const xmatrix4& L2C )                                                                     noexcept = 0;
    virtual             void                DrawBufferTriangles2(   const xmatrix4&                         L2C, 
                                                                    const eng_material_informed::handle&    hHandle,
                                                                    const eng_pipeline::handle&             hPipeline,
                                                                    const int                               IndexOffset = 0, 
                                                                    const int                               IndexCount  = -1 )                              noexcept = 0;

    // Driver State functions
    virtual             void                setSampler          ( const eng_sampler::handle& hSampler )                                                     noexcept = 0;
    virtual             void                setScissor          ( const xirect& Rect )                                                                      noexcept = 0;
    virtual             void                ClearSampler        ( void )                                                                                    noexcept = 0;
    virtual             void                ClearScissor        ( void )                                                                                    noexcept = 0;

    // Quick functions for rendering a simple primitive
    inline              void                DrawTriangle        ( const xmatrix4& L2C, const vertex& Vertex1, const vertex& Vertex2, const vertex& Vertex3 ) noexcept;
    inline              void                DrawTriangle        ( const xmatrix4& L2C, const xbuffer_view<vertex> Vertices )                                 noexcept;
    inline              void                DrawLine            ( const xmatrix4& L2C, const vertex& Vertex1, const vertex& Vertex2 )                        noexcept;
    inline              void                DrawLine            ( const xmatrix4& L2C, const xbuffer_view<vertex> Vertices )                                 noexcept;

    // Draw Primitives
    inline              void                DrawShadedRect      ( const xmatrix4& L2C, const xrect& Rect, const xcolor TL, const xcolor TR, const xcolor BL, const xcolor BR ) noexcept;
    inline              void                DrawSolidRect       ( const xmatrix4& L2C, const xrect& Rect, const xcolor Color )                                                 noexcept;
    inline              void                DrawTexturedRect    ( const xmatrix4& L2C, const xrect& Rect, const xrect& TexCoord = xrect{0,0,1,1}, const xcolor Color = xcolor{~0u})                          noexcept;
                        void                DrawDebugMarker     ( const xmatrix4& W2C, const xvector3& Pos, const xcolor Color )                                               noexcept;

    inline              void                DrawBBox            ( const xmatrix4& L2C, const xvector3& Center, const xcolor Color )                         noexcept;
    inline              void                DrawSphere          ( const xmatrix4& L2C, const xcolor Color )                                                 noexcept;


    // Allocate primitives
    inline              buffers             popSolidCube        ( const xbbox&                      BBox, 
                                                                  const xcolor                      Color )                                                 noexcept;
    inline              buffers             popWireCube         ( const xbbox&                      BBox, 
                                                                  const xcolor                      Color )                                                 noexcept;
                        buffers             popSolidUVSphere    ( const int                         nSlices,
                                                                  const int                         nStacks,
                                                                  const xcolor                      Color )                                                 noexcept;

    // Copy data to render primitives
            static      void                CopySolidCubeData   ( xbuffer_view<eng_draw::vertex>    pVertex, 
                                                                  xbuffer_view<u32>                 pIndex, 
                                                                  const xbbox&                      BBox, 
                                                                  const xcolor                      Color )                                                 noexcept;

            static      void                CopyWireCubeData    ( xbuffer_view<eng_draw::vertex>    pVertex, 
                                                                  xbuffer_view<u32>                 pIndex, 
                                                                  const xbbox&                      BBox, 
                                                                  const xcolor                      Color )                                                 noexcept;
 
//    x_import    void    setProgram          ( const eng_shader_program& Program );
//    x_import    void    ClearProgram        ( void );

protected:

    virtual                                ~eng_draw            ( void )                                                                                    noexcept {}
};



