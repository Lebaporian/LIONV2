#include "MyGame.h"

namespace gb_unit_test_06
{
    //-------------------------------------------------------------------------------------------

    void game_mgr::onInitialize( void ) noexcept
    {
        //
        // Notify our parent about initialization
        //
        t_parent::onInitialize();


		//
		// Register all the constant data types that we need to deal with
		//
		m_ConstDataTypeDB.RegisterButRetainOwnership(render_component::const_data::t_type);
		m_ConstDataTypeDB.RegisterButRetainOwnership(physics_component::const_data::t_type);


        //
        // Register managers
        //
        m_CompManagerDB.RegisterButRetainOwnership( m_AIMgr );            

        m_CompManagerDB.RegisterButRetainOwnership( m_RenderMgr );            
        const float         XRes = static_cast<f32>(m_RenderMgr.getWindow().getWidth());
        const float         YRes = static_cast<f32>(m_RenderMgr.getWindow().getHeight());

        m_CompManagerDB.RegisterButRetainOwnership( m_PhysicsMgr );
        m_PhysicsMgr.Initialize( XRes, YRes );

        //
        // Build the game graph
        //
        if( true )
        {
            // Less precise but more parallel graph
            InitializeGraphConnection( m_StartSyncPoint,    m_PhysicsMgr,  m_PhysicsSyncPoint      );
            InitializeGraphConnection( m_PhysicsSyncPoint,  m_AIMgr,       m_EndSyncPoint          );
            InitializeGraphConnection( m_PhysicsSyncPoint,  m_RenderMgr,   m_EndSyncPoint          );
        }
        else
        {
            // More precise graph because entities created in the AIMgr will be render this frame
            InitializeGraphConnection( m_StartSyncPoint,    m_PhysicsMgr,  m_PhysicsSyncPoint      );
            InitializeGraphConnection( m_PhysicsSyncPoint,  m_AIMgr,       m_AISyncPoint           );
            InitializeGraphConnection( m_AISyncPoint,       m_RenderMgr,   m_EndSyncPoint          );
        }
            
        //
        // Register all the component types
        //
        RegisterComponentAndDependencies<entity_bullet>         ( m_AIMgr,      m_AISyncPoint );
        RegisterComponentAndDependencies<entity_spaceship>      ( m_AIMgr,      m_AISyncPoint );
		RegisterComponentAndDependencies<entity_spawner>		( m_AIMgr,		m_AISyncPoint );
        RegisterComponentAndDependencies<physics_component>     ( m_PhysicsMgr, m_PhysicsSyncPoint );
        RegisterComponentAndDependencies<render_component>      ( m_RenderMgr,  m_EndSyncPoint );

        //
        // Create Bullet blue print
        //
        const gb_blueprint::master::guid        gBulletBluePrint        { x_constStrCRC32("BulletBluePrint")        };
        {
            // Constant data for the bullet physics
            const gb_component::const_data::guid gBulletPhysicsConstData { x_constStrCRC32("BulletPhysicsConstData") };
            {
                x_ll_share_ref<physics_component::const_data>   BulletPhysicsConstData;
                BulletPhysicsConstData.New( gBulletPhysicsConstData );
                BulletPhysicsConstData->m_bHandleCollisions = true;
                BulletPhysicsConstData->m_bIgnoreCollisions = false;
                m_ConstDataDB.RegisterButRetainOwnership( *BulletPhysicsConstData.TransferOwnerShip() );
            }

            // Constant data for the bullet render
            const gb_component::const_data::guid gBulletRenderConstData { x_constStrCRC32("BulletRenderConstData") };
            {
                x_ll_share_ref<render_component::const_data>    BulletRenderConstData;
                BulletRenderConstData.New( gBulletRenderConstData );
                BulletRenderConstData->m_Shape = render_component::const_data::shape::TRIANGLE;
                BulletRenderConstData->m_Color = xcolor( 0xff, 0xff, 0, 0xff);
                m_ConstDataDB.RegisterButRetainOwnership( *BulletRenderConstData.TransferOwnerShip() );
            }

            // Create the blue print
            {
                auto& Blueprint = CreateBlueprint( gBulletBluePrint );
                auto& Entity    = CreateEntity<entity_bullet>       ( gb_component::entity::guid( gb_component::entity::guid::RESET ) );
                auto& Physics   = CreateComponent<physics_component>( Entity ).LinearSetup( m_ConstDataDB.getEntry( gBulletPhysicsConstData ).SafeCast<physics_component::const_data>() );
                auto& Render    = CreateComponent<render_component> ( Entity ).LinearSetup( m_ConstDataDB.getEntry( gBulletRenderConstData ).SafeCast<render_component::const_data>() );

                Blueprint.setup( X_STR(""), X_STR("bullet_BP"), Entity );

                // Done with the entity
                Entity.linearDestroy();
            }
        }

        //
        // Create Spaceship blue print
        //
        const gb_blueprint::master::guid        gSpaceshipBluePrint     { x_constStrCRC32("SpaceshipBluePrint")     };
        {
    
            // constant data for the spaceship physics
            const gb_component::const_data::guid    gShipPhysicsConstData   { x_constStrCRC32("ShipPhysicsConstData")   };
            {
                x_ll_share_ref<physics_component::const_data>   ShipPhysicsConstData;
                ShipPhysicsConstData.New( gShipPhysicsConstData );
                ShipPhysicsConstData->m_bHandleCollisions   = false;
                ShipPhysicsConstData->m_bIgnoreCollisions   = false;
                m_ConstDataDB.RegisterButRetainOwnership( *ShipPhysicsConstData.TransferOwnerShip() );
            }

            // Constant data for the spaceship render
            const gb_component::const_data::guid gSpaceshipRenderConstData { x_constStrCRC32("SpaceshipRenderConstData") };
            {
                x_ll_share_ref<render_component::const_data>    SpaceshipRenderConstData;
                SpaceshipRenderConstData.New( gSpaceshipRenderConstData );
                SpaceshipRenderConstData->m_Shape = render_component::const_data::shape::SQUARE;
                SpaceshipRenderConstData->m_Color = xcolor( 0x9f, 0X9f, 0X9F, 0xff);
                m_ConstDataDB.RegisterButRetainOwnership( *SpaceshipRenderConstData.TransferOwnerShip() );
            }

            // create blue print
            {
                auto& Blueprint = CreateBlueprint( gSpaceshipBluePrint );
				auto& Entity = CreateEntity<entity_spaceship>(gb_component::entity::guid(gb_component::entity::guid::RESET));// .LinearSetup(gBulletBluePrint);
                auto& Physics   = CreateComponent<physics_component>( Entity ).LinearSetup( m_ConstDataDB.getEntry( gShipPhysicsConstData ).SafeCast<physics_component::const_data>()    );
                auto& Render    = CreateComponent<render_component> ( Entity ).LinearSetup( m_ConstDataDB.getEntry( gSpaceshipRenderConstData ).SafeCast<render_component::const_data>() );

                Blueprint.setup( X_STR(""), X_STR("spaceShip_BP"), Entity );

                // Done with the entity
                Entity.linearDestroy();
            }
        }

		auto& Entity = CreateEntity<entity_spawner>(gb_component::entity::guid(gb_component::entity::guid::RESET));
		Entity.linearAddToWorld();
		

  //      //
  //      // Create a bunch of ships in the world
  //      //
		//x_constexprvar int  nShips = 1000;// 135000;
  //      auto&               Blueprint = m_BlueprintDB.getEntry( gSpaceshipBluePrint );
  //      x_job_block         JobBlock(X_WSTR("CreateShips"));
  //      for( s32 i=0; i<nShips; i++ )
  //      {
  //          // When in release we can add entities in parallel, because in debug we saturate the circular queue
  //          #if _X_RELEASE
  //              JobBlock.SubmitJob( [this,&Blueprint,XRes,YRes]()
  //          #endif
  //              {
  //                  gb_component::entity& Entity = Blueprint.CreateInstance( gb_component::entity::guid( gb_component::entity::guid::RESET ), *this ).SafeCast<entity_spaceship>();

  //                  xvector2 Pos;
  //                  xvector2 Vel;
  //                  Pos.m_X = m_LinearTypes.m_SmallRnd.RandF32( 0, XRes );
  //                  Pos.m_Y = m_LinearTypes.m_SmallRnd.RandF32( 0, YRes );

  //                  x_constexprvar f32 VelMag = 4.0f*10;
  //                  Vel.m_X = m_LinearTypes.m_SmallRnd.RandF32( -VelMag, VelMag );
  //                  Vel.m_Y = m_LinearTypes.m_SmallRnd.RandF32( -VelMag, VelMag );
  //                  Entity.getComponent<physics_component>()->LinearSetup( Pos, Vel );

  //                  // Add to world the entity async or sync depending on build mode
  //                  #if _X_RELEASE
  //                      Entity.msgAddToWorld();
  //                  #else
  //                      Entity.linearAddToWorld();
  //                  #endif 
  //              }
  //          #if _X_RELEASE 
  //          );
  //          #endif
  //      } 
  //      JobBlock.Join();
    }
        
    //-------------------------------------------------------------------------------------------

    void game_mgr::onEndFrame( void ) noexcept
    {
        gb_game_graph::base::onEndFrame();

        static int x=0;
        x++;
        if( (x%60)==0 )
        {
            std::cout << "Frame #" << m_nFrames << " SecondsPerFrame: " << xtimer::ToSeconds( m_Stats_LogicTime ) << "\n";
        }

        //
        // This is totally not the right place the handle the windows messages, it should be inside a input_mgr but
        // since this demo is simple this will work for now.
        //
        m_bLoop = m_RenderMgr.getWindow().HandleEvents();
    }
}