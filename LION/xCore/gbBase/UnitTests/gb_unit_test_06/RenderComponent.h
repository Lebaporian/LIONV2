namespace gb_unit_test_06
{
    class render_mgr;

    class render_component : public gb_component::base
    {
        x_object_type( render_component, is_quantum_lock_free, rtti(gb_component::base), is_not_copyable, is_not_movable )

	public: // --- class traits ---

        // Customization of the physics for this particular example
        struct const_data : public gb_component::const_data
        {
            x_object_type( const_data, is_quantum_lock_free, rtti(gb_component::const_data), is_not_copyable, is_not_movable )
    
            x_constexprvar type t_type = 
            { 
                static_cast<const_data*>(nullptr),
				{ x_constStrCRC32("Render/SpaceshipsRender::const_data") },
                X_STR("space_ship_game/render_component/const_data"),
                X_STR("No help!") 
            };

			enum shape : u8
			{
				NONE,
				TRIANGLE,
				SQUARE
			};

			virtual prop_table&  onPropertyTable(void) const noexcept override
			{
				static prop_table E(this, this, X_STR("Unittest06-constRender"), [&](xproperty_v2::table& E)
				{
					E.AddChildTable(t_parent::onPropertyTable());
				//	E.AddProperty(X_STR("ShareColor"), m_ShareColor, X_STR("this is an example about a property of color part of the constant data"));
					E.AddProperty(X_STR("Color"), m_Color, X_STR("this is a property of color part of the constant data"));

					static const xarray< std::pair<int, xconst_str<xchar>>, 2> ShapeEnumList
					{
						std::pair<int, xconst_str<xchar>>{ static_cast<int>(shape::TRIANGLE),  X_STR("TRIANGLE")       },
						std::pair<int, xconst_str<xchar>>{ static_cast<int>(shape::SQUARE),  X_STR("SQUARE")       },
						//std::pair<int, xconst_str<xchar>>{ static_cast<int>(xproperty_data::data_enum::entry::END_DISPLAY), X_STR("") },
						//std::pair<int, xconst_str<xchar>>{ static_cast<int>(shape::NONE), X_STR("INVALID")   }
					};
					E.AddProperty(X_STR("ShapeEnum"), m_Shape, xproperty_v2::info::enum_t(ShapeEnumList), X_STR("ShapeEnum"));
					//E.AddProperty(X_STR("Shape"), m_Shape, X_STR("this is a property of the shape part of the constant data"));
				});

				return E;
			}

            

            x_forceconst                const_data          ( const guid Guid ) : gb_component::const_data( Guid ) {}
            virtual       const type&   getType             ( void )            const   noexcept            { return t_type; }

            xcolor                      m_Color     { ~0 };
            shape                       m_Shape     { shape::SQUARE };
        };

        // This structure contains detail information about our component that other system need
        struct t_descriptors : t_parent::t_descriptors
        {
            x_constexprvar auto             t_category_string           = X_STR_U("Render/SpaceshipsRender");
            x_constexprvar auto             t_type_guid                 = gb_component::type_base::guid{ X_STR_CRCINFO( "Render-SpaceshipsRender" ) };
            x_constexprvar auto             t_help                      = X_STR("This component should never be use in production");
            x_constexprvar def              t_definition                = def::MASK_DEFAULT;
            using                           t_type                      = gb_component::type_pool<t_self>;
        };

    public: // --- class traits ---

        render_component&                   LinearSetup             ( const_data& ConstData )
        {
            m_ConstData.AddReference( ConstData );
            return *this; 
        }

    protected: 

        x_forceinline                       render_component        ( const base_construct_info& C )            noexcept : gb_component::base(C) {};
        virtual         void                onResolve               ( void )                                    noexcept override;
        virtual         err                 onCheckResolve          ( xvector<xstring>& WarningList )   const   noexcept override;
        virtual         prop_table&         onPropertyTable         ( void )                                            const noexcept override 
        { 
            static prop_table E( this, this, X_STR("Render-SpaceShip"), [&](xproperty_v2::table& E)
            {
                E.AddChildTable( t_parent::onPropertyTable() );
                E.AddProperty<u64>      ( X_STR("ConstantData"),    GB_PROP_CONSTANT_DATA( m_ConstData ),   X_STR("This is the component constant/read-only data. This data will be use by the component so it must be of the correct type." ) );
            });

            return E; 
        }

    protected: 
    
        physics_component*          m_pPhysics  { nullptr };
        x_ll_share_ref<const_data>  m_ConstData {};

    protected: 

        friend class render_mgr;
        using gb_component::base::qt_onRun;
    };
}
