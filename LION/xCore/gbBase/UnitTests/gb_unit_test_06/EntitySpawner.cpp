#include "MyGame.h"


namespace gb_unit_test_06
{
    //------------------------------------------------------------------------------------------------
    
	void entity_spawner::onExecute(void) noexcept
	{
		t_parent::onResolve();


		auto&       GameMgr = getGlobalInterface().m_GameMgr.SafeCast<game_mgr>();


		//
		// Create a bunch of ships in the world
		//
		const float         XRes = static_cast<f32>(GameMgr.getRenderMgr().getWindow().getWidth());
		const float         YRes = static_cast<f32>(GameMgr.getRenderMgr().getWindow().getHeight());


		x_constexprvar int  nShips = 1000;// 135000;
		auto&               Blueprint = GameMgr.m_BlueprintDB.getEntry(m_bluePrint);
		x_job_block         JobBlock(X_WSTR("CreateShips"));
		for (s32 i = 0; i<nShips; i++)
		{
			// When in release we can add entities in parallel, because in debug we saturate the circular queue
#if _X_RELEASE
			JobBlock.SubmitJob([this, &Blueprint, XRes, YRes]()
#endif
			{
				gb_component::entity& Entity = Blueprint.CreateInstance(gb_component::entity::guid(gb_component::entity::guid::RESET), GameMgr);

				xvector2 Pos;
				xvector2 Vel;
				Pos.m_X = m_SmallRnd.RandF32(0, XRes);
				Pos.m_Y = m_SmallRnd.RandF32(0, YRes);

				x_constexprvar f32 VelMag = 4.0f * 10;
				Vel.m_X = m_SmallRnd.RandF32(-VelMag, VelMag);
				Vel.m_Y = m_SmallRnd.RandF32(-VelMag, VelMag);
				Entity.getComponent<physics_component>()->LinearSetup(Pos, Vel);

				// Add to world the entity async or sync depending on build mode
#if _X_RELEASE
				Entity.msgAddToWorld();
#else
				//Entity.linearAddToWorld();//linear world
				Entity.msgAddToWorld();//in quantum world
#endif 
			}
#if _X_RELEASE 
			);
#endif
		}
		JobBlock.Join();

		
		//destroy myself
		msgDestroy();
	}	
}