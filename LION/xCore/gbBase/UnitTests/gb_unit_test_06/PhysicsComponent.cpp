
#include "MyGame.h"

namespace gb_unit_test_06
{
    //-------------------------------------------------------------------------------

    xrect physics_component::getBounds( f32 delta ) const
    {   
        const auto& T0      = getT0Data<mutable_data>();
        const f32   R       = 1;
        const f32   EnergyR = (T0.m_Vel * delta).getLength();
        const f32   X       = T0.m_Pos.m_X;
        const f32   Y       = T0.m_Pos.m_Y;
        const xrect Bounds  = xrect(X - R, Y - R, X + R, Y + R).Inflate( EnergyR, EnergyR );

        return Bounds;
    }
}
