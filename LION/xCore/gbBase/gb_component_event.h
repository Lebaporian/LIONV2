 //----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

namespace gb_component
{
    //  GB_DEF_COMPONENT_EVENT( "Activate", event_Activate, "This event will be trigger when the entity activates", bool );
    //  event_Activate m_Activate{ X_STR_CRCINFO("Unique Instance GUID"), X_STR("Display Name") };
    //  event_Activate::delegate<t_self> m_ActivateDelegate{ *this, &t_self::setActive }; 

    //------------------------------------------------------------------------------
    // Description:
    //          Narrow guid are unique guids used to identify an component instance
    //          inside an entity. To achive this the narrow guid uses two parts:
    //          24 bits of the component type guid (gameCRC)
    //          8 bits which are store in each component. This 8 bits are call the narrow_count.
    //          Since the narrow count is 8bits it can have up to 254 different component of the same type.
    //          0xff is reserved to represent null. 
    //------------------------------------------------------------------------------
    struct narrow_count
    {
                                                narrow_count        ( void ) = default;
        x_forceconst        explicit            narrow_count        ( nullptr_t )                               noexcept : m_Value{0xff}{}
        x_forceconst        explicit            narrow_count        ( u32 V )                                   noexcept : m_Value{static_cast<u8>(V&0xff)}{}
        x_forceconst bool                       isValid             ( void )                            const   noexcept { return m_Value != 0xff; }
        x_forceconst bool                       isNull              ( void )                            const   noexcept { return m_Value == 0xff; }
        u8 m_Value;
    };

    struct narrow_guid 
    { 
                                                narrow_guid         ( void ) = default;
        x_forceconst                            narrow_guid         ( named_guid<type_base> ComponentNameCRC, narrow_count Count )  noexcept : m_Value{ (ComponentNameCRC.m_Value<<8) | Count.m_Value } {}
        x_forceconst        explicit            narrow_guid         ( u32 Value )                               noexcept : m_Value{Value}{}
        x_forceconst bool                       operator ==         ( narrow_guid X )                   const   noexcept { return X.m_Value == m_Value; }
        x_forceconst bool                       operator !=         ( narrow_guid X )                   const   noexcept { return X.m_Value != m_Value; }
        x_forceconst auto                       getNarrowCount      ( void )                            const   noexcept { return narrow_count{ m_Value }; }
        u32 m_Value; 
    };

    //------------------------------------------------------------------------------
    // Description:
    //          Events are generic callbacks / messages that a component may issue.
    //          Events are broken down into 3 parts; the event type, the event instance or caller, and the delegate or receiver.
    //          What this means is that a component which has an event can call other components that are listening to that particular event.
    //          Events types are global objects which define a particular static typed event.This ensures that the delegates 
    //          that connect to the event match the right type.There is not a limitation on how specific or general these types are.
    //          Events Instances are used in the components directly. Their job is to call to any delegate that is connected to it. 
    //          Event Instances have a GUID which means a component may have multiple instances of a particular event type.
    //              u32 - the type of the event
    //              u32 - The event GUID
    //          Delegates are the callbacks instances that register with events.They also belong to components as well. 
    //          A delegate can only be registered with one event.
    //          Delegates have a GUID that links the delegate with the right event. This GUID has the following information :
    //              u32 - The type of the event
    //              u8  - Narrow GUID
    //              u24 - The event GUID
    //------------------------------------------------------------------------------
 
    //------------------------------------------------------------------------------
    // Description:
    //          Event types defines category of an event. It is a companion structure that gives static 
    //          information about the event. You do not create this class directly.
    //------------------------------------------------------------------------------
    struct event_type_base
    {
    public:

        using guid = extended_guid<event_type_base,event_type_base,0ul>;

    public:

        xstring::const_str          m_Name;
        guid                        m_Guid;
        xstring::const_str          m_Help;
    };

    struct interface_type : event_type_base
    {
    public:
        x_constexprvar  auto        t_class_string                  = X_STR("InterfaceType");
        x_constexprvar  auto        t_class_guid                    = class_guid{"InterfaceType"};

    public:

        constexpr                   interface_type          ( xstring::const_str Name, guid Guid, xstring::const_str Help ) : event_type_base{ Name, guid{ t_class_guid, Guid.getNameCRC() }, Help }{}
    };

    struct event_type : event_type_base
    {
    public:
        x_constexprvar  auto        t_class_string                  = X_STR("EventType");
        x_constexprvar  auto        t_class_guid                    = class_guid{"EventType"};

    public:
        constexpr                    event_type              ( xstring::const_str Name, guid Guid, xstring::const_str Help ) : event_type_base{ Name, guid{ t_class_guid, Guid.getNameCRC() }, Help }{}
    };

    //------------------------------------------------------------------------------
    // Description:
    //          An Event is a class that can send message to another component.
    //          This class gets instantiated every time you may want to create such a message pump.
    //          You do not use this class directly rather you define the event by using GB_COMPONENT_EVENT_TYPE_DEF 
    //------------------------------------------------------------------------------
    struct event
    {
        x_constexprvar  auto        t_class_string                  = X_STR("EventInstance");
        x_constexprvar  auto        t_class_guid                    = class_guid{ "EventInstance" };
        
        using guid = extended_guid<event,event_type_base,t_class_guid.m_Value>;

        struct delegate
        {
            x_object_type(delegate, is_linear, rtti_start, is_not_copyable, is_not_movable)

            struct guid : xguid<delegate>
            {
                using xguid<delegate>::xguid;

                                 guid( void ) = default;
                x_forceconst     guid( named_guid<event_type_base> NameCRC, narrow_guid NarrowGUID ) noexcept : xguid{ NarrowGUID.m_Value, NameCRC.m_Value } {}

                    // This is the guid that the delegate is pointing to
                //      The lower 32 bits has the component NamedCRC/type
                //      The next   8 bits have the narrow GUID of the component
                //      The upper 24 bits has the lower 24bits of the event guid 
                x_forceconst named_guid<event_type_base>    getNamedCRC    ( void ) const { return named_guid<event_type_base>{static_cast<u32>(m_Value)};      }
                x_forceconst narrow_guid                    getNarrowGuid  ( void ) const { return narrow_guid{ static_cast<u32>(m_Value>>32) };  }
            };

            guid                            m_Guid{};

                                            delegate                ( void ) = default;
            virtual const event_type&       getEventType            ( void ) const noexcept = 0;
        };
    };

    //------------------------------------------------------------------------------
    // Description:
    //          Reference to interfaces
    //------------------------------------------------------------------------------
    struct interface_base{};
    template< typename T_TYPE >
    struct interface_ref
    {
        event::delegate::guid   m_Guid {nullptr};
        T_TYPE*                 m_pValue{nullptr};
    };

    //------------------------------------------------------------------------------
    // Description:
    //          Macro used to create Events + Event_types and its delegates
    //------------------------------------------------------------------------------
#define GB_COMPONENT_EVENT_TYPE_DEF( TYPE_NAME, UNIQUE_TYPE_NAME, HELP, ... )                                                               \
    struct TYPE_NAME final : gb_component::event, x_message::event<__VA_ARGS__>                                                             \
    {                                                                                                                                       \
        static const gb_component::event_type& getType( void )                                                                              \
        {                                                                                                                                   \
            x_constexprvar gb_component::event_type EventType                                                                               \
            {                                                                                                                               \
                X_STR(UNIQUE_TYPE_NAME),                                                                                                    \
                gb_component::event_type::guid{ gb_component::event_type::t_class_guid, gb_component::named_guid<gb_component::event_type_base>{ x_constStrCRC32(UNIQUE_TYPE_NAME) } },\
                X_STR(HELP)                                                                                                                 \
            };                                                                                                                              \
            return EventType;                                                                                                               \
        }                                                                                                                                   \
        template< typename T_CLASS >                                                                                                        \
        class delegate : public gb_component::event::delegate, public x_message::delegate<T_CLASS, __VA_ARGS__ >                            \
        {                                                                                                                                   \
            x_object_type( delegate, is_linear, rtti(gb_component::event::delegate), is_not_copyable, is_not_movable )                      \
            using t_parent2 =  x_message::delegate<T_CLASS, __VA_ARGS__ >;                                                                  \
            using t_event   =  TYPE_NAME;                                                                                                   \
        public:                                                                                                                             \
            x_forceconst                    delegate                    (void(T_CLASS::*pFunction)( __VA_ARGS__ ) ) :                       \
                                                                            t_parent2( pFunction ) {}                                       \
            x_forceconst                    delegate                    (T_CLASS& Class, void(T_CLASS::*pFunction)( __VA_ARGS__ ) ) :       \
                                                                            t_parent2( Class, pFunction ) {}                                \
            x_forceconst                    delegate                    ( void ) = delete;                                                  \
            virtual const gb_component::event_type&        getEventType                ( void ) const noexcept override                     \
                                                            { return TYPE_NAME::getType();  }                                               \
        };                                                                                                                                  \
    };         

    GB_COMPONENT_EVENT_TYPE_DEF( event_fake, "", "", gb_component::base& )


    //------------------------------------------------------------------------------
    // Description:
    //          This is a base class to define a collection of events... it can be one.
    //          All events of a component should be inside this class.
    //          You do not use this class directly stead you will use: events_harness<>
    //------------------------------------------------------------------------------
    struct events
    {
        x_object_type( events, is_linear, rtti_start, is_not_copyable, is_not_movable )

        using guid              = gb_component::event::guid;

        events(void) = default;

        struct definition final
        {
            constexpr definition(std::size_t Offset, xstring::const_str Name, gb_component::event::guid Guid, const event_type_base& EventType, xstring::const_str Help = { nullptr }) :
                m_Offset    { Offset                                    },
                m_Name      { Name.m_pValue                             },
                m_CRC       { x_constStrCRC32(Name.m_pValue)            },
                m_Guid      { Guid                                      },
                m_EventType { EventType                                 },
                m_Help      { Help.m_pValue ? Help : EventType.m_Help   } {}

            std::size_t                 m_Offset;
            xstring::const_str          m_Name;
            u32                         m_CRC;
            gb_component::event::guid   m_Guid;
            xstring::const_str          m_Help;
            const event_type_base&      m_EventType;
        };

        struct table  {};
    };

    //------------------------------------------------------------------------------
    // Description:
    //          This is a base class to define a collection of delegates... it can be one.
    //          All delegates of a component should be inside this class.
    //------------------------------------------------------------------------------
    struct delegates
    {
        x_object_type( delegates, is_linear, rtti_start, is_not_copyable, is_not_movable )
        delegates( void ) = default;

        struct definition final
        {
            constexpr definition(std::size_t Offset, xstring::const_str Name, const event_type_base& EventType, xstring::const_str Help = { nullptr }) :
                m_Offset    { Offset                                    },
                m_Name      { Name.m_pValue                             },
                m_CRC       { x_constStrCRC32(Name.m_pValue)            },
                m_EventType { EventType                                 },
                m_Help      { Help.m_pValue ? Help : EventType.m_Help   } {}

            std::size_t                 m_Offset;
            xstring::const_str          m_Name;
            u32                         m_CRC;
            xstring::const_str          m_Help;
            const event_type_base&      m_EventType;
        };

        struct table {};
    };
}